import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class I18NDelegate extends LocalizationsDelegate<I18N> {
  const I18NDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'es'].contains(locale.languageCode);

  @override
  Future<I18N> load(Locale locale) {
    return SynchronousFuture<I18N>(I18N(locale));
  }

  @override
  bool shouldReload(I18NDelegate old) => false;
}

class I18N {
  I18N(this.locale);

  final Locale locale;

  static I18N? of(BuildContext context) {
    return Localizations.of<I18N>(context, I18N);
  }

  //TODO : for example
  static Map<String, Map<String, String>> localizedValues = {
    'en': {
      'heading': 'Mob\'s kitchen',
      'subheading': 'place your order',
      'welcomeMessage': 'Welcome Back!',
      'deliveryCardString1': 'Our job is to filling your',
      'deliveryCardString2': 'tummy with ',
      'deliveryCardString3': 'Delicious food',
      'deliveryCardString4': '& ',
      'deliveryCardString5': 'Fast Delivery',
      'vendorString': 'Select Vendor',
      'ratingCardString1': 'Help us improve!',
      'ratingCardString2': 'Please share your feedback',
      'ratingCardString3': 'Rate Us',
      'chefCardString1': 'Order from',
      'chefCardString2': 'MOB’s Kitchen',
      'orderBtString': 'Order now',
      'chefCardString3': 'place your order',
      'menuTitleOrder': 'My Order',
      'menuTitleWallet': 'My Wallet',
      'menuTitleAbout': 'About Us',
      'logoutBtString': 'Logout',
      'couponWalletString': 'Coupon Wallet',
      'walletString': 'Wallet',
      'MobMenu': "MOB's Menu",
      'Checkoutbutton': 'Checkout',
      'emaillabel': 'Email',
      'emailhint': 'Type Email',
      'passwordlabel': 'Password',
      'passwordhint': 'Type Password',
      'loginbuttontext': 'Login',
      'authorizeduser': 'Invalid Email & Password',
      'emailvalidation': 'Email can/t be empty',
      'passwordvalidation': 'Password can/t be empty',
      'passwordvalidation1': 'Don/t use space while entring the password',
      'passwordvalidation2': 'Password is less then 4 words',
      'CheckoutButton': 'Checkout',
      'MyWallet': 'MyWallet',
      'Coupon Wallet': 'Coupon Wallet',
      'Wallet': 'Wallet',
      'Transactions': 'Transactions',
      'Payment Details': 'Payment Details',
      'Product Item': 'Product Item',
      'Total Amount': 'Total Amount',
      'Place Order': 'Place Order',
      'Cancel': 'Cancel',
      'Item': 'Item',
      'YES': 'YES',
      'Are you sure, you want to cancel this payment?': 'Are you sure, you want to cancel this payment?',
    },
    'es': {
      'heading': 'Mob\'s Cocino',
      'subheading': 'Haga su pedido',
      'welcomeMessage': 'Bienvenido de nuevo!',
      'deliveryCardString1': 'Nuestro trabajo es llenar su',
      'deliveryCardString2': 'panza con ',
      'deliveryCardString3': 'Comida deliciosa',
      'deliveryCardString4': '& ',
      'deliveryCardString5': 'Entrega rápida',
      'vendorString': 'Seleccionar proveedor',
      'ratingCardString1': 'Ayúdanos a mejorar!',
      'ratingCardString2': 'Por favor comparta sus comentarios',
      'ratingCardString3': 'Nos califica',
      'chefCardString1': 'Ordenar desde',
      'chefCardString2': 'La cocina de MOB',
      'orderBtString': 'Ordenar ahora',
      'chefCardString3': 'Haga su pedido',
      'menuTitleOrder': 'Mi pedido',
      'menuTitleWallet': 'Mi billetera',
      'menuTitleAbout': 'Sobre nosotros',
      'logoutBtString': 'Cerrar sesión',
      'couponWalletString': 'Cartera de cupones',
      'walletString': 'Cartera',
      'MobMenu': 'Menú de MOB',
      'CheckoutButton': 'Verificar',
      'emaillabel': 'Correo electrónico',
      'emailhint': 'Escriba el correo electrónico',
      'passwordlabel': 'Clave',
      'passwordhint': 'Escriba contraseña',
      'loginbuttontext': 'Acceso',
      'authorizeduser': 'Correo electrónico y contraseña no válidos',
      'emailvalidation': 'El correo electrónico no puede estar vacío',
      'passwordvalidation': 'La contraseña no puede estar vacía',
      'passwordvalidation1': 'No use espacio al ingresar la contraseña',
      'passwordvalidation2': 'La contraseña es menos de 4 palabras',
      'MyWallet': 'Mi billetera',
      'Coupon Wallet': 'Cartera de cupones',
      'Wallet': 'Cartera',
      'Transactions': 'Actas',
      'Payment Details': 'Detalles del pago',
      'Product Item': 'Artículo del producto',
      'Total Amount': 'Cantidad total',
      'Place Order': 'Realizar pedido',
      'Cancel': 'Cancelar',
      'Item': 'Artículo',
      'YES': 'SÍ',
      'Are you sure, you want to cancel this payment?': '¿Está seguro de que desea cancelar este pago?',
    },
  };

  String? get heading {
    return localizedValues[locale.languageCode]!['heading'];
  }

  String? get subheading {
    return localizedValues[locale.languageCode]!['subheading'];
  }

  String? get welcomeMessage {
    return localizedValues[locale.languageCode]!['welcomeMessage'];
  }

  String? get deliveryCardString1 {
    return localizedValues[locale.languageCode]!['deliveryCardString1'];
  }

  String? get deliveryCardString2 {
    return localizedValues[locale.languageCode]!['deliveryCardString2'];
  }

  String? get deliveryCardString3 {
    return localizedValues[locale.languageCode]!['deliveryCardString3'];
  }

  String? get deliveryCardString4 {
    return localizedValues[locale.languageCode]!['deliveryCardString4'];
  }

  String? get deliveryCardString5 {
    return localizedValues[locale.languageCode]!['deliveryCardString5'];
  }

  String? get vendorString {
    return localizedValues[locale.languageCode]!['vendorString'];
  }

  String? get ratingCardString1 {
    return localizedValues[locale.languageCode]!['ratingCardString1'];
  }

  String? get ratingCardString2 {
    return localizedValues[locale.languageCode]!['ratingCardString2'];
  }

  String? get ratingCardString3 {
    return localizedValues[locale.languageCode]!['ratingCardString3'];
  }

  String? get chefCardString1 {
    return localizedValues[locale.languageCode]!['chefCardString1'];
  }

  String? get chefCardString2 {
    return localizedValues[locale.languageCode]!['chefCardString2'];
  }

  String? get chefCardString3 {
    return localizedValues[locale.languageCode]!['chefCardString3'];
  }

  String? get orderBtString {
    return localizedValues[locale.languageCode]!['orderBtString'];
  }

  String? get menuTitleOrder {
    return localizedValues[locale.languageCode]!['menuTitleOrder'];
  }

  String? get menuTitleWallet {
    return localizedValues[locale.languageCode]!['menuTitleWallet'];
  }

  String? get menuTitleAbout {
    return localizedValues[locale.languageCode]!['menuTitleAbout'];
  }

  String? get logoutBtString {
    return localizedValues[locale.languageCode]!['logoutBtString'];
  }


  String? get emaillabel {
    return localizedValues[locale.languageCode]!['emaillabel'];
  }

  String? get checkoutButton {
    return localizedValues[locale.languageCode]!['CheckoutButton'];
  }

  String? get emailhint {
    return localizedValues[locale.languageCode]!['emailhint'];
  }

  String? get passwordlabel {
    return localizedValues[locale.languageCode]!['passwordlabel'];
  }

  String? get passwordhint {
    return localizedValues[locale.languageCode]!['passwordhint'];
  }

  String? get loginbuttontext {
    return localizedValues[locale.languageCode]!['loginbuttontext'];
  }

  String? get authorizeduser {
    return localizedValues[locale.languageCode]!['authorizeduser'];
  }

  String? get emailvalidation {
    return localizedValues[locale.languageCode]!['emailvalidation'];
  }

  String? get passwordvalidation {
    return localizedValues[locale.languageCode]!['passwordvalidation'];
  }

  String? get passwordvalidation1 {
    return localizedValues[locale.languageCode]!['passwordvalidation1'];
  }

  String? get passwordvalidation2 {
    return localizedValues[locale.languageCode]!['passwordvalidation2'];
  }

  String? get mobMenu {
    return localizedValues[locale.languageCode]!['MobMenu'];
  }

  String? get myWallet {
    return localizedValues[locale.languageCode]!['MyWallet'];
  }

  String? get wallet {
    return localizedValues[locale.languageCode]!['Wallet'];
  }

  String? get couponWallet {
    return localizedValues[locale.languageCode]!['Coupon Wallet'];
  }

  String? get transaction {
    return localizedValues[locale.languageCode]!['Transactions'];
  }

  String? get paymentDetails {
    return localizedValues[locale.languageCode]!['Payment Details'];
  }

  String? get productItem {
    return localizedValues[locale.languageCode]!['Product Item'];
  }

  String? get totalAmount {
    return localizedValues[locale.languageCode]!['Total Amount'];
  }

  String? get placeOrder {
    return localizedValues[locale.languageCode]!['Place Order'];
  }

  String? get cancel {
    return localizedValues[locale.languageCode]!['Cancel'];
  }

  String? get item {
    return localizedValues[locale.languageCode]!['Item'];
  }
  String? get yes {
    return localizedValues[locale.languageCode]!['YES'];
  }
  String? get AreYouSureYouWantToCancelThisPayment {
    return localizedValues[locale.languageCode]!['Are you sure, you want to cancel this payment?'];
  }



}

