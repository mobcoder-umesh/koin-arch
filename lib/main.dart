
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:mob_kitchen_freshers/presentation/login/cubit/login_cubit.dart';
import 'package:mob_kitchen_freshers/presentation/login/login_view.dart';

import 'localization/i18n.dart';
import 'utils/app_constant.dart';
import 'utils/preference_util.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {

    return BlocProvider(
      create: (context) => LoginCubit(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: PageName.splash,
        localizationsDelegates: [
          I18NDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          Locale('en', ''),
          Locale('es', ''),
        ],

        home: LoginPage(),

      ),
    );
  }
}

