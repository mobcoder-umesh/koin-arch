import 'package:flutter/material.dart';

String? ACCESS_TOKEN = "";
String? EMPLOYEE_ID = "";

class AppConstants {
  static const String API_TYPE = 'request_code';
  static const String welcomeMessage = 'Welcome Back!';
  static const String deliveryCardString1 = 'Our job is to filling your';
  static const String deliveryCardString2 = 'tummy with ';
  static const String deliveryCardString3 = 'Delicious food';
  static const String deliveryCardString4 = '& ';
  static const String deliveryCardString5 = 'Fast Delivery';
  static const String vendorString = 'Select Vendor';
  static const String ratingCardString1 = 'Help us improve!';
  static const String ratingCardString2 = 'Please share your feedback';
  static const String ratingCardString3 = 'Rate Us';
  static const String chefCardString1 = 'Order from';
  static const String chefCardString2 = 'MOB’s Kitchen';
  static const String orderBtString = 'Order now';
  static const String chefCardString3 = 'place your order';
  static const String MobMenu = "MOB's Menu";
  static const String Chai = 'Masala Chai';
  static const String ChocolateMilkShake = 'Chocolate Milk Shake';
  static const String KandaPoha = 'Kanda Poha';
  static const String ChaiPrice = '₹10.0';
  static const String ChocolateMilkShakePrice = '₹30.0';
  static const String KandaPohaPrice = '₹10.0';
  static const String orderSchedule = 'Schedule for';
  static const String emaillabel = 'Email';
  static const String passwordlabel = 'Password';
  static const String emailhint = 'Type Email';
  static const String passwordhint = 'Type Password';
  static const String menuTitleOrder = 'My Order';
  static const String menuTitleWallet = 'My Wallet';
  static const String menuTitleAbout = 'About Us';
  static const String logoutBtString = 'Logout';
  static const String couponWalletString = 'Coupon Wallet';
  static const String walletString = 'Wallet';
  static const String loginbuttontext = 'Login';
  static const String myOrders = 'My Orders';
  static const String myWallet = 'My Wallet';
  static const String ordersDetails = 'Order Detail';
  static const String orderedBy = 'Ordered By';

  static const String productItem = 'Product Item';
  static const String numberItem = '1 Item';
  static const String userContact = 'User Contact';
  static const String contact = '910123456789';
  static const String orderIdText = 'Order ID';
  static const String orderID = '6456rfj6u74i76trkiyg';
  static const String orderStatus = 'Order Status';
  static const String status = 'text';
  static const String totalAmount = 'Total Amount';
  static const String amount = '₹40.0';
  static const String transaction = 'Transactions';
  static const String paymentDetails = 'Payment Details';
  static const String placeOrder = 'Place Order';
  static const String cancel = 'Cancel';
  static const String leaveRecord = 'Leave Record';
  static const String deviceRecord = 'Device Record';
  static const String dailyAttendence = 'Daily Attendence';
  static const String lateComings = 'LATE \n COMINGS';
  static const String averageHours = 'AVERAGE \n HRS/DAY';
  static const String earlyLeaves = 'EARLY \n LEAVES';
  static const String yourAttendence = 'Your Attendence';
  static const String checkIn = 'Check-In';
  static const String checkOut = '   Check-Out';
  static const String totalHours = 'Total Hours';
  static const String casual = 'CASUAL';
  static const String earned = 'EARNED';
  static const String sick = 'SICK';
  static const String compensatory = 'COMPENSATORY';
  static const String markedLWP = 'MARKED LWP';
  static const String startDate = 'Start Date';
  static const String endDate = 'End Date';
  static const String casualLeave = 'Casual Leave';
  static const String earlyLeave = 'Early Leave';
  static const String shortLeave = 'Short Leave';
  static const String lWPLeave = 'Leave With-Out Pay';
  static const String comOff = 'Com Off';
  static const String halfCL = 'Half Casual Leave';
  static const String halfEL = 'Half Early Leave';
  static const String halfSL = 'Half Short Leave';
  static const String halfLWP = 'Half Leave With-Out Pay';
  static const String notClear = 'Not Clear';
}

class PageName {
  static const String splash = 'Splash';
  static const String login = 'Login';
  static const String dashBoard = 'Dashboard';
  static const String menu = 'Menu';
  static const String orders = 'Orders';
  static const String payment = 'Payment';
  static const String wallet = 'Wallet';
  static const String orderDetails = 'OrderDetails';
  static const String aboutUs = 'Wallet';
}

class ApiKey {
  static const String FACEBOOK_KEY = 'facebookId';
  static const String SIGN_UP = 'sign_up';
  static const String PAIRING_STATUS = 'pairing_status';
  static const String PAIRING_CODE = 'pairing_code';
  static const String CONFIRMATION_CODE = 'confirmation_code';
  static const String MESSAGE = 'message';
  static const String DEVICE_ID = 'device_id';
  static const String AUTH_TOKEN = 'authentication_token';
  static const String USERNAME = 'username';
  static const String PASSWORD = 'password';
}

class CLR {
  static const whiteClr = Colors.white;
  static const blackClr = Colors.black;
  static const signInAndUpClr = Color(0xff7AAD45);
  static const deep_greyClr = Color(0xff666666);
  static const amberClr = Color(0xfffea000);
  static const maharoonClr = Color(0xff6c1b03);
  static const lightRedShade = Color(0xffFFC0C0);
  static const lightRedShade1 = Color(0xffFF7F7F);
  static const redShade = Color(0xffAC0000);
  static const orangeClr = Color(0xffc23001);
  static const deepOrangeClr = Color(0xff942601);
  static const greyShade = Color(0xff6B6B6B);
  static const light_greyClr = Color(0xffe2e2e2);
  static const light_container_greyClr = Color(0xffE6E6E6);
  static const lightGreyShade = Color(0xffAAAAAA);
  static const light_peachClr = Color(0xfffad3c3);
  static const light_container_peachClr = Color(0xffFFC0C0);
  static const light_container_orangeClr = Color(0xffFFD5A5);
  static const peachClr = Color(0xffffc7b5);
  static const ancent_amberClr = Color(0xfffec600);
  static const lightPeachShade1 = Color(0xffFFD5A5);
  static const lightPeachShade2 = Color(0xffFFBA9E);
  static const orangeShade = Color(0xffFD6121);
  static const greenClr = Color(0xff59AC00);
  static const lightGreenShade1 = Color(0xffC9ECBA);

  static const lightGreenShade2 = Color(0xffABDA97);
  static const lightGreenShade3 = Color(0xff59AC00);
  static const light_greenClr = Color(0xffEBF6D5);
  static const redClr = Color(0xffBE0312);
  static const light_redClr = Color(0xffF5D0D2);
  static const lightDarkRedShadeClr = Color(0xff814735);
  static const lightBrownShadeClr = Color(0xff894935);
  static const veryLightBrownShadeClr = Color(0xffA16D5D);
  static const brownDarkClr = Color(0xff6C1B03);
  static const peachShade = Color(0xffFAD2C2);
  static const chefCardBorder = Color(0xffCACACB);
  static const lightOrangeColor = Color(0xffE88431);
  static const orangeShade1 = Color(0xffD87046);
  static const orangeShade2 = Color(0xffBD633F);
  static const blueShade1 = Color(0xffC9E0EB);
  static const blueShade2 = Color(0xff1699D8);
  static const unselectedChoiceColor = Color(0xffD5D5D5);
  static const greencart = Color(0xff4CCB6B);
  static const greencart1 = Color(0xff53A767);
  static const bluecart = Color(0xff56B4E1);
  static const bluecart1 = Color(0xff1F99D2);
  static const darkredcart = Color(0xffD87046);
  static const darkredcart1 = Color(0xffBD633F);
  static const darkredcart2 = Color(0xff884935);
  static const light_border_orangeClr = Color(0xffFFBA9E);

  static const darkblueClr1 = Color(0xffC9E0EB);
  static const darkblueClr2 = Color(0xffE2F0F7);
  static const greyshadeClr = Color(0xffB8B8B8);
}

class IMAGE {
  static String appBackground = "assets/images/img_app_background.png";
  static String drawerBackground = "assets/images/drawer_background.png";

  //static String backButton = "assets/images/ic_back.png";

  static String teaImage = "assets/images/img_tea.png";
  static String coldCoffeeImage = "assets/images/img_cold_coffee.png";
  static String walletImage = "assets/images/img_money_bag.png";
  static String LOGO = "assets/images/mob_logo.png";
  static String logo = "assets/images/mob_logo.png";
  static String passwordLock = "assets/images/ic_msgbox.png";
  static String mailMessage = "assets/images/ic_lockmail.png";
  static String back_Arrow = "assets/images/back_Arrow.png";
  static const String signInBackground = 'assets/images/food.png';
  static const String signUpBackground = 'assets/images/Palette.png';
  static const String moneyBag = 'assets/images/ic_money_bag.png';
  static const String couponWalletImage = "assets/images/ic_coupon_wallet.png";
  static const String couponMyWalletImage ="assets/images/ic_coupon_mywallet.png";
  static const String blackAndWhiteGradient = 'assets/images/gradient.png';
  static const String deliveryBackground ='assets/images/delivery_background.png';
  static const String menuBt = 'assets/images/ic_menu.png';
  static const String likeBt = 'assets/images/ic_like.png';
  static const String dismissBt = 'assets/images/ic_dismiss.png';
  static const String drawerDismissBt = 'assets/images/ic_drawer_dismiss.png';
  static const String chefIcon = 'assets/images/ic_chef.png';
  static const String chefImage1 = 'assets/images/img_chef_1.png';
  static const String poha = 'assets/images/img_poha.png';
  static const String menuItemOrder = 'assets/images/ic_my_orders.png';
  static const String menuItemWallet = 'assets/images/ic_my_wallet.png';
  static const String menuItemAbout = 'assets/images/ic_about_us.png';
  static const String logoutBt = 'assets/images/ic_logout.png';
  static const String orderBox = 'assets/images/ic_box.png';
  static const String placeHolder = 'assets/images/ic_placeholder.png';
  static const String backButton = 'assets/images/ic_back_button.png';



  static const String dashBoardViewImg = 'assets/images/dashboard_view.png';
  static const String leavelogo = 'assets/images/ic_leave.png';
  static const String leavecartinnericon = 'assets/images/ic_leave_cart.png';
  static const String attendancelogo = 'assets/images/ic_attendance.png';
  static const String projectlogo = 'assets/images/ic_project.png';
  static const String docsImage = 'assets/images/img_docs.png';
  static const String pdfImage = 'assets/images/img_pdf.png';
  static const String pdfImage1 = 'assets/images/pdf_img.png';
  static const String pngImage = 'assets/images/img_png.png';
  static const String svgImage = 'assets/images/img_svg.png';
  static const String xlsImage = 'assets/images/img_xls.png';
  static const String jsonImage = 'assets/images/img_json.png';
  static const String jpgImage = 'assets/images/img_jpg.svg';
  static const String docxImage = 'assets/images/img_docx.png';
  static const String downloadIcon = 'assets/images/ic_download.png';
  static const String deleteIcon = 'assets/images/ic_delete.png';
  static const String attendanceinnericon =
      'assets/images/ic_attendancecart.png';
  static const String projectinnericon = 'assets/images/ic_projectcart.png';

  static const String drawerattendanceicon =
      'assets/images/ic_daily_attendance.png';
  static const String drawerleaverecordicon =
      'assets/images/ic_leave_record.png';
  static const String drawerprojectrecordicon =
      'assets/images/ic_project_records.png';
  static const String drawermyresumeicon = 'assets/images/ic_my_resume.png';
  static const String drawerreimbursementicon =
      'assets/images/ic_reimbursements.png';
  static const String draweryourticketsicon =
      'assets/images/ic_your_tickets.png';
  static const String drawerholidaylisticon =
      'assets/images/ic_holiday_list.png';
  static const String drawerdevicerecordicon =
      'assets/images/ic_device_records.png';
  static const String profileicon = 'assets/images/ic_profile.png';

  static const String desktop = 'assets/images/desktop.png';
  static const String holidayList = 'assets/images/holiday_background.png';

}

class Fonts {
  static String AvenirNextLTProRegular = "AvenirNextLTProRegular";
  static String AvenirNextLTProBold = "AvenirNextLTProBold";
  static String AvenirNextLTProIt = "AvenirNextLTProIt";
  static String VAGRoundedRegular = "VAG_Rounded_Regular.ttf";
  static String AvenirNextLTProMedium = "AvenirNextLTProMedium";
  static String AvenirNextLTProDemi = "AvenirNextLTProDemi";
}
