import 'package:flutter/material.dart';

import 'app_constant.dart';


class BaseWidget extends StatefulWidget {


    BaseWidget(
      { Key? key,});

  @override
  _BaseWidgetState createState() => _BaseWidgetState();
}

class _BaseWidgetState extends State<BaseWidget> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: CLR.blackClr.withOpacity(0.2),
      child: Stack(
        children: <Widget>[
          Center(
            child: SizedBox(
              width: 45,
              height: 45,
              child: Image.asset('assets/images/ic_logo.png'),

            ),
          ),
          Center(
            child: SizedBox(
              width: 50,
              height: 50,
              child: CircularProgressIndicator(

               backgroundColor: CLR.lightOrangeColor,
                color: CLR.orangeClr.withOpacity(0.5)
              ),
            ),
          )

        ],
      ),
    );
  }
}
