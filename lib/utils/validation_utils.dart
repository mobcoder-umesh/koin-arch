class Validation {
  static isBlank(String value) {
    if (value == '') {
      print('The field can\'t be empty');
    }
  }

  static bool isNotNull(String? value) {
    return value != null && value != '';
  }
}
