import 'package:shared_preferences/shared_preferences.dart';

import 'validation_utils.dart';




class PreferenceUtil {
  static void saveDeviceIdAndPairingCode(
      String pairingCode, String deviceId) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(deviceId, pairingCode);
  }

  static void setLogin(bool isLogin) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool('is_login', isLogin);
  }

  static void setEmployeeID( String empID) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('emp_id', empID);
  }

  static Future<String?> getEmployeeID() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('emp_id');
  }



  static Future<bool?> isLogin() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool('is_login');
  }

  static void setAccessToken(String? accessToken) async {


    final prefs = await SharedPreferences.getInstance();

    if(Validation.isNotNull(accessToken)){
      print('ACCESS SIGN ${accessToken}');
      prefs.setString('access_token', accessToken!);
    }
  }

  static Future<String?> getAccessToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('access_token');
  }

  // static void setDeviceToken(String deviceToken) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setString('device_token', deviceToken);
  // }
  //
  // static Future<String> getDeviceToken() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   return prefs.getString('device_token');
  // }
  //
  // static void saveUserProfile(UserProfile userProfile) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setString('user_data', jsonEncode(userProfile));
  // }
  //
  // static Future<UserProfile> userProfile() async {
  //   final prefs = await SharedPreferences.getInstance();
  //
  //   UserProfile userProfile = UserProfile();
  //
  //   var data = prefs.getString('user_data');
  //   if (data != null) {
  //     UserProfile userProfile1 = UserProfile.fromJson(jsonDecode(data));
  //     if (userProfile1 == null) {
  //       return userProfile;
  //     } else {
  //       return userProfile1;
  //     }
  //   }
  //   return userProfile;
  // }
  //
  // static void saveGroupsData(JoinGroupResponse userProfile) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setString('group_data', jsonEncode(userProfile));
  // }
  //
  // static Future<JoinGroupResponse> getGroupsData() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   return JoinGroupResponse.fromJson(
  //       jsonDecode(prefs.getString('group_data')));
  // }
  //
  // static void clear() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.clear();
  // }
  //
  // //////////   set bool for show case ///////
  //
  // static void setNoMeal(bool isNoMeal) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setBool('no_meal', isNoMeal);
  // }
  //
  // static void setEditMeal(bool isEdit) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setBool('edit_meal', isEdit);
  // }
  //
  // static void setWhoMeal(bool isWho) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setBool('who_meal', isWho);
  // }
  //
  //
  // static void setGroupChange(bool groupChange) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setBool('group_change', groupChange);
  // }
  //
  // static void setTimeMeal(bool isTime) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setBool('time_meal', isTime);
  // }
  // static Future<bool> isNoMeal() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   return prefs.getBool('no_meal');
  // }
  //
  // static Future<bool> isEditMeal() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   return prefs.getBool('edit_meal');
  // }
  //
  //
  // static Future<bool> isTimeMeal() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   return prefs.getBool('time_meal');
  // }
  //
  // static Future<bool> isGroupChange() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   bool isChange = prefs.getBool('group_change');
  //   if (isChange == null) {
  //     isChange = true;
  //   }
  //   return isChange;
  // }
  //
  //
  // static Future<bool> isWhoMeal() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   return prefs.getBool('who_meal');
  // }
  //
  // static void savePreferenceData(PreferenceModel preferenceData) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setString('prefernce_data', jsonEncode(preferenceData));
  // }
  //
  // static Future<PreferenceModel> getPreferenceData() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   PreferenceModel data = PreferenceModel();
  //   var id = prefs.getString('prefernce_data');
  //   if (id != null) {
  //     PreferenceModel data = PreferenceModel.fromJson(jsonDecode(id));
  //     if (data == null) {
  //       return PreferenceModel();
  //     } else {
  //       return data;
  //     }
  //   }
  //   return data;
  // }
  //
  // static void setHomeUIIndex(int index) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setInt('home_tab_ui', index);
  // }
  //
  // static Future<int> getHomeUIIndex() async {
  //   final prefs = await SharedPreferences.getInstance();
  //   return prefs.getInt('home_tab_ui');
  // }
}
