import 'package:flutter/material.dart';

import 'app_constant.dart';
import 'login_callback.dart';

class LabelWidget {
  static Widget widgetLabel(BuildContext context, String label) {
    return Text(
      label,
      style: const TextStyle(fontSize: 15, color: CLR.blackClr),
      textAlign: TextAlign.start,
    );
  }

  static Widget widgetLabel1(
      BuildContext context, String label, int type, LoginCallback callback) {
    return ElevatedButton(
      child: Text(label),
      onPressed: () {
        callback.getCallBack(type);
      },
    );
  }
}
