


import 'package:intl/intl.dart';

void main(){
 var date =  getFileExtension('jjlkjlk.pdf');
 print(date);

}

String? getFileExtension(String fileName) {
  try {
    return "." + fileName.split('.').last;
  } catch(e){
    return null;
  }
}

class DateUtil{

  static getCurrentDate() {
    var now = new DateTime.now();
    return int.parse(DateFormat('d').format(now));
  }

  static getCurrentDateMillis() {
    var currentDateTime = new DateTime.now();
    currentDateTime = currentDateTime.toLocal();
    var now = DateTime(currentDateTime.year, currentDateTime.month,
        currentDateTime.day, 0, 0, 0, 0, 0);
    return now.millisecondsSinceEpoch;
  }


  static getWeekDay(){
    var currentWeekDay = DateTime.now();
    return DateFormat('EE').format(currentWeekDay);
  }

  static getFullMonthDate(){
    var currentDate = DateTime.now();
    return DateFormat('dd MMM yyyy').format(currentDate);
  }
  static getCurrentTime(){
    var currentTime = DateTime.now();
    return DateFormat('h:mm a').format(currentTime);

  }

  static getWeekStartDate() {
    List<int> dateList = [];
    var now = new DateTime.now();
    now = now.toLocal();

    // set it to feb 10th for testing
    //now = now.add(new Duration(days:7));

    int today = now.weekday;

    // ISO week date weeks start on monday
    // so correct the day number
    var dayNr = (today + 6) % 7;

    // ISO 8601 states that week 1 is the week
    // with the first thursday of that year.
    // Set the target date to the thursday in the target week
    dateList.add(0);
    DateTime d = now.subtract(new Duration(days: (dayNr + 1)));
    for (int i = 0; i < 7; i++) {
      dateList.add(int.parse(
          DateFormat('d').format(d.add(new Duration(days: (i + 1))))));
      print('Date ====> for i =' +
          i.toString() +
          int.parse(DateFormat('d')
              .format(now.subtract(new Duration(days: (dayNr + i - 1)))))
              .toString());
    }

    print('Date list ====> ' + dateList.toString());

    return dateList;
  }

  static String dateFormatter(String savedDateString){
     DateTime tempDate =  DateTime.parse(savedDateString);
    final DateFormat formatter = DateFormat('dd MMM yyyy');
    final String formatted = formatter.format(tempDate);
    return formatted;

  }

  static convertMillisToDate(int millis) {
    return
      DateFormat('M/d').format(DateTime.fromMillisecondsSinceEpoch(millis));
  }

  static convertMillisToDateYYYYMMDD(int millis) {
    return int.parse(
        DateFormat('yyyMMdd').format(DateTime.fromMillisecondsSinceEpoch(millis)));
  }

  static convertMillisToDateDDMMYYYY(int millis) {
    return
        DateFormat('dd MMMM yyyy').format(DateTime.fromMillisecondsSinceEpoch(millis));
  }
  static getCurrentWeekDateMilliSeconds({int? weekNo}) {
    List<int> dateList = [];
    int week = 7 * weekNo!;
    var current = DateTime.now();
    current = current.toLocal();
    var currentDateTime =  current.add(Duration(days: week));
    currentDateTime = currentDateTime.toLocal();
    var now = DateTime(currentDateTime.year, currentDateTime.month,
        currentDateTime.day, 0, 0, 0, 0, 0);
    // set it to feb 10th for testing
    //now = now.add(new Duration(days:7));

    int today = now.weekday;

    // ISO week date weeks start on monday
    // so correct the day number
    var dayNr = (today + 6) % 7;

    // ISO 8601 states that week 1 is the week
    // with the first thursday of that year.
    // Set the target date to the thursday in the target week

    //Adding 0 at first position so that on UI it will show blank box on first place of list
    dateList.add(0);
    DateTime d = now.subtract(new Duration(days: (dayNr + 1)));
    for (int i = 0; i < 7; i++) {
      dateList.add(
          d.add(new Duration(days: (i + 1))).toLocal().millisecondsSinceEpoch);
    }

    print('Date in MilliSeconds list ====> ' + dateList.toString());

    return dateList;
  }



}