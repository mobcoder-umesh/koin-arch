import 'package:flutter/material.dart';

class CommonWidgets{

 static Widget widgetPadding(context,Widget widget,{double? left,double? right,double? top,double? bottom}){
    return Padding(
        padding: EdgeInsets.only(left:left??0.0 ,top:top??0.0,bottom:bottom??0.0 ,right:right??0.0),
        child: widget,
    );
  }


}