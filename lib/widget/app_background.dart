import 'package:flutter/material.dart';

import '../utils/app_constant.dart';

class Background {
  static Widget widgetAppBackground(context, Color color) {
    return Container(
      color: color,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Image.asset(
        IMAGE.appBackground,
        fit: BoxFit.fill,
      ),
    );
  }

  static Widget widgetDrawerBackground(context, String image) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Image.asset(
        IMAGE.drawerBackground,
        fit: BoxFit.fill,
      ),
    );
  }
}



class AppBackground extends StatelessWidget {
  final double? height;

  const AppBackground({Key? key, required this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Image.asset(
            IMAGE.appBackground,
            fit: BoxFit.fill,
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: height,
          decoration: BoxDecoration(
            color: CLR.blueShade1.withOpacity(0.5),
            borderRadius: const BorderRadius.only(
                bottomRight: Radius.circular(40),
                bottomLeft: Radius.circular(40)),
          ),
        )
      ],
    );
  }
}
