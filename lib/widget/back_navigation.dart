import 'package:flutter/material.dart';

import '../utils/app_constant.dart';


class BackNavigation extends StatelessWidget {
  final String pageName;
  const BackNavigation({Key? key,required this.pageName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [

        Padding(
          padding: const EdgeInsets.only(left:27 ,top:56),
          child: InkWell(
            onTap: (){
              Navigator.of(context).pop();
            },
            child: Image.asset(IMAGE.backButton,
            width: 18,
            height: 18,),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(top:56),
              child: Text(pageName,
              style: TextStyle(fontSize: 18,
                  fontFamily: Fonts.AvenirNextLTProDemi,
                  color: CLR.blackClr),),
            ),
          ],
        ),

      ],
    );
  }
}
