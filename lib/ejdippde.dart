import 'package:flutter/material.dart';
void main() => runApp(const ChoiceChipLess());

class ChoiceChipLess extends StatelessWidget {
  const ChoiceChipLess({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ChoiceChipFul(),
    );
  }
}
class ChoiceChipFul extends StatefulWidget {
  const ChoiceChipFul({Key? key}) : super(key: key);

  @override
  _ChoiceChipFulState createState() => _ChoiceChipFulState();
}

class _ChoiceChipFulState extends State<ChoiceChipFul> {
  bool select = false;
  int index1 = 0;
  List<ChoiceChipClass> listOfChips = [
    ChoiceChipClass('Anurag', Colors.lightBlueAccent, Colors.lightBlueAccent,false),
    ChoiceChipClass('Ayush', Colors.yellow, Colors.yellow,false),
    ChoiceChipClass('Pranshi', Colors.lightGreenAccent, Colors.lightGreenAccent,false),
    ChoiceChipClass('Sachin', Colors.red, Colors.redAccent,false),
    ChoiceChipClass('Manish', Colors.deepPurpleAccent, Colors.deepPurpleAccent,false),
    ChoiceChipClass('Ekta', Colors.pink, Colors.pinkAccent,false),
    ChoiceChipClass('Aman', Colors.orangeAccent, Colors.orangeAccent,false),
    ChoiceChipClass('Ankesh', Colors.teal, Colors.tealAccent,false),
    ChoiceChipClass('Abhinav', Colors.blueGrey, Colors.blueGrey,false),
    ChoiceChipClass('Lalit', Colors.brown, Colors.brown,false),
    ChoiceChipClass('Anandita', Colors.deepPurple, Colors.deepPurple,false),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.only(top:60),
        width: 300,
        child: Wrap(
          direction: Axis.horizontal,
          alignment: WrapAlignment.spaceAround,

          spacing: 7,
          runSpacing: 7,
          children: List.generate(11, (index)  {
            return ChoiceChip(
              label: Text(listOfChips[index].label,
                style: const TextStyle(color: Colors.white),),
              selectedColor: listOfChips[index].chipColor,
              elevation: 10,
              backgroundColor: Colors.grey.shade600,

              selected: listOfChips[index].value,
              pressElevation: 20,
              selectedShadowColor: listOfChips[index].chipShadow,
              //shadowColor:Colors.lightBlueAccent ,
              onSelected: (bool value){
                setState(() {
                  listOfChips[index].value =value;

                });
              },
            );
          }).toList(),
        ),
      ),
    );
  }
}

class ChoiceChipClass{
  String label;
  Color chipColor;
  Color chipShadow;
  bool value;


  ChoiceChipClass(this.label, this.chipColor, this.chipShadow, this.value);



}

