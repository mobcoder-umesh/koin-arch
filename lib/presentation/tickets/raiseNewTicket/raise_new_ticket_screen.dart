import 'package:flutter/material.dart';

import 'package:mob_kitchen_freshers/presentation/tickets/tickets_screen.dart';

import '../../../utils/app_constant.dart';
import '../../../widget/app_background.dart';
import '../../../widget/back_navigation.dart';

class RaiseNewTicket extends StatefulWidget {
  const RaiseNewTicket({Key? key}) : super(key: key);

  @override
  State<RaiseNewTicket> createState() => _RaiseNewTicketState();
}

class _RaiseNewTicketState extends State<RaiseNewTicket> {
  final categoryController = TextEditingController();
  final subjectController = TextEditingController();
  final ticketDescriptionController = TextEditingController();
  final List<DropdownMenuItem<String>> _listOfDropMenu = [
    const DropdownMenuItem<String>(
      child: Text('----'),
      value: '----',
    ),
    const DropdownMenuItem<String>(
      child: Text('SOFTWARE'),
      value: 'SOFTWARE',
    ),
    const DropdownMenuItem<String>(
      child: Text('HARDWARE'),
      value: 'HARDWARE',
    ),
    const DropdownMenuItem<String>(
      child: Text('HR RELATED'),
      value: 'HR RELATED',
    ),
    const DropdownMenuItem<String>(
      child: Text('OTHERS'),
      value: 'OTHERS',
    ),
  ];

  // String dropDownValue = '----';
  // var items = [
  //   '----'
  //   'SOFTWARE',
  //   'HARDWARE',
  //   'HR RELATED',
  //   'OTHERS',
  //     ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Background.widgetAppBackground(context, CLR.whiteClr),
            const AppBackground(height: 220),
            Column(
              children: [
                const BackNavigation(pageName: "Claim Raise"),
                formFill(),
              ],
            ),
            submitButton(context),
          ],
        ),
      ),
    );
  }

  Widget formFill() {
    return Container(
        margin: const EdgeInsets.only(top: 15, left: 20, right: 20, bottom: 93),
        height: 400,
        width: 335,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: CLR.whiteClr,
            boxShadow: const [
              BoxShadow(color: Color(0xffE6E6E6), offset: Offset(1, 1)),
              BoxShadow(color: Color(0xffE6E6E6), offset: Offset(-1, -1))
            ]),
        child: Column(children: [
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 20, left: 20),
            child: Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(
                top: 17.76,
              ),
              child: Text(
                "Category",
                style: TextStyle(
                    fontFamily: Fonts.AvenirNextLTProMedium, fontSize: 14),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 6.95, left: 18, right: 18),
            child: DropdownButtonFormField(
              items: _listOfDropMenu,
              onChanged: (String? value) {
                _selectCategory = value;
              },
              decoration: const InputDecoration(
                  label: Text('Select Category'),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    borderSide: BorderSide(
                      color: Colors.black,
                    ),
                  ),
                  hintText: "Select Category"),
            ),
          ),
          // Container(
          //   margin: EdgeInsets.only(top: 6.95, left: 18, right: 18),
          //   child: TextFormField(
          //     controller: categoryController,
          //     decoration: InputDecoration(
          //       hintText: "Select Category",
          //       border: OutlineInputBorder(
          //         borderRadius: BorderRadius.all(Radius.circular(10)),
          //         borderSide: BorderSide(
          //           color: Colors.black,
          //         ),
          //       ),
          //       suffixIcon: IconButton(
          //         icon: Icon(
          //           Icons.arrow_drop_down_sharp,size: 50,
          //         ),
          //         onPressed: () {
          //          DropButton();
          //         },
          //       ),
          //     ),
          //   ),
          // ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 17.76, left: 18),
            child: Text(
              "Subject",
              style: TextStyle(
                  fontFamily: Fonts.AvenirNextLTProMedium, fontSize: 14),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 6.95, left: 18, right: 18),
            child: TextFormField(
              controller: subjectController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 17.76, left: 18),
            child: Text(
              "Ticket Description",
              style: TextStyle(
                  fontFamily: Fonts.AvenirNextLTProMedium, fontSize: 14),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 6.95, left: 18, right: 18),
            child: TextFormField(
              controller: ticketDescriptionController,
              maxLines: 3,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
        ]));
  }

  String? _selectCategory = 'Select Category';

  Widget submitButton(context) {
    return Container(
      width: 194,
      height: 50,
      margin: const EdgeInsets.only(
          left: 90.5, top: 465, right: 90.5, bottom: 68.38),
      child: TextButton(
        style: TextButton.styleFrom(
          primary: CLR.whiteClr,
          backgroundColor: CLR.maharoonClr,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25)),
          ),
        ),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const Tickets()));
        },
        child: Text(
          "Submit",
          style: TextStyle(
            fontFamily: Fonts.AvenirNextLTProMedium,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
// DropButton() {
//  BlocBuilder<RaiseNewTicketCubit,RaiseNewTicketState>(
//    builder: (context, state) {
//      return DropdownButton(
//        value:state.category,
//        icon: Icon(Icons.keyboard_arrow_down),
//        items: items.map((String items) {
//          return DropdownMenuItem(
//            value: items,
//            child: Text(items),
//          );
//        }).toList(),
//        onChanged: (String? newValue) {
//          print('hjbbhb======>${newValue}');
//
//          BlocProvider.of<RaiseNewTicketCubit>(context).onCategorySelect(newValue??'');
//        },
//      );
//    },
//  );
// }

}
