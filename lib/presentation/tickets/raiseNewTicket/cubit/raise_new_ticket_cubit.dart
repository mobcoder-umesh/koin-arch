import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'raise_new_ticket_state.dart';

class RaiseNewTicketCubit extends Cubit<RaiseNewTicketState> {
  RaiseNewTicketCubit() : super(SubmitInitial(false, false, false));

  void onCategoryChange(String category) {
    if (category != null && category.isNotEmpty) {
      print('category correct $category');
      emit(SubmitCredentialsState(
          true, state.isSubjectValid, state.isTicketDescriptionValid));
    } else {
      print('category incorrect $category');
      emit(SubmitCredentialsState(
          false, state.isSubjectValid, state.isTicketDescriptionValid));
    }
  }

  void onSubjectChange(String subject) {
    if (subject != null && subject.isNotEmpty) {
      print('subject correct $subject');
      emit(SubmitCredentialsState(
          state.isCategoryValid, true, state.isTicketDescriptionValid));
    } else {
      print('subject incorrect $subject');
      emit(SubmitCredentialsState(
          state.isCategoryValid, false, state.isTicketDescriptionValid));
    }
  }

  void onTicketDescriptionChange(String ticketDescription) {
    if (ticketDescription != null && ticketDescription.isNotEmpty) {
      print('ticketDescription correct $ticketDescription');
      emit(SubmitCredentialsState(
          state.isCategoryValid, state.isSubjectValid,
          true));
    } else {
      print('ticketDescription incorrect $ticketDescription');
      emit(SubmitCredentialsState(
          state.isCategoryValid, state.isSubjectValid,
          false));
    }
  }
  // void onSubmit(String category,String subject,String ticketDescription,String errorMessage)async{
  // final RaiseNewTicketRepository repository = RaiseNewTicketRepository();
  // RaiseNewTicketResponse responseData = await repository.getClaimRaiseResponse(category, subject, ticketDescription);
  // if (responseData.statusCode == 1){
  //   emit(SubmitSuccessState(state.isCategoryValid,state.isSubjectValid,state.isTicketDescriptionValid,category, subject, ticketDescription));
  // }else{
  //   emit(SubmitErrorState(state.isCategoryValid,state.isSubjectValid,state.isTicketDescriptionValid,errorMessage));
  // }
//}

}