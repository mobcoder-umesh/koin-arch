part of 'raise_new_ticket_cubit.dart';

abstract class RaiseNewTicketState extends Equatable {
  final bool isCategoryValid;
  final bool isSubjectValid;
  final bool isTicketDescriptionValid;


  RaiseNewTicketState(this.isCategoryValid, this.isSubjectValid,
      this.isTicketDescriptionValid);

  @override
  List<Object> get props => [
        isCategoryValid,
        isSubjectValid,
        isTicketDescriptionValid,
      ];
}

class SubmitInitial extends RaiseNewTicketState {
  SubmitInitial(bool isCategoryValid, bool isSubjectValid,
      bool isTicketDescriptionValid)
      : super(isCategoryValid, isSubjectValid, isTicketDescriptionValid,
           );

// @override
// List<Object?> get props => [];
}

class SubmitCredentialsState extends RaiseNewTicketState {
  SubmitCredentialsState(bool isCategoryValid, bool isSubjectValid,
      bool isTicketDescriptionValid)
      : super(isCategoryValid, isSubjectValid, isTicketDescriptionValid,
           );
}

class SubmitSuccessState extends RaiseNewTicketState {
  final String category;
  final String subject;
  final String ticketDescription;

  SubmitSuccessState(
      bool isCategoryValid,
      bool isSubjectValid,
      bool isTicketDescriptionValid,
      this.category,
      this.subject,
      this.ticketDescription)
      : super(isCategoryValid, isSubjectValid, isTicketDescriptionValid,
            );

// @override
// List<Object?> get props => [email];

}

class SubmitErrorState extends RaiseNewTicketState {
  final String errorMessage;

  SubmitErrorState(bool isCategoryValid, bool isSubjectValid,
      bool isTicketDescriptionValid, this.errorMessage)
      : super(isCategoryValid, isSubjectValid, isTicketDescriptionValid,
           );

  @override
  List<Object> get props => [errorMessage];
}

