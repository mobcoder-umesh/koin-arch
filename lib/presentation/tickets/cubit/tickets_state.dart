import 'package:equatable/equatable.dart';

import '../../../network/model/tickets/ticket_response.dart';

abstract class TicketsState extends Equatable {
  List<TicketDetails> ticketDetails;

  TicketsState(this.ticketDetails);
}

class TicketsInitial extends TicketsState {
  TicketsInitial(List<TicketDetails> ticketDetails) : super(ticketDetails);

  @override
  List<Object> get props => [];
}

class TicketsLoading extends TicketsState {
  TicketsLoading(List<TicketDetails> ticketDetails) : super(ticketDetails);

  @override
  List<Object?> get props => [];
}

class TicketsLoaded extends TicketsState {
  TicketsLoaded(List<TicketDetails> ticketDetails) : super(ticketDetails);

  @override
  List<Object?> get props => [TicketDetails()];
}

class TicketsError extends TicketsState {
  TicketsError(List<TicketDetails> ticketDetails) : super(ticketDetails);

  @override
  List<Object?> get props => [];
}
