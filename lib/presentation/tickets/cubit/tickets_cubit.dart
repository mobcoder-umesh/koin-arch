
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mob_kitchen_freshers/domain/repositories/ticket_repository.dart';
import 'package:mob_kitchen_freshers/network/model/tickets/ticket_response.dart';

import 'tickets_state.dart';





class TicketsCubit extends Cubit<TicketsState> {

  TicketsCubit() : super(TicketsInitial(const [])) {
    getTicketData();
  }

  void getTicketData() async {
    final TicketsRepository repository = TicketsRepository();
    try {
      emit(TicketsLoading(state.ticketDetails));
      TicketResponse response =
      (await repository.getTicketDetails());

      List<TicketDetails> listOfTicket =
          response.responseData?.ticketDetails ?? [];

      emit(TicketsLoaded(listOfTicket));
    } catch (e) {
      emit(TicketsError(state.ticketDetails));
    }
  }

}