import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mob_kitchen_freshers/network/model/tickets/ticket_response.dart';

import 'package:mob_kitchen_freshers/presentation/tickets/cubit/tickets_cubit.dart';
import 'package:mob_kitchen_freshers/presentation/tickets/cubit/tickets_state.dart';

import '../../utils/app_constant.dart';
import '../../utils/date_utils.dart';
import '../../widget/app_background.dart';
import '../../widget/back_navigation.dart';
import 'raiseNewTicket/raise_new_ticket_screen.dart';

class Tickets extends StatefulWidget {
  const Tickets({Key? key}) : super(key: key);

  @override
  State<Tickets> createState() => _TicketsState();
}

class _TicketsState extends State<Tickets> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => TicketsCubit(),
      child: Scaffold(
        body: Stack(
          children: [
            Background.widgetAppBackground(context, CLR.whiteClr),
            const AppBackground(height: 250),
            Column(
              children: [
                const BackNavigation(pageName: "Your Tickets"),
                raiseNewTicketButton(context),
                BlocBuilder<TicketsCubit, TicketsState>(
                  builder: (context, state) {
                    return Expanded(
                      child: ListView.builder(
                          itemCount: state.ticketDetails.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ticketContainer(
                                context, state.ticketDetails, index);
                          }),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget raiseNewTicketButton(context) {
    return Container(
      width: 216,
      height: 50,
      margin: const EdgeInsets.only(left: 38, top: 20, right: 38, bottom: 10),
      child: TextButton(
        style: TextButton.styleFrom(
          primary: CLR.whiteClr,
          backgroundColor: CLR.maharoonClr,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25)),
          ),
        ),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const RaiseNewTicket()));
        },
        child: Text(
          "Raise New Ticket",
          style: TextStyle(
            fontFamily: Fonts.AvenirNextLTProMedium,
            fontSize: 16,
          ),
        ),
      ),
    );
  }

  Widget ticketContainer(context, List<TicketDetails> list, int index) {
    return Container(
      height: 155,
      width: MediaQuery.of(context).size.width,
      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Color(0xffE6E6E6), width: 2),
        boxShadow: const [
          BoxShadow(
            color: Color(0xffE6E6E6),
            offset: Offset(
              2.0,
              2.0,
            ),
          ),
          BoxShadow(
            color: Color(0xffE6E6E6),
            offset: Offset(
              -2.0,
              -2.0,
            ),
          ),
        ],
        color: Colors.white,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 22, bottom: 4, left: 21),
            child: Text(
              list[index].subject ?? '',
              style: TextStyle(
                  fontSize: 17,
                  fontFamily: Fonts.AvenirNextLTProDemi,
                  color: Color(0xff6C1B03)),
            ),
          ),
          Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 95,
                  margin: EdgeInsets.only(top: 4, bottom: 4, left: 21),
                  child: Text(
                    category(list[index].category ?? 0),
                    style: TextStyle(
                        fontSize: 14,
                        fontFamily: Fonts.AvenirNextLTProMedium,
                        color: Colors.black),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: statusColor(list[index].status ?? 0),
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(
                        color: statusColor(list[index].status ?? 0), width: 1),
                  ),
                  height: 23,
                  width: 80,
                  margin: EdgeInsets.only(top: 5, left: 120, right: 20),
                  child: Center(
                    child: Text(status(list[index].status ?? 0),
                        style: TextStyle(
                          fontSize: 10,
                          fontFamily: Fonts.AvenirNextLTProMedium,
                          color: textColor(list[index].status ?? 0),
                        )),
                  ),
                )
              ]),
          Container(
            margin: EdgeInsets.only(top: 5, bottom: 4, left: 21),
            child: Text(
              DateUtil.dateFormatter(list[index].created.toString()),
              style: TextStyle(
                fontSize: 13,
                fontFamily: Fonts.AvenirNextLTProMedium,
                color: Color(0xff6B6B6B),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 5, left: 23),
            child: Text(
              list[index].description.toString(),
              style: TextStyle(
                  fontSize: 14,
                  fontFamily: Fonts.AvenirNextLTProMedium,
                  color: Colors.black),
            ),
          )
        ],
      ),
    );
  }

  String status(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return 'Pending';
      case 2:
        return 'Claim Settled';
      case 3:
        return 'Rejected';
      default:
        return 'NOT CLEAR';
    }
  }

  Color statusColor(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return Color(0xffFFD5A5);
      case 2:
        return Color(0xffC9ECBA);
      case 3:
        return CLR.light_redClr;
      default:
        return CLR.light_greyClr;
    }
  }

  Color textColor(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return Color(0xffFD6121);
      case 2:
        return Color(0xff59AC00);
      case 3:
        return CLR.redClr;
      default:
        return CLR.whiteClr;
    }
  }

  String category(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return 'SOFTWARE';
      case 2:
        return 'HARDWARE';
      case 3:
        return 'HR RELATED';
      case 4:
        return 'OTHERS';
      default:
        return 'NOT CLEAR';
    }
  }
}
