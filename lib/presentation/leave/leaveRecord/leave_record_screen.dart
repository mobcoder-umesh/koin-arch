import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../utils/app_constant.dart';
import '../../../utils/date_utils.dart';
import '../../../widget/app_background.dart';
import '../../../widget/back_navigation.dart';
import '../../../widget/common_widgets.dart';
import '../../dailyAttendence/widget.dart';
import 'cubit/leave_record_cubit.dart';
import 'cubit/leave_record_state.dart';

class LeaveRecordFul extends StatefulWidget {
  const LeaveRecordFul({Key? key}) : super(key: key);

  @override
  State<LeaveRecordFul> createState() => _LeaveRecordFulState();
}

List<ChoiceItem> _chipsList = [
  ChoiceItem("ALL LEAVES", true),
  ChoiceItem("PENDING", false),
  ChoiceItem("APPROVED", false),
  ChoiceItem("REJECTED", false),
];

class _LeaveRecordFulState extends State<LeaveRecordFul> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => LeaveRecordCubit(_chipsList),
      child: Scaffold(
        body: Stack(
          children: [
            const AppBackground(height: 327),
            BlocBuilder<LeaveRecordCubit, LeaveRecordState>(
              builder: (context, state) {
                var data = state.leaveRecordResponse.responseData;
                return Column(
                  children: [
                    const BackNavigation(pageName: AppConstants.leaveRecord),
                    const SizedBox(height: 30),
                    Wrap(
                      alignment: WrapAlignment.center,
                      runSpacing: 6,
                      spacing: 6,
                      children: [
                        widgetLeaveCard('${data?.totalCasualLeaveTaken ?? 0}/6',
                            AppConstants.casual, const Color(0xff56B4E1), 82),
                        widgetLeaveCard('${data?.totalEarnedLeaveTaken ?? 0}/6',
                            AppConstants.earned, const Color(0xff1F99D2), 82),
                        widgetLeaveCard('${data?.totalSickLeaveTaken ?? 0}/6',
                            AppConstants.sick, const Color(0xff2C8EBD), 82),
                        widgetLeaveCard(
                            '${data?.totalComOffTaken ?? 0}',
                            AppConstants.compensatory,
                            const Color(0xff56B4E1),
                            82),
                        widgetLeaveCard(
                            '${data?.totalLeaves ?? 0}',
                            AppConstants.markedLWP,
                            const Color(0xff1F99D2),
                            82),
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 20, top: 20),
                      height: 50,
                      child: ListView.builder(
                          itemCount: state.list.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (BuildContext context, int index) {
                            return LeaveStatusChoice(
                              choiceName: state.list[index].name,
                              isSelected: state.list[index].isSelected,
                              index: index,
                            );
                          }),
                    ),
                    Flexible(
                      child: ListView.builder(
                        itemCount: data?.leaveDetails?.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            margin: const EdgeInsets.only(
                                top: 5, left: 20, right: 20),
                            padding: const EdgeInsets.only(top: 10),
                            width: 335,
                            height: 131,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15),
                                boxShadow: const [
                                  BoxShadow(
                                      color: Colors.black12, blurRadius: 2)
                                ]),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                widgetDateAttendence(
                                    context,
                                    status(
                                        data?.leaveDetails?[index].leaveType ??
                                            0),
                                    17,
                                    Fonts.AvenirNextLTProBold,
                                    CLR.maharoonClr,
                                    63,
                                    containerColor(
                                        data?.leaveDetails?[index].status ?? 0),
                                    borderColor(
                                        data?.leaveDetails?[index].status ?? 0),
                                    leaveStatus(
                                        data?.leaveDetails?[index].status ?? 0),
                                    textColor(
                                        data?.leaveDetails?[index].status ??
                                            0)),
                                widgetDateDetails(
                                    context,
                                    DateUtil.convertMillisToDateDDMMYYYY(
                                        data?.leaveDetails?[index].startDate ??
                                            0),
                                    DateUtil.convertMillisToDateDDMMYYYY(
                                        data?.leaveDetails?[index].endDate ??
                                            0),
                                    17,
                                    17),
                                widgetDateDetails(
                                    context,
                                    AppConstants.startDate,
                                    AppConstants.endDate,
                                    13,
                                    13),
                                CommonWidgets.widgetPadding(
                                  context,
                                  Container(
                                    margin:
                                        const EdgeInsets.only(top: 6, left: 20),
                                    child: Text(
                                      data?.leaveDetails?[index].leaveMsg ?? '',
                                      textAlign: TextAlign.left,
                                      style: const TextStyle(
                                          fontSize: 13,
                                          color: Color(0xffAAAAAA),
                                          fontStyle: FontStyle.italic),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                );
              },
            )
          ],
        ),
      ),
    );
  }

  String status(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return AppConstants.casualLeave;
      case 2:
        return AppConstants.earlyLeave;
      case 3:
        return AppConstants.shortLeave;
      case 4:
        return AppConstants.lWPLeave;
      case 5:
        return AppConstants.comOff;
      case 6:
        return AppConstants.halfCL;
      case 7:
        return AppConstants.halfEL;
      case 8:
        return AppConstants.halfSL;
      case 9:
        return AppConstants.halfLWP;
      default:
        return AppConstants.notClear;
    }
  }

  String leaveStatus(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return 'Pending';
      case 2:
        return 'Approved';
      case 3:
        return 'Rejected';
      case 4:
        return 'Unapproved';
      default:
        return 'NOT CLEAR';
    }
  }

  Color containerColor(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return CLR.light_container_greyClr;
      case 2:
        return CLR.lightGreenShade1;
      case 3:
        return CLR.light_container_peachClr;
      case 4:
        return CLR.light_container_orangeClr;
      default:
        return CLR.light_container_greyClr;
    }
  }

  Color borderColor(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return CLR.unselectedChoiceColor;
      case 2:
        return CLR.lightGreenShade2;
      case 3:
        return CLR.lightRedShade1;
      case 4:
        return CLR.light_border_orangeClr;
      default:
        return CLR.unselectedChoiceColor;
    }
  }

  Color textColor(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return CLR.greyShade;
      case 2:
        return CLR.lightGreenShade3;
      case 3:
        return CLR.redShade;
      case 4:
        return CLR.orangeShade;
      default:
        return CLR.greyShade;
    }
  }
}

class LeaveStatusChoice extends StatelessWidget {
  final String choiceName;
  final bool isSelected;
  final int index;

  const LeaveStatusChoice(
      {Key? key,
      required this.choiceName,
      required this.isSelected,
      required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        List<ChoiceItem> listOfChoice = _chipsList.map((e) {
          if (e.name == _chipsList[index].name) {
            e.isSelected = true;
            return e;
          } else {
            e.isSelected = false;
            return e;
          }
        }).toList();
        BlocProvider.of<LeaveRecordCubit>(context)
            .onLeaveStatusChange(listOfChoice, _chipsList[index].name);
      },
      child: Container(
        margin: const EdgeInsets.only(right: 10),
        height: 44,
        decoration: BoxDecoration(
            color: CLR.whiteClr,
            borderRadius: BorderRadius.circular(50),
            border: Border.all(
                color:
                    isSelected ? CLR.maharoonClr : CLR.unselectedChoiceColor)),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(left: 25, right: 25),
            child: Text(
              choiceName,
              style: TextStyle(
                fontSize: 14,
                color: isSelected ? CLR.maharoonClr : CLR.blackClr,
                fontFamily: Fonts.AvenirNextLTProDemi,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ChoiceItem {
  String name;
  bool isSelected;

  ChoiceItem(this.name, this.isSelected);
}

Widget widgetDateDetails(
  context,
  String text1,
  String text2,
  double? size1,
  double? size2,
) {
  return CommonWidgets.widgetPadding(
      context,
      Row(
        children: [
          Text(
            text1,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: size1,
            ),
          ),
          const Spacer(
            flex: 1,
          ),
          Text(
            text2,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: size2,
            ),
          ),
          const Spacer(
            flex: 1,
          ),
        ],
      ),
      left: 20,
      right: 65,
      top: 5);
}
