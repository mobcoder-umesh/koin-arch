// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';
import '../../../../network/model/leave/leaveRecord/leave_record_response.dart';
import '../leave_record_screen.dart';

abstract class LeaveRecordState extends Equatable {
  LeaveRecordResponse leaveRecordResponse;
  List<ChoiceItem> list;

  LeaveRecordState(this.leaveRecordResponse, this.list);
}

class LeaveRecordInitial extends LeaveRecordState {
  LeaveRecordInitial(
      LeaveRecordResponse leaveRecordResponse, List<ChoiceItem> list)
      : super(leaveRecordResponse, list);

  @override
  List<Object> get props => [];
}

class LeaveRecordStatusChangeState extends LeaveRecordState {
  LeaveRecordStatusChangeState(
      LeaveRecordResponse leaveRecordResponse, List<ChoiceItem> list)
      : super(leaveRecordResponse, list);

  @override
  List<Object?> get props => [];
}

class LeaveRecordLoadingState extends LeaveRecordState {
  LeaveRecordLoadingState(
      LeaveRecordResponse leaveRecordResponse, List<ChoiceItem> list)
      : super(leaveRecordResponse, list);

  @override
  List<Object?> get props => [];
}

class LeaveRecordLoadedState extends LeaveRecordState {
  LeaveRecordLoadedState(
      LeaveRecordResponse leaveRecordResponse, List<ChoiceItem> list)
      : super(leaveRecordResponse, list);

  @override
  List<Object?> get props => [];
}

class LeaveRecordErrorState extends LeaveRecordState {
  LeaveRecordErrorState(
      LeaveRecordResponse leaveRecordResponse, List<ChoiceItem> list)
      : super(leaveRecordResponse, list);

  @override
  List<Object?> get props => [];
}
