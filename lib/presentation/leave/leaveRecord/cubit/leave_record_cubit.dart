// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import '../../../../domain/repositories/leave_repository.dart';
import '../../../../network/model/leave/leaveRecord/leave_record_response.dart';
import '../../../../network/model/leave/leaveRecord/model/LeaveDetails.dart';
import '../leave_record_screen.dart';
import 'leave_record_state.dart';

class LeaveRecordCubit extends Cubit<LeaveRecordState> {
  LeaveRecordCubit(List<ChoiceItem> globalList)
      : super(LeaveRecordInitial(LeaveRecordResponse(), globalList)) {
    getLeaveRecordData();
  }

  List<ChoiceItem> globalList = [
    ChoiceItem("ALL LEAVES", true),
    ChoiceItem("PENDING", false),
    ChoiceItem("APPROVED", false),
    ChoiceItem("REJECTED", false),
  ];

  void getLeaveRecordData() async {
    final LeaveRecordRepository repository = LeaveRecordRepository();
    try {
      print('response');
      emit(LeaveRecordLoadingState(state.leaveRecordResponse, state.list));
      print('response Done');

      LeaveRecordResponse leaveRecordResponse =
          (await repository.getLeaveRecord());

      emit(LeaveRecordLoadedState(leaveRecordResponse, state.list));
    } catch (e) {
      print('error');
      emit(LeaveRecordErrorState(state.leaveRecordResponse, state.list));
    }
  }

  void onLeaveStatusChange(List<ChoiceItem> list, String optionName) {
    List<LeaveDetails> list1;

    print('Inside Function');

    if (optionName == 'ALL LEAVES') {
      print('Inside All');
      emit(LeaveRecordStatusChangeState(state.leaveRecordResponse, list));
      emit(LeaveRecordLoadedState(state.leaveRecordResponse, list));
    } else {
      print('Inside $optionName');

      list1 = state.leaveRecordResponse.responseData!.leaveDetails!
          .where((e) => getStatus1(e.status!) == optionName)
          .toList();
      print('Filter list =====>>>>   ${list1.toString()}');
      emit(LeaveRecordStatusChangeState(state.leaveRecordResponse, list));
      emit(LeaveRecordLoadedState(state.leaveRecordResponse, state.list));
    }
  }

  String getStatus1(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return 'PENDING';

      case 2:
        return 'APPROVED';

      case 3:
        return 'REJECTED';

      default:
        return 'ALL LEAVES';
    }
  }
}
