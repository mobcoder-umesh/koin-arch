import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mob_kitchen_freshers/utils/app_constant.dart';
import 'package:mob_kitchen_freshers/widget/app_background.dart';

import '../../../widget/back_navigation.dart';

class applyleavepage extends StatefulWidget {
  const applyleavepage({Key? key}) : super(key: key);

  @override
  State<applyleavepage> createState() => _applyleavepageState();
}

class _applyleavepageState extends State<applyleavepage> {
  TextEditingController _dobController = TextEditingController();
  TextEditingController _dobController1 = TextEditingController();
  final List<DropdownMenuItem<String>> _listOfDropMenu = [
    const DropdownMenuItem<String>(
      child: Text('----'),
      value: '----',
    ),
    const DropdownMenuItem<String>(
      child: Text('CL'),
      value: 'CL',
    ),
    const DropdownMenuItem<String>(
      child: Text('EL'),
      value: 'EL',
    ),
    const DropdownMenuItem<String>(
      child: Text('SL'),
      value: 'SL',
    ),
    const DropdownMenuItem<String>(
      child: Text('LWP'),
      value: 'LWP',
    ),
    const DropdownMenuItem<String>(
      child: Text('Com off'),
      value: 'Com off',
    ),
    const DropdownMenuItem<String>(
      child: Text('Half Cl'),
      value: 'Half Cl',
    ),
    const DropdownMenuItem<String>(
      child: Text('Half El'),
      value: 'Half El',
    ),
    const DropdownMenuItem<String>(
      child: Text('Half Sl'),
      value: 'Half Sl',
    ),
    const DropdownMenuItem<String>(
      child: Text('Half LWP'),
      value: 'Half LWP',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            AppBackground(
              height: MediaQuery.of(context).size.height,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BackNavigation(pageName: "Apply Leave"),
                TextHeading(
                    'Leave Type', 14, EdgeInsets.only(left: 38, top: 40)),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 55,
                  margin: EdgeInsets.only(top: 6.95, left: 38, right: 38),
                  child: DropdownButtonFormField(
                    items: _listOfDropMenu,
                    onChanged: (String? value) {
                      _selectCategory = value;
                    },
                    decoration: const InputDecoration(
                        filled: true,
                        fillColor: CLR.whiteClr,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                          borderSide: BorderSide(
                            color: Colors.black,
                          ),
                        ),
                        hintText: "Select "),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 38, right: 38, top: 28),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              //margin: EdgeInsets.only(top: 28.05, left: 38),
                              child: Text(
                                "Start Date",
                                style: TextStyle(
                                    fontFamily: Fonts.AvenirNextLTProMedium,
                                    fontSize: 14),
                              ),
                            ),
                            SizedBox(
                              height: 7,
                            ),
                            Container(
                              height: 55,

                              //margin: EdgeInsets.only(top: 7, left: 38,),
                              child: TextFormField(
                                controller: _dobController,
                                decoration: InputDecoration(
                                  hintText: 'Select',
                                  filled: true,
                                  fillColor: CLR.whiteClr,
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      Icons.calendar_month,
                                    ),
                                    onPressed: () async {
                                      DateTime selected = DateTime(1900);
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                      selected = (await showDatePicker(
                                          context: context,
                                          initialDate: DateTime.now(),
                                          firstDate: DateTime(1900),
                                          lastDate: DateTime(2100)))!;
                                      _dobController.text = DateFormat(
                                        'dd/MM/yyyy',
                                      ).format(selected);
                                    },
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 19,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              //margin: EdgeInsets.only(top: 28.05, right: 38),
                              child: Text(
                                "End Date",
                                style: TextStyle(
                                    fontFamily: Fonts.AvenirNextLTProMedium,
                                    fontSize: 14),
                              ),
                            ),
                            SizedBox(
                              height: 7,
                            ),
                            Container(
                              height: 55,

                              //margin: EdgeInsets.only(top: 7, right: 38),
                              child: TextFormField(
                                controller: _dobController1,
                                decoration: InputDecoration(
                                  hintText: 'Select',
                                  filled: true,
                                  fillColor: CLR.whiteClr,
                                  suffixIcon: IconButton(
                                    icon: Icon(
                                      Icons.calendar_month,
                                    ),
                                    onPressed: () async {
                                      DateTime selected = DateTime(1900);
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                      selected = (await showDatePicker(
                                          context: context,
                                          initialDate: DateTime.now(),
                                          firstDate: DateTime(1900),
                                          lastDate: DateTime(2100)))!;
                                      _dobController1.text = DateFormat(
                                        'dd/MM/yyyy',
                                      ).format(selected);
                                    },
                                  ),
                                  border: OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    borderSide: BorderSide(
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20.05, left: 38, right: 38),
                  child: Text(
                    "Give Reason",
                    style: TextStyle(
                        fontFamily: Fonts.AvenirNextLTProMedium, fontSize: 14),
                  ),
                ),
                Container(
                  height: 112,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(top: 7, left: 38, right: 38),
                  child: TextFormField(
                    maxLines: 3,
                    decoration: InputDecoration(
                      hintText: 'Give Your Reason',
                      filled: true,
                      fillColor: CLR.whiteClr,
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                ApplyLeaveButton(context),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget TextHeading(String text, double FontSize, EdgeInsets? margin) {
  return Container(
    margin: margin,
    child: Text(
      text,
      style: TextStyle(
        fontFamily: Fonts.AvenirNextLTProMedium,
        fontSize: FontSize,
      ),
    ),
  );
}

String? _selectCategory = 'Select Category';

Widget ApplyLeaveButton(context) {
  return Container(
    width: MediaQuery.of(context).size.width,
    height: 50,
    margin: const EdgeInsets.only(left: 38, top: 282.95, right: 38),
    child: TextButton(
      style: TextButton.styleFrom(
        primary: CLR.whiteClr,
        backgroundColor: CLR.maharoonClr,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
        ),
      ),
      onPressed: () {},
      child: Text(
        "Apply",
        style: TextStyle(
          fontFamily: Fonts.AvenirNextLTProMedium,
          fontSize: 16,
        ),
      ),
    ),
  );
}
