import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:mob_kitchen_freshers/network/model/vendor/vendor_response.dart';

import '../../../domain/repositories/sample_dashboard_repository.dart';
import '../../../network/model/userProfile/user_profile_response.dart';
import 'sample_dashboard_state.dart';



class DashboardCubit extends Cubit<DashboardState> {
  DashboardCubit() : super(DashboardInitial(true,[],UserProfileResponse())){
    getDashboardData();
  }



  void getDashboardData()async{
    final DashboardRepository repository = DashboardRepository() ;
    try {

      emit(DashboardLoading(state.isDeliveryCard,state.vendorsList,state.userProfileResponse));
      VendorResponse response = (await repository.getVendorList());
      UserProfileResponse profileResponse =(await repository.getUserProfileData());
      List<VendorList> listOfVendors = response.responseData?.vendorList??[];
      emit(DashBoardLoaded(state.isDeliveryCard,listOfVendors,profileResponse));
    } catch (e) {
        emit(DashBoardError(state.isDeliveryCard,state.vendorsList,state.userProfileResponse));
    }
  }

  void onDeliveryCardVisibilityChange(){

    emit(DismissDeliveryCard(false,state.vendorsList,state.userProfileResponse));
    //emit(DashBoardLoaded(state.isDeliveryCard,state.,profileResponse));

   // emit(DashBoardLoaded(state.isDeliveryCard,listOfVendors));


  }
}
