

import 'package:equatable/equatable.dart';

import '../../../network/model/userProfile/user_profile_response.dart';
import '../../../network/model/vendor/vendor_response.dart';

abstract class DashboardState extends Equatable {
  bool isDeliveryCard;
  List<VendorList> vendorsList;
  UserProfileResponse userProfileResponse;

  DashboardState(this.isDeliveryCard,this.vendorsList,this.userProfileResponse);
}

class DashboardInitial extends DashboardState {
  DashboardInitial(bool isDeliveryCard, List<VendorList> vendorsList, UserProfileResponse userProfileResponse) : super(isDeliveryCard, vendorsList, userProfileResponse);



  @override
  List<Object> get props => [];
}

class DashboardLoading extends DashboardState{
  DashboardLoading(bool isDeliveryCard, List<VendorList> vendorsList, UserProfileResponse userProfileResponse) : super(isDeliveryCard, vendorsList, userProfileResponse);



  @override
  List<Object?> get props => [];

}

class DashBoardLoaded extends DashboardState{
  DashBoardLoaded(bool isDeliveryCard, List<VendorList> vendorsList, UserProfileResponse userProfileResponse) : super(isDeliveryCard, vendorsList, userProfileResponse);




  @override
  List<Object?> get props => [vendorsList];

}

class DismissDeliveryCard extends DashboardState{
  DismissDeliveryCard(bool isDeliveryCard, List<VendorList> vendorsList, UserProfileResponse userProfileResponse) : super(isDeliveryCard, vendorsList, userProfileResponse);



  @override
  List<Object?> get props => [];



}

class DashBoardError extends DashboardState{
  DashBoardError(bool isDeliveryCard, List<VendorList> vendorsList, UserProfileResponse userProfileResponse) : super(isDeliveryCard, vendorsList, userProfileResponse);



  @override
  List<Object?> get props => [];

}
