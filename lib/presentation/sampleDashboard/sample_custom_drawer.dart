import 'package:flutter/material.dart';
import '../../localization/i18n.dart';
import '../../utils/app_constant.dart';
import '../../utils/preference_util.dart';
import '../../widget/app_background.dart';
import '../../widget/common_widgets.dart';
enum SourceTypeItem{
  myOrders,
  myWallet,
  aboutUs

}
class CustomDrawer{
  static Widget widgetShowDrawer(context,String email,String phone,int walletPrice,int couponWalletPrice){
    return SizedBox(
      width: 345,
      child: Drawer(
        backgroundColor: CLR.brownDarkClr,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30),
              bottomRight: Radius.circular(30)),
        ),
        child: Stack(
          children: [
            Background.widgetDrawerBackground(context,IMAGE.drawerBackground),
            CommonWidgets.widgetPadding(context,Row(
              children: [
                const Spacer(flex: 1,),
                GestureDetector(
                  onTap: (){
                    Navigator.pop(context);
                  },
                    child: Image.asset(IMAGE.drawerDismissBt,height: 35,width: 35,)),
              ],
            ),right: 20,top: 35),
            CommonWidgets.widgetPadding(context,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(I18N.of(context)!.heading??'',style: TextStyle(
                  color: CLR.whiteClr,
                  fontSize: 18,
                  fontFamily: Fonts.AvenirNextLTProDemi,
                ),),
                 const SizedBox(height: 18,),
                Text(email,style: TextStyle(
                  color: CLR.whiteClr,
                  fontSize: 16,
                  fontFamily: Fonts.AvenirNextLTProRegular,

                ),),
               const SizedBox(height: 5,),

                Text(phone,style: TextStyle(
                  color: CLR.whiteClr,
                  fontSize: 16,
                  fontFamily: Fonts.AvenirNextLTProDemi,

                ),),
                const SizedBox(height:20 ,),
                const Divider(height: 1,thickness:1,color: CLR.lightDarkRedShadeClr,),
                const SizedBox(height: 15,),
                widgetWalletContainer(context,walletPrice,couponWalletPrice),
                const SizedBox(height: 30,),
                widgetDrawerMenuItem(0    ,IMAGE.menuItemOrder, I18N.of(context)!.menuTitleOrder??'',SourceTypeItem.myOrders,context),
                widgetDrawerMenuItem(walletPrice,IMAGE.menuItemWallet,I18N.of(context)!.menuTitleWallet??'',SourceTypeItem.myWallet,context),
                widgetDrawerMenuItem(0,IMAGE.menuItemAbout, I18N.of(context)!.menuTitleAbout??'',SourceTypeItem.aboutUs,context),
                const Spacer(flex: 1,),
                Padding(
                  padding: const EdgeInsets.only(bottom: 70,left: 50,right: 30),
                  child: widgetLogoutBt(context),
                ),
              ],
            ),top:67,left: 30,right: 27),


            // Text(AppConstants.chefCardString2,)
          ],

        ),
      ),
    );
  }

  static Widget widgetWalletContainer(context,int walletPrice,int couponWalletPrice){
   return Container(
     width: 287,
     height: 138,
     decoration: BoxDecoration(
       color: CLR.lightBrownShadeClr,
       borderRadius: BorderRadius.circular(10),
     ),
     child: Column(

       children: [
         Padding(
           padding: const EdgeInsets.only(top:17, left:30,right: 30),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.start,
             crossAxisAlignment: CrossAxisAlignment.end,
             children: [
               Image.asset(IMAGE.moneyBag,width: 20,height: 20,),
               const SizedBox(width: 7,),
               Text(I18N.of(context)!.wallet??'',style: TextStyle(
                 color: CLR.whiteClr,
                 fontSize: 15,
                 fontFamily: Fonts.AvenirNextLTProRegular,
               ),),

               const Spacer(flex: 1,),
               Text('₹$walletPrice',style: TextStyle(
                 color: CLR.whiteClr,
                 fontSize: 16,
                 fontFamily: Fonts.AvenirNextLTProDemi,
               ),),

             ],
           ),
         ),
         const SizedBox(height: 10,),
         widgetInnerCouponWalletContainer(context,couponWalletPrice),

       ],
     ),
   );
  }
  static Widget widgetInnerCouponWalletContainer(context,int couponWalletPrice){
   return  Container(
     width: 265,
     height: 81,
     decoration: BoxDecoration(
       color: CLR.veryLightBrownShadeClr,
       borderRadius: BorderRadius.circular(8),
     ),
     child: Column(
       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
       crossAxisAlignment: CrossAxisAlignment.center,

       children: [
         Padding(
           padding: const EdgeInsets.only(left:60,right: 50),
           child: Row(
             mainAxisAlignment: MainAxisAlignment.start,
             crossAxisAlignment: CrossAxisAlignment.end,
             children: [
               Image.asset(IMAGE.couponWalletImage,width: 24,height: 24,),
               const SizedBox(width: 7,),
               Text(I18N.of(context)!.couponWallet??'',style: TextStyle(
                 color: CLR.whiteClr,
                 fontSize: 14,
                 fontFamily: Fonts.AvenirNextLTProRegular,

               ),),
             ],
           ),
         ),
         const Divider(height: 1,thickness:2,color: CLR.whiteClr,indent: 120,endIndent: 120,),
         Text('₹$couponWalletPrice',style: TextStyle(
           color: CLR.whiteClr,
           fontSize: 20,
           fontFamily: Fonts.AvenirNextLTProDemi,

         ),),

       ],
     ),

   );
  }
  static Widget widgetDrawerMenuItem(var walletPrice ,String menuIcon,String menuTitle,SourceTypeItem sourceTypeItem,context){
   return  GestureDetector(
     onTap: (){
       if(sourceTypeItem==SourceTypeItem.myOrders){
         Navigator.pushReplacementNamed(context, PageName.orders);
       }else if(sourceTypeItem==SourceTypeItem.myWallet){
         //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> WalletFul(balance: walletPrice,)));

       }else if(sourceTypeItem== SourceTypeItem.aboutUs){
         Navigator.pushReplacementNamed(context, PageName.aboutUs);

       }
       else{
         Navigator.pop(context);

       }

     },
     child: Column(
       children: [
         Row(
         children: [
           Image.asset(menuIcon,height: 40,width: 40,),
           const SizedBox(width: 15,),
           Text(menuTitle,style: TextStyle(
             color: CLR.whiteClr,
             fontSize: 14,
             fontFamily: Fonts.AvenirNextLTProDemi,
           ),),
         ],
         ),
         const SizedBox(height: 10,),
         const Divider(height: 1,thickness:1,color: CLR.lightDarkRedShadeClr,indent: 55,),
         const SizedBox(height: 20,)
       ],
     ),
   );
  }
  static Widget widgetLogoutBt(context){
   return ElevatedButton.icon(
     icon: Image.asset(IMAGE.logoutBt,height: 17.57,width: 20.25,),
       onPressed: (){
       PreferenceUtil.setLogin(false);
       Navigator.pushReplacementNamed(context, PageName.login);

       },
       label:Text(I18N.of(context)!.logoutBtString??'',style: TextStyle(
         fontSize: 16,
         fontFamily: Fonts.AvenirNextLTProMedium,
       ),),
   style: ElevatedButton.styleFrom(
     fixedSize: const Size(261, 44),
     primary: CLR.whiteClr,
     onPrimary: CLR.blackClr,
       shape: RoundedRectangleBorder(
           borderRadius: BorderRadius.circular(25))

   ),);
  }
 }