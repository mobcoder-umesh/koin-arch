import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


import '../../localization/i18n.dart';
import '../../utils/app_constant.dart';
import '../../utils/base_widget.dart';
import '../../utils/preference_util.dart';
import '../../widget/app_background.dart';
import '../../widget/common_widgets.dart';
import 'cubit/sample_dashboard_cubit.dart';
import 'cubit/sample_dashboard_state.dart';


class DashBoardPage extends StatefulWidget {
  const DashBoardPage({Key? key}) : super(key: key);

  @override
  State<DashBoardPage> createState() => _DashBoardPageState();
}

class _DashBoardPageState extends State<DashBoardPage> {

  bool isVisible = true;


  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DashboardCubit(),
      child: Scaffold(
        // drawer: StreamBuilder<ProfileResponseData>(
        //     stream: profileDetailsController.stream,
        //     builder: (context, snapshot) {
        //       var data = snapshot.data;
        //       return CustomDrawer.widgetShowDrawer(context,
        //           data?.employeeProfile?.email ?? '',
        //           data?.employeeProfile?.phone ?? '',
        //           data?.employeeProfile?.walletBalance ?? 0,
        //           data?.employeeProfile?.couponWalletBalance ?? 0);
        //     }
       // ),
        body: Stack(
          children: [
            Background.widgetAppBackground(context, CLR.whiteClr),
            CommonWidgets.widgetPadding(context,
                Builder(builder: (BuildContext context) {
                  return IconButton(
                    onPressed: () {
                      Scaffold.of(context).openDrawer();
                    },
                    icon: Image.asset(
                      IMAGE.menuBt,
                      width: 22.8,
                      height: 14.48,
                    ),
                  );
                }), left: 19, top: 56),
            CommonWidgets.widgetPadding(context,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    BlocBuilder<DashboardCubit, DashboardState>(
                      builder: (context, state) {
                        return Text(
                            'Hello '
                                '${state.userProfileResponse.responseData?.employeeProfile?.firstName??''}',
                            style: TextStyle(
                                fontSize: 17, fontFamily: Fonts
                                .AvenirNextLTProMedium),
                          );

                        },
                    ),
                    const SizedBox(height: 10,),
                    Text(
                      I18N.of(context)!.welcomeMessage ?? '',
                      style: TextStyle(
                          fontSize: 20, fontFamily: Fonts.AvenirNextLTProBold),
                    ),
                  ],
                ), left: 30, top: 96),


            CommonWidgets.widgetPadding(context, _widgetStaticDeliveryCard(),
                left: 20, right: 20, top: 163),
            CommonWidgets.widgetPadding(context, _widgetOrderFromChef(),
                top: 282, left: 20, right: 20),
            // StreamBuilder<bool>(
            //     stream: loaderController.stream,
            //     builder: (context, snapshot) {
            //       return Visibility(
            //           visible: snapshot.data ?? false,
            //           child: Center(
            //             child: BaseWidget(),
            //           ));
            //     })


          ],
        ),
      ),
    );
  }


  Widget _widgetStaticDeliveryCard() {
    return SizedBox(
        height: 104,
        width: MediaQuery
            .of(context)
            .size
            .width,
        child: Stack(
          children: [
            SizedBox(
                width: MediaQuery
                    .of(context)
                    .size
                    .width,
                child: Image.asset(
                  IMAGE.deliveryBackground,
                  fit: BoxFit.fill,
                )),
            CommonWidgets.widgetPadding(
                context,
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text(
                    I18N.of(context)!.deliveryCardString1 ?? '',
                    style: TextStyle(
                        fontFamily: Fonts.AvenirNextLTProRegular, fontSize: 14),
                  ),
                  RichText(
                    text: TextSpan(
                        text: I18N.of(context)!.deliveryCardString2 ?? '',
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: Fonts.AvenirNextLTProRegular,
                            color: CLR.blackClr),
                        children: [
                          TextSpan(
                              text: I18N.of(context)!.deliveryCardString3 ?? '',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: Fonts.AvenirNextLTProDemi,
                                  color: CLR.blackClr)),
                        ]),
                  ),
                  RichText(
                    text: TextSpan(
                        text: I18N.of(context)!.deliveryCardString4 ?? '',
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: Fonts.AvenirNextLTProRegular,
                            color: CLR.blackClr),
                        children: [
                          TextSpan(
                              text: I18N.of(context)!.deliveryCardString5 ?? '',
                              style: TextStyle(
                                  fontSize: 14,
                                  fontFamily: Fonts.AvenirNextLTProDemi,
                                  color: CLR.blackClr)),
                        ]),
                  ),
                ]),
                left: 28,
                top: 25)
          ],
        ));
  }

  Widget _widgetRateUs(context) {
    return Container(
      width: MediaQuery
          .of(context)
          .size
          .width,
      height: 50,
      decoration: BoxDecoration(
        color: CLR.peachShade,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Image.asset(
            IMAGE.likeBt,
            width: 32,
            height: 32,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                I18N.of(context)!.ratingCardString1 ?? '',
                style: TextStyle(
                    fontSize: 14, fontFamily: Fonts.AvenirNextLTProDemi),
              ),
              const SizedBox(
                height: 3,
              ),
              Text(
                I18N.of(context)!.ratingCardString2 ?? '',
                style: TextStyle(
                    fontSize: 12, fontFamily: Fonts.AvenirNextLTProRegular),
              )
            ],
          ),
          CommonWidgets.widgetPadding(
              context,
              Text(
                I18N.of(context)!.ratingCardString3 ?? '',
                style: TextStyle(
                    color: CLR.brownDarkClr,
                    fontSize: 13,
                    fontFamily: Fonts.AvenirNextLTProDemi),
              ),
              left: 20),
          GestureDetector(
              onTap: () {
                BlocProvider
                    .of<DashboardCubit>(context)
                    .onDeliveryCardVisibilityChange();
              },
              child: Image.asset(
                IMAGE.dismissBt,
                width: 21.5,
                height: 21.5,
              )),
        ],
      ),
    );
  }

  Widget _widgetOrderFromChef() {
    return Column(
      children: [
        BlocBuilder<DashboardCubit, DashboardState>(
          builder: (context, state) {
            return Visibility(
                visible: state.isDeliveryCard, child: _widgetRateUs(context));
          },
        ),

        Container(
          margin: const EdgeInsets.only(top: 20),
          child: Row(
            children: [
              Image.asset(
                IMAGE.chefIcon,
                width: 17.05,
                height: 19.72,
              ),
              const SizedBox(
                width: 14,
              ),
              Text(
                I18N.of(context)!.vendorString ?? '',
                style: TextStyle(
                    fontFamily: Fonts.AvenirNextLTProDemi, fontSize: 16),
              )
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Expanded(
          child: BlocBuilder<DashboardCubit, DashboardState>(
            builder: (context, state) {

               return GridView.count(
                  crossAxisCount: 2,
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  childAspectRatio: 1 / 1.42,
                  children: List.generate(
                      state.vendorsList.length,
                          (index) =>
                          _widgetChefCard(
                              state.vendorsList[index].name ?? '',
                              Image.network(
                                  state.vendorsList[index].image ?? ''),
                              index)),
                );
              },
          ),
        ),
      ],
    );
  }

  Widget _widgetChefCard(String chefName, Image chefImage, int index) {
    return Container(
      width: 162,
      height: 275,
      decoration: BoxDecoration(
        color: CLR.whiteClr,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(color: CLR.chefCardBorder),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CommonWidgets.widgetPadding(
              context,
              Image.asset(
                IMAGE.chefImage1,
                width: 100.8,
                height: 110.02,
              ),
              bottom: 10,
              right: 10),
          Text(
            I18N.of(context)!.chefCardString1 ?? '',
            textScaleFactor: 1.0,
            style:
            TextStyle(fontFamily: Fonts.AvenirNextLTProDemi, fontSize: 16),
          ),
          Text(
            I18N.of(context)!.chefCardString2 ?? '',
            textScaleFactor: 1.0,
            style:
            TextStyle(fontFamily: Fonts.AvenirNextLTProDemi, fontSize: 16),
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            'CHEF $chefName',
            textScaleFactor: 1.0,
            style: TextStyle(
                fontFamily: Fonts.AvenirNextLTProMedium, fontSize: 12),
          ),
          const SizedBox(
            height: 5,
          ),
          ElevatedButton(
              onPressed: () async {
                // var result = await Navigator.of(context).push(MaterialPageRoute(builder: (context) => MenuPage(vendorId: snapshot.data!.vendorList![index].vendorId!)));
                //print(result);
                // if (result) {
                //   getProfileData();
                //   getVendorList();
                // }
              },
              style: ElevatedButton.styleFrom(
                  fixedSize: const Size(120, 23.89),
                  primary: CLR.brownDarkClr,
                  onPrimary: CLR.whiteClr,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25))),
              child: Text(I18N.of(context)!.orderBtString ?? '')),
        ],
      ),
    );
  }


}
