import 'package:flutter/material.dart';

import '../../utils/app_constant.dart';
import '../../widget/app_background.dart';
import '../../widget/back_navigation.dart';

class Holiday extends StatefulWidget {
  const Holiday({Key? key}) : super(key: key);

  @override
  State<Holiday> createState() => _HolidayState();
}

class _HolidayState extends State<Holiday> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Background.widgetAppBackground(context, CLR.whiteClr),
          const AppBackground(height: 220),
          Column(
            children: [
              const BackNavigation(pageName: "Holiday List"),
              Container(
                height: 460,
                width: 335,
                margin: EdgeInsets.only(top: 85, left: 20, right: 20),
                child: Image.asset(
                  IMAGE.holidayList,
                  fit: BoxFit.fill,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
