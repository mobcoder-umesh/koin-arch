import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../utils/app_constant.dart';
import '../../widget/app_background.dart';
import '../../widget/back_navigation.dart';
import 'cubit/device_record_cubit.dart';
import 'cubit/device_record_state.dart';

void main() => runApp(const DeviceRecordFul());

class DeviceRecordFul extends StatefulWidget {
  const DeviceRecordFul({Key? key}) : super(key: key);

  @override
  State<DeviceRecordFul> createState() => _DeviceRecordFulState();
}

class _DeviceRecordFulState extends State<DeviceRecordFul> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DeviceRecordCubit(),
      child: Scaffold(
        body: Stack(
          children: [
            const AppBackground(height: 218),
            Column(
              children: [
                const BackNavigation(pageName: AppConstants.deviceRecord),
                const SizedBox(
                  height: 50,
                ),
                BlocBuilder<DeviceRecordCubit, DeviceRecordState>(
                  builder: (context, state) {
                    return widgetItemDetails(
                        context,
                        '${state.deviceRecordResponse.responseData?.totalDevices ?? 0}',
                        '${state.deviceRecordResponse.responseData?.desktops ?? 0}',
                        '${state.deviceRecordResponse.responseData?.phones ?? 0}',
                        61.73,
                        59,
                        IMAGE.desktop,
                        CLR.maharoonClr,
                        17,
                        FontWeight.w700,
                        CLR.deep_greyClr,
                        13,
                        FontWeight.normal,
                        CLR.blackClr,
                        14,
                        FontWeight.w600);
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget widgetItemDetails(
      context,
      String deviceName,
      String issueDate,
      String deviceDetails,
      double? width,
      double? height,
      String img,
      Color? color1,
      double? size1,
      FontWeight? bold1,
      Color? color2,
      double? size2,
      FontWeight? bold2,
      Color? color3,
      double? size3,
      FontWeight? bold3) {
    return Container(
      width: 335,
      height: 99,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: const Color(0xffE6E6E6), width: 1)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: width,
            height: height,
            margin: const EdgeInsets.fromLTRB(20, 0, 0, 5),
            child: Image.asset(
              img,
              fit: BoxFit.fill,
            ),
          ),
          const SizedBox(
            width: 14.27,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(
                height: 19,
              ),
              Text(
                deviceName,
                style: TextStyle(
                    color: color1, fontSize: size1, fontWeight: bold1),
              ),
              Text(
                'Issued On: $issueDate',
                style: TextStyle(
                    color: color2, fontSize: size2, fontWeight: bold2),
              ),
              const SizedBox(height: 3),
              Text(
                deviceDetails,
                style: TextStyle(
                    color: color3, fontSize: size2, fontWeight: bold3),
              )
            ],
          )
        ],
      ),
    );
  }
}
