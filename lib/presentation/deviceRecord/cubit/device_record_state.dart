// ignore_for_file: must_be_immutable

import 'package:equatable/equatable.dart';
import '../../../network/model/deviceRecord/device_record_response.dart';

abstract class DeviceRecordState extends Equatable {
  DeviceRecordResponse deviceRecordResponse;

  DeviceRecordState(this.deviceRecordResponse);
}

class DeviceRecordInitial extends DeviceRecordState {
  DeviceRecordInitial(DeviceRecordResponse deviceRecordResponse)
      : super(deviceRecordResponse);

  @override
  List<Object> get props => [];
}

class DeviceRecordLoading extends DeviceRecordState {
  DeviceRecordLoading(DeviceRecordResponse deviceRecordResponse)
      : super(deviceRecordResponse);

  @override
  List<Object?> get props => [];
}

class DeviceRecordLoaded extends DeviceRecordState {
  DeviceRecordLoaded(DeviceRecordResponse deviceRecordResponse)
      : super(deviceRecordResponse);

  @override
  List<Object?> get props => [];
}

class DeviceRecordError extends DeviceRecordState {
  DeviceRecordError(DeviceRecordResponse deviceRecordResponse)
      : super(deviceRecordResponse);

  @override
  List<Object?> get props => [];
}
