// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import '../../../domain/repositories/device_record_repository.dart';
import '../../../network/model/deviceRecord/device_record_response.dart';
import 'device_record_state.dart';

class DeviceRecordCubit extends Cubit<DeviceRecordState> {
  DeviceRecordCubit() : super(DeviceRecordInitial(DeviceRecordResponse()));

  void getDeviceRecordData() async {
    final DeviceRecordRepository repository = DeviceRecordRepository();
    try {
      print('response');
      emit(DeviceRecordLoading(state.deviceRecordResponse));
      print('response Done');

      DeviceRecordResponse deviceRecordResponse =
          (await repository.getDeviceRecord());

      emit(DeviceRecordLoaded(deviceRecordResponse));
    } catch (e) {
      print('error');
      emit(DeviceRecordError(state.deviceRecordResponse));
    }
  }
}
