import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mob_kitchen_freshers/domain/repositories/project_record_repository.dart';
import 'package:mob_kitchen_freshers/network/model/projectRecord/project_record_response.dart';

import '../project_record_page.dart';
import 'project_record_state.dart';

class ProjectRecordCubit extends Cubit<ProjectRecordState> {
  ProjectRecordCubit(List<ChoiceItem> globalList)
      : super(ProjectRecordInitialState(ProjectRecordResponse(),globalList, [])) {
    onProjectRecordData();
  }

  List<ChoiceItem> globalList= [
  ChoiceItem("ALL", true),
  ChoiceItem("ONGOING", false),
  ChoiceItem("COMPLETED", false),
  ChoiceItem("INCOMPLETED", false),
  ];

  void onProjectRecordData() async {
    final ProjectRecordRepository repository = ProjectRecordRepository();
    try {
      emit(ProjectRecordLoadingState(state.response,state.list,state.projectList));
      ProjectRecordResponse response = (await repository.getProjectRecords());
      print('response=====>${response.responseData?.projectDetails?.length}');
      emit(ProjectRecordLoadedState(response,state.list,response.responseData?.projectDetails??[]));
    } catch (e) {
      emit(ProjectRecordErrorState(state.response,state.list,state.projectList));
    }
  }

  void onProjectStatusChange(List<ChoiceItem> list, String optionName ){
    List<ProjectDetails> list1;

    print('Inside Function');

    if(optionName=='ALL'){
      print('Inside All');
      emit(ProjectRecordStatusChangeState(state.response,list,state.projectList));
      emit(ProjectRecordLoadedState(state.response,list,state.response.responseData?.projectDetails??[]));
    }else{
      print('Inside $optionName');
      print('Inside ${state.response.responseData!.projectDetails.toString()}');
      list1 = state.response.responseData!.projectDetails!.where((e) => getStatus1(e.status!)==optionName).toList();
      print('Filter list =====>>>>   ${list1.toString()}');
      emit(ProjectRecordStatusChangeState(state.response,list,state.projectList));
      emit(ProjectRecordLoadedState(state.response,state.list,list1));
    }
  }

  String getStatus1(
      int choice,
      ) {
    switch (choice) {
      case 1:
        return 'ONGOING';

      case 2:
        return  'COMPLETED';

      case 3:
        return  'INCOMPLETED';

      default:
        return 'ALL';

    }
  }
}
