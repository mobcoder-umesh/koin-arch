import 'package:equatable/equatable.dart';

import '../../../network/model/projectRecord/project_record_response.dart';
import '../project_record_page.dart';

abstract class ProjectRecordState extends Equatable{
  ProjectRecordResponse response;
  List<ChoiceItem> list;
  List<ProjectDetails> projectList;
  ProjectRecordState(this.response,this.list,this.projectList);
}

class ProjectRecordInitialState extends ProjectRecordState{
  ProjectRecordInitialState(ProjectRecordResponse response, List<ChoiceItem> list, List<ProjectDetails> projectList) : super(response, list, projectList);





  @override
  List<Object?> get props => [];

}
class ProjectRecordStatusChangeState extends ProjectRecordState{
  ProjectRecordStatusChangeState(ProjectRecordResponse response, List<ChoiceItem> list, List<ProjectDetails> projectList) : super(response, list, projectList);

  @override
  List<Object?> get props => [];

}

class ProjectRecordLoadingState extends ProjectRecordState{
  ProjectRecordLoadingState(ProjectRecordResponse response, List<ChoiceItem> list, List<ProjectDetails> projectList) : super(response, list, projectList);






  @override
  List<Object?> get props => [];

}


class ProjectRecordLoadedState extends ProjectRecordState{
  ProjectRecordLoadedState(ProjectRecordResponse response, List<ChoiceItem> list, List<ProjectDetails> projectList) : super(response, list, projectList);


  @override
  List<Object?> get props => [];

}

class ProjectRecordErrorState extends ProjectRecordState{
  ProjectRecordErrorState(ProjectRecordResponse response, List<ChoiceItem> list, List<ProjectDetails> projectList) : super(response, list, projectList);

  @override
  List<Object?> get props => [];

}
