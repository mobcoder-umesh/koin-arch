import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../utils/app_constant.dart';
import '../../utils/date_utils.dart';
import '../../widget/app_background.dart';
import '../../widget/back_navigation.dart';
import 'cubit/project_record_cubit.dart';
import 'cubit/project_record_state.dart';


void main() => runApp(const MyTestApp());

class MyTestApp extends StatelessWidget {
  const MyTestApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ProjectRecordPage(),
    );
  }
}

class ProjectRecordPage extends StatefulWidget {
  const ProjectRecordPage({Key? key}) : super(key: key);

  @override
  State<ProjectRecordPage> createState() => _ProjectRecordPageState();
}

List<ChoiceItem> _chipsList = [
  ChoiceItem("ALL", true),
  ChoiceItem("ONGOING", false),
  ChoiceItem("COMPLETED", false),
  ChoiceItem("INCOMPLETED", false),
];

class _ProjectRecordPageState extends State<ProjectRecordPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ProjectRecordCubit(_chipsList),
      child: Scaffold(
        body: Stack(
          children: [
            const AppBackground(height: 262),
            Column(
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const BackNavigation(pageName: 'Project Record'),
                const SizedBox(
                  height: 50,
                ),
                BlocBuilder<ProjectRecordCubit, ProjectRecordState>(
                  builder: (context, state) {
                    if (state is ProjectRecordLoadedState) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                                child: ProjectRecordCard(
                                    title: 'COMPLETED',
                                    countOfProjects: state.response.responseData
                                            ?.completedProjects ??
                                        0,
                                    colorOfCard: CLR.orangeShade1)),
                            const SizedBox(
                              width: 6,
                            ),
                            Expanded(
                                child: ProjectRecordCard(
                                    title: 'ON-GOING',
                                    countOfProjects: state.response.responseData
                                            ?.onGoingProjects ??
                                        0,
                                    colorOfCard: CLR.orangeShade2)),
                          ],
                        ),
                      );
                    } else {
                      return Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            Expanded(
                                child: ProjectRecordCard(
                                    title: 'COMPLETED',
                                    countOfProjects: 0,
                                    colorOfCard: CLR.orangeShade1)),
                            SizedBox(
                              width: 6,
                            ),
                            Expanded(
                                child: ProjectRecordCard(
                                    title: 'ON-GOING',
                                    countOfProjects: 0,
                                    colorOfCard: CLR.orangeShade2)),
                          ],
                        ),
                      );
                    }
                  },
                ),
                BlocBuilder<ProjectRecordCubit, ProjectRecordState>(
                  builder: (context, state) {
                    return Container(
                      margin: const EdgeInsets.only(left: 20, top: 20),
                      height: 50,
                      child: ListView.builder(
                          itemCount: state.list.length,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (BuildContext context, int index) {
                            return ProjectStatusChoice(
                              choiceName: state.list[index].name,
                              isSelected: state.list[index].isSelected,
                              index: index,
                            );
                          }),
                    );
                  },
                ),
                Expanded(
                  child: BlocBuilder<ProjectRecordCubit, ProjectRecordState>(
                    builder: (context, state) {
                      if (state is ProjectRecordLoadedState) {
                        var data = state.projectList;
                        return ListView.builder(
                            itemCount: data.length,
                            itemBuilder: (BuildContext context, int index) {
                              return ProjectInfoCard(
                                projectName: data[index].projectName ?? '',
                                clientName: data[index].clientName ?? '',
                                projectManager:
                                    data[index].projectManager ?? '',
                                startDate: data[index].startDate ?? 0,
                                endDate: data[index].endDate ?? 0,
                                status: data[index].status ?? 0,
                              );
                            });
                      } else {
                        return Container();
                      }
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ProjectRecordCard extends StatelessWidget {
  final String title;
  final int countOfProjects;
  final Color colorOfCard;

  const ProjectRecordCard(
      {Key? key,
      required this.title,
      required this.countOfProjects,
      required this.colorOfCard})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 97,
      decoration: BoxDecoration(
        color: colorOfCard,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            countOfProjects.toString(),
            style: TextStyle(
              fontSize: 24,
              color: Colors.white,
              fontFamily: Fonts.AvenirNextLTProDemi,
            ),
          ),
          const SizedBox(
            height: 7,
          ),
          Text(
            title,
            style: TextStyle(
              fontSize: 12,
              color: Colors.white,
              fontFamily: Fonts.AvenirNextLTProMedium,
            ),
          )
        ],
      ),
    );
  }
}

class ProjectStatusChoice extends StatelessWidget {
  final String choiceName;
  final bool isSelected;
  final int index;

  const ProjectStatusChoice(
      {Key? key,
      required this.choiceName,
      required this.isSelected,
      required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        List<ChoiceItem> listOfChoice = _chipsList.map((e) {
          if (e.name == _chipsList[index].name) {
            e.isSelected = true;
            return e;
          } else {
            e.isSelected = false;
            return e;
          }
        }).toList();
        BlocProvider.of<ProjectRecordCubit>(context)
            .onProjectStatusChange(listOfChoice,_chipsList[index].name);
      },
      child: Container(
        margin: const EdgeInsets.only(right: 10),
        height: 44,
        decoration: BoxDecoration(
            color: CLR.whiteClr,
            borderRadius: BorderRadius.circular(50),
            border: Border.all(
                color:
                    isSelected ? CLR.maharoonClr : CLR.unselectedChoiceColor)),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(left: 25, right: 25),
            child: Text(
              choiceName,
              style: TextStyle(
                fontSize: 14,
                color: isSelected ? CLR.maharoonClr : CLR.blackClr,
                fontFamily: Fonts.AvenirNextLTProDemi,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ProjectInfoCard extends StatelessWidget {
  final String projectName;
  final String clientName;
  final String projectManager;
  final int startDate;
  final int endDate;
  final int status;

  const ProjectInfoCard(
      {Key? key,
      required this.projectName,
      required this.clientName,
      required this.projectManager,
      required this.startDate,
      required this.endDate,
      required this.status})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 20, right: 20, top: 10),
      width: MediaQuery.of(context).size.width,
      height: 150,
      decoration: BoxDecoration(
          color: CLR.whiteClr,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: CLR.unselectedChoiceColor)),
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  projectName,
                  style: TextStyle(
                      color: CLR.maharoonClr,
                      fontSize: 17,
                      fontFamily: Fonts.AvenirNextLTProDemi),
                ),
                const Spacer(
                  flex: 1,
                ),
                getStatus(status),
              ],
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              'Client: ${clientName}',
              style: TextStyle(
                  fontFamily: Fonts.AvenirNextLTProMedium,
                  fontSize: 14,
                  color: CLR.lightGreyShade),
            ),
            const SizedBox(
              height: 3,
            ),
            Text(
              'Project Manager: ${projectManager}',
              style: TextStyle(
                  fontFamily: Fonts.AvenirNextLTProMedium,
                  fontSize: 14,
                  color: CLR.lightGreyShade),
            ),
            const SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        DateUtil.convertMillisToDateDDMMYYYY(startDate),
                        style: TextStyle(
                            fontFamily: Fonts.AvenirNextLTProMedium,
                            fontSize: 17,
                            color: CLR.greyShade),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        'Start Date',
                        style: TextStyle(
                            fontFamily: Fonts.AvenirNextLTProMedium,
                            fontSize: 13,
                            color: CLR.greyShade),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  width: 46,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        DateUtil.convertMillisToDateDDMMYYYY(endDate),
                        style: TextStyle(
                            fontFamily: Fonts.AvenirNextLTProMedium,
                            fontSize: 17,
                            color: CLR.greyShade),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Text(
                        'End Date',
                        style: TextStyle(
                            fontFamily: Fonts.AvenirNextLTProMedium,
                            fontSize: 13,
                            color: CLR.greyShade),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ChoiceItem {
  String name;
  bool isSelected;

  ChoiceItem(this.name, this.isSelected);
}

Widget getStatus(
  int choice,
) {
  switch (choice) {
    case 1:
      return const ProjectStatusCard(
          status: 'Ongoing',
          cardColor: CLR.lightGreenShade1,
          borderColor: CLR.lightGreenShade2,
          textColor: CLR.lightGreenShade3);
    case 2:
      return const ProjectStatusCard(
          status: 'Completed',
          cardColor: CLR.lightRedShade,
          borderColor: CLR.lightRedShade1,
          textColor: CLR.redShade);
    case 3:
      return const ProjectStatusCard(
          status: 'Incompleted',
          cardColor: CLR.lightPeachShade1,
          borderColor: CLR.lightPeachShade2,
          textColor: CLR.orangeShade);
    default:
      return const ProjectStatusCard(
          status: 'Not Clear',
          cardColor: CLR.lightGreenShade1,
          borderColor: CLR.lightGreenShade2,
          textColor: CLR.lightGreenShade3);

  }
}

class ProjectStatusCard extends StatelessWidget {
  final String status;
  final Color cardColor;
  final Color borderColor;
  final Color textColor;

  const ProjectStatusCard(
      {Key? key,
      required this.status,
      required this.cardColor,
      required this.borderColor,
      required this.textColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 63,
      height: 23,
      decoration: BoxDecoration(
          color: cardColor,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: borderColor)),
      child: Center(
        child: Text(status,
            style: TextStyle(
                fontFamily: Fonts.AvenirNextLTProMedium,
                fontSize: 10,
                color: textColor)),
      ),
    );
  }
}
