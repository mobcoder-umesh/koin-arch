import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mob_kitchen_freshers/presentation/reimbursements/cubit/reimbursements_cubit.dart';
import 'package:mob_kitchen_freshers/presentation/reimbursements/cubit/reimbursements_state.dart';
import 'package:mob_kitchen_freshers/widget/back_navigation.dart';

import '../../network/model/reimbursement/model/ReimbursementList.dart';
import '../../utils/app_constant.dart';
import '../../utils/date_utils.dart';
import '../../widget/app_background.dart';
import 'claimRaise/claim_raise_screen.dart';

class Reimbursements extends StatefulWidget {
  const Reimbursements({Key? key}) : super(key: key);

  @override
  State<Reimbursements> createState() => _ReimbursementsState();
}

class _ReimbursementsState extends State<Reimbursements> {
  String dropDownValue = '2022';
  var items = [
    '2022',
    '2021',
    '2020',
    '2019',
    '2018',
    '2017',
    '2016',
    '2015',
  ];

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ReimbursementCubit(),
      child: Scaffold(
        body: Stack(
          children: [
            Background.widgetAppBackground(context, CLR.whiteClr),
            const AppBackground(height: 280),
            Column(
              children: [
                const BackNavigation(pageName: "Reimbursements"),
                DropButton(),
                claimRaiseButton(context),
                BlocBuilder<ReimbursementCubit, ReimbursementsState>(
                  builder: (context, state) {
                    return Expanded(
                      child: ListView.builder(
                          itemCount: state.ReimbursementsList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return claimContainer(
                                context,
                                state.ReimbursementsList,
                                index,
                                state
                                        .ReimbursementsList[0]
                                        .monthlyReimburseList?[0]
                                        .uploadBill
                                        ?.first ??
                                    '');
                          }),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget DropButton() {
    return BlocBuilder<ReimbursementCubit, ReimbursementsState>(
      builder: (context, state) {
        return DropdownButton(
          value: state.year,
          icon: Icon(Icons.keyboard_arrow_down),
          items: items.map((String items) {
            return DropdownMenuItem(
              value: items,
              child: Text(items),
            );
          }).toList(),
          onChanged: (String? newValue) {
            BlocProvider.of<ReimbursementCubit>(context)
                .onYearChange(newValue ?? '');
            if (items != newValue) {
              BlocProvider.of<ReimbursementCubit>(context)
                  .getReimbursementData1();
            }
          },
        );
      },
    );
  }

  Widget claimRaiseButton(context) {
    return Container(
      width: 216,
      height: 50,
      margin: const EdgeInsets.only(left: 38, top: 20, right: 38, bottom: 10),
      child: TextButton(
        style: TextButton.styleFrom(
          primary: CLR.whiteClr,
          backgroundColor: CLR.maharoonClr,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25)),
          ),
        ),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const ClaimRaise()));
        },
        child: Text(
          "Claim Raise",
          style: TextStyle(
            fontFamily: Fonts.AvenirNextLTProMedium,
            fontSize: 16,
          ),
        ),
      ),
    );
  }

  Widget claimContainer(
      context, List<ReimbursementList> list, int index, String fileUrl) {
    return BlocBuilder<ReimbursementCubit, ReimbursementsState>(
      builder: (context, state) {
        return Container(
          height: 155,
          width: MediaQuery.of(context).size.width,
          margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: Color(0xffE6E6E6), width: 2),
            boxShadow: const [
              BoxShadow(
                color: Color(0xffE6E6E6),
                offset: Offset(
                  2.0,
                  2.0,
                ),
              ),
              BoxShadow(
                color: Color(0xffE6E6E6),
                offset: Offset(
                  -2.0,
                  -2.0,
                ),
              ),
            ],
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 23, top: 22),
                    child: Image.asset(
                      getExtensionImage(fileUrl),
                      width: 43,
                      height: 56,
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 22, bottom: 4, left: 21),
                        child: Text(
                          list[index].monthlyReimburseList?[0].title ?? '',
                          style: TextStyle(
                              fontSize: 17,
                              fontFamily: Fonts.AvenirNextLTProDemi,
                              color: Color(0xff6C1B03)),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 4, bottom: 4, left: 21),
                        child: Text(
                          DateUtil.dateFormatter(list[index]
                                  .monthlyReimburseList?[0]
                                  .expenseDate ??
                              ''),
                          style: TextStyle(
                              fontSize: 13,
                              fontFamily: Fonts.AvenirNextLTProMedium),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: 27,
                            width: 100,
                            margin:
                                EdgeInsets.only(top: 5, bottom: 4, left: 21),
                            child: Text(
                              "${(list[index].monthlyReimburseList?[0].amount ?? 0).toString()} Rs.",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontFamily: Fonts.AvenirNextLTProBold,
                                  color: Color(0xff6C1B03)),
                            ),
                          ),
                          SizedBox(width: 40),
                          Container(
                            decoration: BoxDecoration(
                              color: statusColor(
                                  list[index].monthlyReimburseList?[0].status ??
                                      0),
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                  color: statusColor(list[index]
                                          .monthlyReimburseList?[0]
                                          .status ??
                                      0),
                                  width: 1),
                            ),
                            height: 23,
                            width: 80,
                            margin: EdgeInsets.only(top: 5, left: 21),
                            child: Center(
                              child: Text(
                                  status(list[index]
                                              .monthlyReimburseList?[0]
                                              .status ??
                                          0)
                                      .toString(),
                                  style: TextStyle(
                                    fontSize: 10,
                                    fontFamily: Fonts.AvenirNextLTProMedium,
                                    color: textColor(list[index]
                                            .monthlyReimburseList?[0]
                                            .status ??
                                        0),
                                  )),
                            ),
                          )
                        ],
                      ),
                    ],
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 5, left: 23),
                child: Text(
                  list[index].monthlyReimburseList?[0].expenseDescription ?? "",
                  style: TextStyle(
                      fontSize: 14, fontFamily: Fonts.AvenirNextLTProMedium),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  String status(
    int choice,
  ) {
    switch (choice) {
      case 0:
        return 'Pending';
      case 1:
        return 'Claim Settled';
      case 2:
        return 'Rejected';
      default:
        return 'NOT CLEAR';
    }
  }

  Color statusColor(
    int choice,
  ) {
    switch (choice) {
      case 0:
        return Color(0xffFFD5A5);
      case 1:
        return Color(0xffC9ECBA);
      case 2:
        return CLR.light_redClr;
      default:
        return CLR.light_greyClr;
    }
  }

  Color textColor(
    int choice,
  ) {
    switch (choice) {
      case 0:
        return Color(0xffFD6121);
      case 1:
        return Color(0xff59AC00);
      case 2:
        return CLR.redClr;
      default:
        return CLR.whiteClr;
    }
  }

  String? getFileExtension(String fileName) {
    try {
      return "." + fileName.split('.').last;
    } catch (e) {
      return null;
    }
  }

  String getExtensionImage(String fileName) {
    switch (getFileExtension(fileName)) {
      case '.pdf':
        return IMAGE.pdfImage;
      case '.docx':
        return IMAGE.docxImage;
      case '.docs':
        return IMAGE.docsImage;
      case '.jpg':
        return IMAGE.jpgImage;
      case '.png':
        return IMAGE.pngImage;
      case '.svg':
        return IMAGE.svgImage;
      case '.xlsx':
        return IMAGE.xlsImage;
      case '.json':
        return IMAGE.jsonImage;
      default:
        return IMAGE.logo;
    }
  }
}
