import 'package:equatable/equatable.dart';

import '../../../network/model/reimbursement/model/ReimbursementList.dart';

abstract class ReimbursementsState extends Equatable {
  List<ReimbursementList> ReimbursementsList;
  String year;

  ReimbursementsState(this.ReimbursementsList,this.year);
}

class ReimbursementsInitial extends ReimbursementsState {
  ReimbursementsInitial(List<ReimbursementList> ReimbursementsList, String year) : super(ReimbursementsList, year);



  @override
  List<Object> get props => [];
}

class ReimbursementsLoading extends ReimbursementsState {
  ReimbursementsLoading(List<ReimbursementList> ReimbursementsList, String year) : super(ReimbursementsList, year);



  @override
  List<Object?> get props => [];
}

class ReimbursementsLoaded extends ReimbursementsState {
  ReimbursementsLoaded(List<ReimbursementList> ReimbursementsList, String year) : super(ReimbursementsList, year);


  @override
  List<Object?> get props => [ReimbursementsList];
}

class ReimbursementsError extends ReimbursementsState {
  ReimbursementsError(List<ReimbursementList> ReimbursementsList, String year) : super(ReimbursementsList, year);


  @override
  List<Object?> get props => [];
}
class ReimbursementsYearChange extends ReimbursementsState {
  ReimbursementsYearChange(List<ReimbursementList> ReimbursementsList, String year) : super(ReimbursementsList, year);




  @override
  List<Object?> get props => [];
}
