
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../domain/repositories/reimbursement_repository.dart';
import '../../../network/model/reimbursement/model/ReimbursementList.dart';
import '../../../network/model/reimbursement/model/ReimbursementResponse.dart';
import 'reimbursements_state.dart';

class ReimbursementCubit extends Cubit<ReimbursementsState> {

  ReimbursementCubit() : super(ReimbursementsInitial(const [],'2022'),) {
    getReimbursementData();
    getReimbursementData1();
  }

  void getReimbursementData() async {
    final ReimbursementRepository repository = ReimbursementRepository();
    try {
      emit(ReimbursementsLoading(state.ReimbursementsList,state.year));
      ReimbursementsResponse response =
          (await repository.getReimbursementList());

      List<ReimbursementList> listOfReimbursement =
          response.responseData?.reimbursementList ?? [];

      emit(ReimbursementsLoaded(listOfReimbursement,state.year));
    } catch (e) {
      emit(ReimbursementsError(state.ReimbursementsList,state.year));
    }
  }
  void getReimbursementData1() async {
    final ReimbursementRepository1 repository = ReimbursementRepository1();
    try {
      emit(ReimbursementsLoading(state.ReimbursementsList,state.year));
      ReimbursementsResponse response =
      (await repository.getReimbursementList());

      List<ReimbursementList> listOfReimbursement =
          response.responseData?.reimbursementList ?? [];

      emit(ReimbursementsLoaded(listOfReimbursement,state.year));
    } catch (e) {
      emit(ReimbursementsError(state.ReimbursementsList,state.year));
    }
  }

  void onYearChange(String year){
    emit(ReimbursementsYearChange(state.ReimbursementsList,year));
    emit(ReimbursementsLoaded(state.ReimbursementsList,state.year));
  }
}
