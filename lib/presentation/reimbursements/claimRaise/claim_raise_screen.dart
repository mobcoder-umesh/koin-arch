import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../utils/app_constant.dart';
import '../../../widget/app_background.dart';
import '../../../widget/back_navigation.dart';
import '../reimbursements_screen.dart';

class ClaimRaise extends StatefulWidget {
  const ClaimRaise({Key? key}) : super(key: key);

  @override
  State<ClaimRaise> createState() => _ClaimRaiseState();
}

class _ClaimRaiseState extends State<ClaimRaise> {
  TextEditingController dateCtl = TextEditingController();
  final titleController = TextEditingController();
  final amountController = TextEditingController();
  final expenseDescriptionController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Stack(
          children: [
            Background.widgetAppBackground(context, CLR.whiteClr),
            const AppBackground(height: 220),
            Column(
              children: [
                const BackNavigation(pageName: "Claim Raise"),
                formFill(),
              ],
            ),
            submitButton(context),
          ],
        ),
      ),
    );
  }

  Widget formFill() {
    return Container(
      margin: const EdgeInsets.only(top: 15, left: 20, right: 20, bottom: 93),
      height: 631,
      width: 335,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: CLR.whiteClr,
          boxShadow: const [
            BoxShadow(color: Color(0xffE6E6E6), offset: Offset(1, 1)),
            BoxShadow(color: Color(0xffE6E6E6), offset: Offset(-1, -1))
          ]),
      child: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 20, left: 20, right: 46),
            child: Text(
              "You can claim your reimbursement within one month. your claim will be credited in the next month.",
              style: TextStyle(
                  fontSize: 16, fontFamily: Fonts.AvenirNextLTProMedium),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(
              top: 17.76,
              left: 18,
            ),
            child: Text(
              "Title",
              style: TextStyle(
                  fontFamily: Fonts.AvenirNextLTProMedium, fontSize: 14),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 6.95, left: 18, right: 18),
            child: TextFormField(
              controller: titleController,
              decoration: const InputDecoration(
                hintText: "Type here",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 17.76, left: 18),
            child: Text(
              "Amount",
              style: TextStyle(
                  fontFamily: Fonts.AvenirNextLTProMedium, fontSize: 14),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 6.95, left: 18, right: 18),
            child: TextFormField(
              controller: amountController,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 17.76, left: 18),
            child: Text(
              "Expense Date",
              style: TextStyle(
                  fontFamily: Fonts.AvenirNextLTProMedium, fontSize: 14),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 6.95, left: 18, right: 18),
            child: TextFormField(
              controller: dateCtl ,
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  icon: Icon(
                    Icons.calendar_month,
                  ),
                  onPressed: () async {
                    DateTime date = DateTime(1900);
                    FocusScope.of(context).requestFocus( FocusNode());
                    date = (await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(1900),
                        lastDate: DateTime(2100)))!;
                    dateCtl.text = DateFormat(
                      'dd/MM/yyyy',
                    ).format(date);
                  },
                ),

                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 17.76, left: 18),
            child: Text(
              "Expense Description",
              style: TextStyle(
                  fontFamily: Fonts.AvenirNextLTProMedium, fontSize: 14),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 6.95, left: 18, right: 18),
            child: TextFormField(
              controller: expenseDescriptionController,
              maxLines: 3,
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  borderSide: BorderSide(
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 17.76, left: 18),
            child: Text(
              "Upload Bill",
              style: TextStyle(
                  fontFamily: Fonts.AvenirNextLTProMedium, fontSize: 14),
            ),
          ),
          Container(
            height: 42,
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.only(top: 6.95, left: 18, right: 18),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey, width: 2),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
            child: GestureDetector(
              child: Container(
                height: 40,
                margin: EdgeInsets.only(left: 198),
                decoration: BoxDecoration(
                  color: Color(0xff1699D8),
                ),
                child: Center(
                    child: Text(
                  "Upload",
                  style: TextStyle(
                      fontSize: 16,
                      fontFamily: Fonts.AvenirNextLTProMedium,
                      color: CLR.whiteClr),
                )),
              ),
              onTap: () {},
            ),
          ),
        ],
      ),
    );
  }

  Widget submitButton(context) {
    return Container(
      width: 194,
      height: 50,
      margin: const EdgeInsets.only(
          left: 90.5, top: 693.66, right: 90.5, bottom: 68.38),
      child: TextButton(
        style: TextButton.styleFrom(
          primary: CLR.whiteClr,
          backgroundColor: CLR.maharoonClr,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25)),
          ),
        ),
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const Reimbursements()));
        },
        child: Text(
          "Submit",
          style: TextStyle(
            fontFamily: Fonts.AvenirNextLTProMedium,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
