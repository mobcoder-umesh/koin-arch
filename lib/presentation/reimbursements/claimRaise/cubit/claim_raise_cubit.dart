import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:mob_kitchen_freshers/domain/repositories/claim_raise_repository.dart';

part 'claim_raise_state.dart';

class ClaimRaiseCubit extends Cubit<ClaimRaiseState> {
  ClaimRaiseCubit() : super(SubmitInitial(false,false,false,false));
  void onTittleChange(String title) {

    if (title!=null && title.isNotEmpty ){
      print('title correct $title');
      emit(SubmitCredentialsState(true,state.isAmountValid,state.isExpenseDateValid,state.isExpenseDescriptionValid));
    }else{
      print('title incorrect $title');
      emit(SubmitCredentialsState(false,state.isAmountValid,state.isExpenseDateValid,state.isExpenseDescriptionValid));

    }
  }
  void onAmountChange(String amount) {

    if (amount!=null && amount.isNotEmpty){
      print('amount correct $amount');
      emit(SubmitCredentialsState(state.isTittleValid,true,state.isExpenseDateValid,state.isExpenseDescriptionValid));
    }else{
      print('amount incorrect $amount');
      emit(SubmitCredentialsState(state.isTittleValid,false,state.isExpenseDateValid,state.isExpenseDescriptionValid));

    }
  }
  void onExpenseDateChange(String ExpenseDate) {

    if (ExpenseDate!=null && ExpenseDate.isNotEmpty ){
      print('ExpenseDate correct $ExpenseDate');
      emit(SubmitCredentialsState(state.isTittleValid,state.isAmountValid,true,state.isExpenseDescriptionValid));
    }else{
      print('ExpenseDate incorrect $ExpenseDate');
      emit(SubmitCredentialsState(state.isTittleValid,state.isAmountValid,false,state.isExpenseDescriptionValid));

    }
  }
  void onExpenseDescriptionChange(String ExpenseDescription) {

    if (ExpenseDescription!=null && ExpenseDescription.isNotEmpty ){
      print('title correct $ExpenseDescription');
      emit(SubmitCredentialsState(state.isTittleValid,state.isAmountValid,state.isExpenseDateValid,true));
    }else{
      print('title incorrect $ExpenseDescription');
      emit(SubmitCredentialsState(state.isTittleValid,state.isAmountValid,state.isExpenseDateValid,false));

    }
  }
  // void onSubmit(String tittle,String amount,String expenseDate,String expenseDescription,String billUpload)async{
  //   final ClaimRaiseRepository repository = ClaimRaiseRepository();
  //   ClaimRaiseResponse responseData = await repository.getClaimRaiseResponse(tittle, amount, expenseDate, expenseDescription, billUpload);
  //   if (responseData.statusCode == 1){
  //     emit(SubmitSuccessState(state.isTittleValid,state.isAmountValid,state.isExpenseDateValid,state.isExpenseDescriptionValid,tittle, amount, expenseDate, expenseDescription));
  //   }else{
  //     emit(SubmitErrorState(state.isTittleValid,state.isAmountValid,state.isExpenseDateValid,state.isExpenseDescriptionValid,errorMessage));
  //   }
 // }
}

//   void onLogin(String email, String password) async {
//     final LoginRepository repository = LoginRepository();
//
//     LoginResponse responseData =
//     await repository.getLoginResponse(email, password);
//     // print('error data ====>${responsedata.errorData?.responseMessage ?? ''}');
//
//
//     if (responseData.statusCode == 1) {
//       //print('check=====>${responsedata.responseData!.employeeProfile!.accessToken}');
//       ACCESS_TOKEN =
//           responseData.responseData?.employeeProfile?.accessToken ?? '';
//       emit(LoginSuccessState(
//           state.isEmailValid,
//           state.isPasswordValid,
//           state.isPasswordVisible,
//           responseData.responseData?.employeeProfile?.email??''));
//
//     } else {
//       print('error data ====>${responseData.errorData?.errorMessage ?? ''}');
//       emit(LoginErrorState(
//           state.isEmailValid,
//           state.isPasswordValid,
//           state.isPasswordVisible,
//           responseData.errorData?.responseMessage ?? ''));
//     }
//   }
// }
