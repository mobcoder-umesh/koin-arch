part of 'claim_raise_cubit.dart';

abstract class ClaimRaiseState extends Equatable {
  final bool isTittleValid;
  final bool isAmountValid;
  final bool isExpenseDateValid;
  final bool isExpenseDescriptionValid;

  const ClaimRaiseState(this.isTittleValid, this.isAmountValid,
      this.isExpenseDateValid, this.isExpenseDescriptionValid);

  @override
  List<Object> get props => [
        isTittleValid,
        isAmountValid,
        isExpenseDateValid,
        isExpenseDescriptionValid,
      ];
}

class SubmitInitial extends ClaimRaiseState {
  SubmitInitial(bool isTittleValid, bool isAmountValid, bool isExpenseDateValid,
      bool isExpenseDescriptionValid)
      : super(isTittleValid, isAmountValid, isExpenseDateValid,
            isExpenseDescriptionValid);

// @override
// List<Object?> get props => [];
}

class SubmitCredentialsState extends ClaimRaiseState {
  SubmitCredentialsState(bool isTittleValid, bool isAmountValid,
      bool isExpenseDateValid, bool isExpenseDescriptionValid)
      : super(isTittleValid, isAmountValid, isExpenseDateValid,
            isExpenseDescriptionValid);
}

class SubmitSuccessState extends ClaimRaiseState {
  final String tittle;
  final String amount;
  final String expenseDate;
  final String expenseDescription;
  SubmitSuccessState(bool isTittleValid, bool isAmountValid,
      bool isExpenseDateValid, bool isExpenseDescriptionValid, this.tittle, this.amount, this.expenseDate, this.expenseDescription)
      : super(isTittleValid, isAmountValid, isExpenseDateValid,
            isExpenseDescriptionValid);

// @override
// List<Object?> get props => [email];

}

class SubmitErrorState extends ClaimRaiseState {
  final String errorMessage;

  SubmitErrorState(
      bool isTittleValid,
      bool isAmountValid,
      bool isExpenseDateValid,
      bool isExpenseDescriptionValid,
      this.errorMessage)
      : super(isTittleValid, isAmountValid, isExpenseDateValid,
            isExpenseDescriptionValid);

  @override
  List<Object> get props => [errorMessage];
}
