// ignore_for_file: must_be_immutable
import 'package:equatable/equatable.dart';
import '../../../network/model/dailyAttendence/daily_attendence_response.dart';

abstract class DailyAttendenceState extends Equatable {
  DateTime currentDate;

  DateTime currentDate2;

  DateTime targetDateTime;
  String currentMonth;
  DailyAttendenceResponse dailyAttendenceResponse;

  DailyAttendenceState(this.currentDate, this.currentDate2, this.targetDateTime,
      this.currentMonth, this.dailyAttendenceResponse);
}

class DailyAttendenceInitial extends DailyAttendenceState {
  DailyAttendenceInitial(
      DateTime currentDate,
      DateTime currentDate2,
      DateTime targetDateTime,
      String currentMonth,
      DailyAttendenceResponse dailyAttendenceResponse)
      : super(currentDate, currentDate2, targetDateTime, currentMonth,
            dailyAttendenceResponse);

  @override
  List<Object> get props => [];
}

class LeftArrowState extends DailyAttendenceState {
  LeftArrowState(
      DateTime currentDate,
      DateTime currentDate2,
      DateTime targetDateTime,
      String currentMonth,
      DailyAttendenceResponse dailyAttendenceResponse)
      : super(currentDate, currentDate2, targetDateTime, currentMonth,
            dailyAttendenceResponse);

  @override
  List<Object> get props => [];
}

class RightArrowState extends DailyAttendenceState {
  RightArrowState(
      DateTime currentDate,
      DateTime currentDate2,
      DateTime targetDateTime,
      String currentMonth,
      DailyAttendenceResponse dailyAttendenceResponse)
      : super(currentDate, currentDate2, targetDateTime, currentMonth,
            dailyAttendenceResponse);

  @override
  List<Object> get props => [];
}

class OnDaySelectState extends DailyAttendenceState {
  OnDaySelectState(
      DateTime currentDate,
      DateTime currentDate2,
      DateTime targetDateTime,
      String currentMonth,
      DailyAttendenceResponse dailyAttendenceResponse)
      : super(currentDate, currentDate2, targetDateTime, currentMonth,
            dailyAttendenceResponse);

  @override
  List<Object> get props => [];
}

class OnCalendarChangeState extends DailyAttendenceState {
  OnCalendarChangeState(
      DateTime currentDate,
      DateTime currentDate2,
      DateTime targetDateTime,
      String currentMonth,
      DailyAttendenceResponse dailyAttendenceResponse)
      : super(currentDate, currentDate2, targetDateTime, currentMonth,
            dailyAttendenceResponse);

  @override
  List<Object> get props => [];
}

class DailyAttendenceLoading extends DailyAttendenceState {
  DailyAttendenceLoading(
      DateTime currentDate,
      DateTime currentDate2,
      DateTime targetDateTime,
      String currentMonth,
      DailyAttendenceResponse dailyAttendenceResponse)
      : super(currentDate, currentDate2, targetDateTime, currentMonth,
            dailyAttendenceResponse);

  @override
  List<Object> get props => [];
}

class DailyAttendenceLoaded extends DailyAttendenceState {
  DailyAttendenceLoaded(
      DateTime currentDate,
      DateTime currentDate2,
      DateTime targetDateTime,
      String currentMonth,
      DailyAttendenceResponse dailyAttendenceResponse)
      : super(currentDate, currentDate2, targetDateTime, currentMonth,
            dailyAttendenceResponse);

  @override
  List<Object> get props => [];
}

class DailyAttendenceError extends DailyAttendenceState {
  DailyAttendenceError(
      DateTime currentDate,
      DateTime currentDate2,
      DateTime targetDateTime,
      String currentMonth,
      DailyAttendenceResponse dailyAttendenceResponse)
      : super(currentDate, currentDate2, targetDateTime, currentMonth,
            dailyAttendenceResponse);

  @override
  List<Object> get props => [];
}
