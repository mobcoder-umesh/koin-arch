// ignore_for_file: depend_on_referenced_packages

import 'package:bloc/bloc.dart';
import 'package:intl/intl.dart';
import '../../../domain/repositories/daily_attendence_repository.dart';
import '../../../network/model/dailyAttendence/daily_attendence_response.dart';
import 'daily_attendence_state.dart';

class DailyAttendenceCubit extends Cubit<DailyAttendenceState> {
  DailyAttendenceCubit()
      : super(DailyAttendenceInitial(
            DateTime.now(),
            DateTime.now(),
            DateTime.now(),
            DateFormat.yMMM().format(DateTime.now()),
            DailyAttendenceResponse())) {
    getDashboardData();
  }

  void onLeftArrowIcon() {
    state.targetDateTime =
        DateTime(state.targetDateTime.year, state.targetDateTime.month - 1);
    state.currentMonth = DateFormat.yMMM().format(state.targetDateTime);
    emit(LeftArrowState(
        state.currentDate,
        state.currentDate2,
        state.targetDateTime,
        state.currentMonth,
        state.dailyAttendenceResponse));
  }

  void onRightArrowIcon() {
    state.targetDateTime =
        DateTime(state.targetDateTime.year, state.targetDateTime.month + 1);
    state.currentMonth = DateFormat.yMMM().format(state.targetDateTime);
    emit(RightArrowState(
        state.currentDate,
        state.currentDate2,
        state.targetDateTime,
        state.currentMonth,
        state.dailyAttendenceResponse));
  }

  void onDateSelect(DateTime date) {
    state.currentDate2 = date;
    emit(OnDaySelectState(
        state.currentDate,
        state.currentDate2,
        state.targetDateTime,
        state.currentMonth,
        state.dailyAttendenceResponse));
  }

  void onCalendarChange(DateTime date) {
    state.targetDateTime = date;
    state.currentMonth = DateFormat.yMMM().format(state.targetDateTime);
    emit(OnCalendarChangeState(
        state.currentDate,
        state.currentDate2,
        state.targetDateTime,
        state.currentMonth,
        state.dailyAttendenceResponse));
  }

  void getDashboardData() async {
    final DailyAttendenceRepository repository = DailyAttendenceRepository();
    try {
      emit(DailyAttendenceLoading(
          state.currentDate,
          state.currentDate2,
          state.targetDateTime,
          state.currentMonth,
          state.dailyAttendenceResponse));
      print('loading');

      DailyAttendenceResponse dailyAttendenceResponse =
          (await repository.getDailyAttendence());

      emit(DailyAttendenceLoaded(state.currentDate, state.currentDate2,
          state.targetDateTime, state.currentMonth, dailyAttendenceResponse));
      print('loaded');
    } catch (e) {
      print('error');
      emit(DailyAttendenceError(
          state.currentDate,
          state.currentDate2,
          state.targetDateTime,
          state.currentMonth,
          state.dailyAttendenceResponse));
    }
  }
}
