import 'package:flutter/material.dart';
import '../../widget/common_widgets.dart';

Widget widgetDateAttendence(
    context,
    String text1,
    double? size1,
    String? fontFamily,
    Color? color,
    double? width,
    Color? containerColor,
    Color borderColor,
    String status,
    Color? textColor) {
  return CommonWidgets.widgetPadding(
      context,
      Row(
        children: [
          Text(
            text1,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: size1, fontFamily: fontFamily, color: color),
          ),
          const Spacer(
            flex: 1,
          ),
          Container(
              margin: const EdgeInsets.only(left: 15, top: 5, bottom: 5),
              width: width,
              height: 23,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: containerColor,
                  border: Border.all(width: 1.0, color: borderColor)),
              child: CommonWidgets.widgetPadding(
                context,
                Center(
                  child: Text(
                    status,
                    style: TextStyle(
                      color: textColor,
                      fontSize: 13,
                    ),
                  ),
                ),
              ))
        ],
      ),
      left: 20,
      right: 10,
      top: 5);
}

Widget widgetLeaveCard(
    String text1, String text2, Color? color, double? height) {
  return Container(
    width: 108,
    height: height,
    decoration:
        BoxDecoration(color: color, borderRadius: BorderRadius.circular(10)),
    child: Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        Text(
          text1,
          style: const TextStyle(
              fontSize: 28, fontWeight: FontWeight.bold, color: Colors.white),
        ),
        const SizedBox(
          height: 5,
        ),
        Center(
          child: Text(
            text2,
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 14, color: Colors.white),
          ),
        )
      ],
    ),
  );
}
