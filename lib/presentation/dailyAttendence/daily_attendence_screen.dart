import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import '../../utils/app_constant.dart';
import '../../utils/date_utils.dart';
import '../../widget/app_background.dart';
import '../../widget/back_navigation.dart';
import '../../widget/common_widgets.dart';
import 'cubit/daily_attendence_cubit.dart';
import 'cubit/daily_attendence_state.dart';
import 'widget.dart';

class CustomCalendarFul extends StatefulWidget {
  const CustomCalendarFul({Key? key}) : super(key: key);

  @override
  State<CustomCalendarFul> createState() => _CustomCalendarFulState();
}

class _CustomCalendarFulState extends State<CustomCalendarFul> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DailyAttendenceCubit(),
      child: Scaffold(
        body: Stack(
          children: [
            const AppBackground(height: 436),
            BlocBuilder<DailyAttendenceCubit, DailyAttendenceState>(
              builder: (context, state) {
                var data = state.dailyAttendenceResponse.responseData;
                return Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      const BackNavigation(
                          pageName: AppConstants.dailyAttendence),
                      CommonWidgets.widgetPadding(
                          context,
                          Row(
                            children: [
                              TextButton(
                                  onPressed: () {
                                    BlocProvider.of<DailyAttendenceCubit>(
                                            context)
                                        .onLeftArrowIcon();
                                  },
                                  child: const Icon(
                                    Icons.arrow_back_ios_sharp,
                                    color: Colors.black,
                                    size: 13,
                                  )),
                              BlocBuilder<DailyAttendenceCubit,
                                  DailyAttendenceState>(
                                builder: (context, state) {
                                  return Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 0),
                                      child: Text(
                                        state.currentMonth,
                                        style: const TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  );
                                },
                              ),
                              TextButton(
                                  onPressed: () {
                                    BlocProvider.of<DailyAttendenceCubit>(
                                            context)
                                        .onRightArrowIcon();
                                  },
                                  child: const Icon(
                                    Icons.arrow_forward_ios_sharp,
                                    color: Colors.black,
                                    size: 13,
                                  )),
                            ],
                          ),
                          left: 85,
                          right: 90),
                      CommonWidgets.widgetPadding(context, BlocBuilder<
                          DailyAttendenceCubit, DailyAttendenceState>(
                        builder: (context, state) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: widgetLeaveCard(
                                    '${data?.late ?? 0}/6',
                                    AppConstants.lateComings,
                                    const Color(0xff56B4E1),
                                    97),
                              ),
                              const SizedBox(
                                width: 6,
                              ),
                              Expanded(
                                child: widgetLeaveCard(
                                    '${data?.avgHours ?? 0}',
                                    AppConstants.averageHours,
                                    const Color(0xff1F99D2),
                                    97),
                              ),
                              const SizedBox(
                                width: 6,
                              ),
                              Expanded(
                                  child: widgetLeaveCard(
                                      '${data?.earlyCheckoutCount ?? 0}/6',
                                      AppConstants.earlyLeaves,
                                      const Color(0xff2C8EBD),
                                      97)),
                            ],
                          );
                        },
                      ), left: 20, right: 20),
                      BlocBuilder<DailyAttendenceCubit, DailyAttendenceState>(
                        builder: (context, state) {
                          return Container(
                            height: 227,
                            padding: const EdgeInsets.only(
                                top: 15, left: 10, right: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15),
                                color: Colors.white,
                                boxShadow: const [
                                  BoxShadow(
                                      color: Colors.black12, blurRadius: 2)
                                ]),
                            margin: const EdgeInsets.only(
                                top: 6, left: 20, right: 20),
                            child: CalendarCarousel<Event>(
                              showOnlyCurrentMonthDate: false,
                              onDayPressed:
                                  (DateTime date, List<Event> events) {
                                BlocProvider.of<DailyAttendenceCubit>(context)
                                    .onDateSelect(date);
                              },
                              showHeader: false,
                              todayTextStyle:
                                  const TextStyle(color: Colors.grey),
                              selectedDayButtonColor: const Color(0xff56B4E1),
                              selectedDateTime: state.currentDate2,
                              targetDateTime: state.targetDateTime,
                              locale: 'en',
                              childAspectRatio: 1 / 0.6,
                              weekdayTextStyle:
                                  const TextStyle(color: Colors.black),
                              weekFormat: false,
                              daysTextStyle:
                                  const TextStyle(color: Colors.grey),
                              nextDaysTextStyle:
                                  TextStyle(color: Colors.grey.shade300),
                              prevDaysTextStyle:
                                  TextStyle(color: Colors.grey.shade300),
                              selectedDayTextStyle:
                                  const TextStyle(color: Colors.white),
                              todayButtonColor: Colors.white,
                              todayBorderColor: Colors.white,
                              weekendTextStyle:
                                  const TextStyle(color: Color(0xff59AC00)),
                              selectedDayBorderColor: const Color(0xff56B4E1),
                              weekDayFormat: WeekdayFormat.short,
                              pageSnapping: false,
                              customGridViewPhysics:
                                  const NeverScrollableScrollPhysics(),
                              daysHaveCircularBorder: true,
                              onDayLongPressed: (DateTime date) {
                                print('Long Pressed date $date');
                              },
                              //weekDayMargin: EdgeInsets.only(left: 50),
                              minSelectedDate: state.currentDate
                                  .subtract(const Duration(days: 360)),
                              maxSelectedDate: state.currentDate
                                  .add(const Duration(days: 360)),
                              firstDayOfWeek: 1,
                              onCalendarChanged: (DateTime date) {
                                BlocProvider.of<DailyAttendenceCubit>(context)
                                    .onCalendarChange(date);
                              },
                            ),
                          );
                        },
                      ),
                      CommonWidgets.widgetPadding(
                          context,
                          Text(
                            AppConstants.yourAttendence,
                            style: TextStyle(
                                color: Colors.grey.shade600,
                                fontSize: 18,
                                fontWeight: FontWeight.w500),
                          ),
                          top: 18,
                          left: 20),
                      Expanded(
                        child: ListView.builder(
                          itemCount: data?.attendenceDetails?.length ?? 0,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              margin:
                                  const EdgeInsets.only(left: 20, right: 20),
                              padding: const EdgeInsets.only(top: 10),
                              width: MediaQuery.of(context).size.width,
                              height: 131,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(15),
                                  boxShadow: const [
                                    BoxShadow(
                                        color: Colors.black12, blurRadius: 2)
                                  ]),
                              child: Column(
                                children: [
                                  widgetDateAttendence(
                                      context,
                                      DateUtil.convertMillisToDateDDMMYYYY(data
                                              ?.attendenceDetails?[index]
                                              .attendanceRecord?[index]
                                              .date ??
                                          0),
                                      17,
                                      Fonts.AvenirNextLTProBold,
                                      CLR.blackClr,
                                      63,
                                      containerColor(data
                                              ?.attendenceDetails?[index]
                                              .attendanceRecord?[index]
                                              .status ??
                                          0),
                                      borderColor(data
                                              ?.attendenceDetails?[index]
                                              .attendanceRecord?[index]
                                              .status ??
                                          0),
                                      attendenceStatus(data
                                              ?.attendenceDetails?[index]
                                              .attendanceRecord?[index]
                                              .status ??
                                          0),
                                      textColor(data
                                              ?.attendenceDetails?[index]
                                              .attendanceRecord?[index]
                                              .status ??
                                          0)),
                                  widgetTimingsDetails(
                                      context,
                                      '${data?.attendenceDetails?[index].attendanceRecord?[index].checkIn ?? 0}',
                                      '${data?.attendenceDetails?[index].attendanceRecord?[index].checkOut ?? 0}',
                                      '${data?.attendenceDetails?[index].attendanceRecord?[index].totalHours ?? 0}',
                                      17,
                                      17,
                                      17),
                                  widgetTimingsDetails(
                                      context,
                                      AppConstants.checkIn,
                                      AppConstants.checkOut,
                                      AppConstants.totalHours,
                                      13,
                                      13,
                                      13),
                                  widgetAttendenceDetails(
                                      context,
                                      'Late',
                                      'Completed',
                                      const Color(0xffFF0000),
                                      13,
                                      13,
                                      const Color(0xff59AC00))
                                ],
                              ),
                            );
                          },
                        ),
                      ),
                    ]);
              },
            ),
          ],
        ),
      ),
    );
  }

  String attendenceStatus(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return 'Present';
      case 2:
        return 'Late';
      case 3:
        return 'Absent';
      case 4:
        return 'Missed Punch';
      default:
        return 'NO DATA';
    }
  }

  Color containerColor(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return CLR.lightGreenShade1;
      case 2:
        return CLR.light_container_peachClr;
      case 3:
        return CLR.light_container_orangeClr;
      case 4:
        return CLR.light_container_greyClr;
      default:
        return CLR.light_container_greyClr;
    }
  }

  Color borderColor(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return CLR.lightGreenShade2;
      case 2:
        return CLR.lightRedShade1;
      case 3:
        return CLR.light_border_orangeClr;
      case 4:
        return CLR.unselectedChoiceColor;
      default:
        return CLR.unselectedChoiceColor;
    }
  }

  Color textColor(
    int choice,
  ) {
    switch (choice) {
      case 1:
        return CLR.lightGreenShade3;
      case 2:
        return CLR.redShade;
      case 3:
        return CLR.orangeShade;
      case 4:
        return CLR.greyShade;
      default:
        return CLR.greyShade;
    }
  }
}

Widget widgetTimingsDetails(context, String text1, String text2, String text3,
    double? size1, double? size2, double? size3) {
  return CommonWidgets.widgetPadding(
      context,
      Row(
        children: [
          Text(
            text1,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: size1,
            ),
          ),
          const Spacer(
            flex: 1,
          ),
          Text(
            text2,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: size2,
            ),
          ),
          const Spacer(
            flex: 1,
          ),
          Text(
            text3,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: size3,
            ),
          ),
        ],
      ),
      left: 20,
      right: 20,
      top: 5);
}

Widget widgetAttendenceDetails(context, String text1, String text2,
    Color? color1, double? size1, double? size2, Color? color2) {
  return CommonWidgets.widgetPadding(
      context,
      Row(
        children: [
          Text(
            text1,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: size1, color: color1),
          ),
          const Spacer(
            flex: 1,
          ),
          Text(
            text2,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: size2, color: color2),
          ),
        ],
      ),
      left: 20,
      right: 20,
      top: 5);
}
