import 'package:flutter/material.dart';

import 'package:mob_kitchen_freshers/presentation/login/login_view.dart';

import '../../localization/i18n.dart';

import 'package:mob_kitchen_freshers/presentation/deviceRecord/device_record_screen.dart';

import 'package:mob_kitchen_freshers/presentation/holiday/holiday_screen.dart';
import 'package:mob_kitchen_freshers/presentation/leave/leaveRecord/leave_record_screen.dart';



import 'package:mob_kitchen_freshers/presentation/projectRecord/project_record_page.dart';
import 'package:mob_kitchen_freshers/presentation/reimbursements/reimbursements_screen.dart';
import 'package:mob_kitchen_freshers/presentation/resumes/resume_page.dart';

import 'package:mob_kitchen_freshers/presentation/tickets/tickets_screen.dart';

import '../../utils/app_constant.dart';
import '../../utils/preference_util.dart';
import '../../widget/app_background.dart';
import '../../widget/common_widgets.dart';

import '../tickets/tickets_screen.dart';

import '../holiday/holiday_screen.dart';

import '../dailyAttendence/daily_attendence_screen.dart';
import '../deviceRecord/device_record_screen.dart';
import '../leave/leaveRecord/leave_record_screen.dart';
import '../projectRecord/project_record_page.dart';
import '../resumes/resume_page.dart';
import 'profile_screen.dart';

enum SourceTypeItem {
  DAILYATTENDANCE,
  LEAVERECORDS,
  PROJECTRECORDS,
  MYRESUME,
  REIMBURSEMENTS,
  YOURTICKETS,
  HOLIDAYLIST,
  DEVICERECORDS
}


class CustomDrawer {
  static Widget widgetShowDrawer(context, String email, String phone) {
    return SizedBox(
      width: 345,
      child: Drawer(
        backgroundColor: CLR.brownDarkClr,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(30), bottomRight: Radius.circular(30)),
        ),
        child: Stack(
          children: [
            Background.widgetDrawerBackground(context, IMAGE.drawerBackground),
            CommonWidgets.widgetPadding(
                context,
                Row(
                  children: [
                    const Spacer(
                      flex: 1,
                    ),
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Image.asset(
                          IMAGE.drawerDismissBt,
                          height: 35,
                          width: 35,
                        )),
                  ],
                ),
                right: 20,
                top: 35),
            CommonWidgets.widgetPadding(
                context,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "MOB'S HRM",
                      style: TextStyle(
                        color: CLR.whiteClr,
                        fontSize: 18,
                        fontFamily: Fonts.AvenirNextLTProDemi,
                      ),
                    ),
                    const SizedBox(
                      height: 18,
                    ),
                    Row(
                      children: [
                        Container(
                          child: Column(
                            crossAxisAlignment:CrossAxisAlignment.start,
                            children: [
                              Text(
                                email,
                                style: TextStyle(
                                  color: CLR.whiteClr,
                                  fontSize: 16,
                                  fontFamily: Fonts.AvenirNextLTProRegular,
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Text(
                                phone,
                                style: TextStyle(
                                  color: CLR.whiteClr,
                                  fontSize: 16,
                                  fontFamily: Fonts.AvenirNextLTProDemi,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Spacer(flex: 1,),
                        GestureDetector(
                          onTap: (){
                            Navigator.of(context).push(
                                MaterialPageRoute(builder: (context) => employeeProfileScreen()));
                          },
                          child: Container(
                            height: 35,
                            width: 35,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(18),
                              color: CLR.darkblueClr2,
                              border: Border.all(color: CLR.darkblueClr1, width: 2),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(5),
                              child: Image.asset(
                                IMAGE.profileicon,
                                height: 8,
                                width: 8,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    const Divider(
                      height: 1,
                      thickness: 1,
                      color: CLR.lightDarkRedShadeClr,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    widgetDrawerItem(
                        IMAGE.drawerattendanceicon,
                        'Daily Attendance',
                        19,
                        18.21,
                        SourceTypeItem.DAILYATTENDANCE,
                        context),
                    widgetDrawerItem(
                        IMAGE.drawerleaverecordicon,
                        'Leave Records',
                        23.76,
                        20,
                        SourceTypeItem.LEAVERECORDS,
                        context),
                    widgetDrawerItem(
                        IMAGE.drawerprojectrecordicon,
                        'Projet Records',
                        17.14,
                        22.05,
                        SourceTypeItem.PROJECTRECORDS,
                        context),
                    widgetDrawerItem(IMAGE.drawermyresumeicon, 'My Resume',
                        20.64, 19.87, SourceTypeItem.MYRESUME, context),
                    widgetDrawerItem(
                        IMAGE.drawerreimbursementicon,
                        'Reimbursements',
                        20.96,
                        22.46,
                        SourceTypeItem.REIMBURSEMENTS,
                        context),
                    widgetDrawerItem(IMAGE.draweryourticketsicon, 'Your Ticket',
                        20.87, 20.87, SourceTypeItem.YOURTICKETS, context),
                    widgetDrawerItem(
                        IMAGE.drawerholidaylisticon,
                        'Holiday List',
                        22.62,
                        22.65,
                        SourceTypeItem.HOLIDAYLIST,
                        context),
                    widgetDrawerItem(
                        IMAGE.drawerdevicerecordicon,
                        'Device Records',
                        17.66,
                        23,
                        SourceTypeItem.DEVICERECORDS,
                        context),
                    widgetLogoutBt(context),
                  ],
                ),
                top: 67,
                left: 30,
                right: 27),
          ],
        ),
      ),
    );
  }

  static Widget widgetDrawerItem(String title, String Title, double? height,
      double? width, SourceTypeItem sourceTypeItem, context) {
    return GestureDetector(
      onTap: () {


        if (sourceTypeItem == SourceTypeItem.DAILYATTENDANCE) {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => CustomCalendarFul()));
        } else if (sourceTypeItem == SourceTypeItem.LEAVERECORDS) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => LeaveRecordFul()));
        } else if (sourceTypeItem == SourceTypeItem.PROJECTRECORDS) {
          Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => ProjectRecordPage()));
        } else if (sourceTypeItem == SourceTypeItem.MYRESUME) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => ResumesPage()));
        } else if (sourceTypeItem == SourceTypeItem.REIMBURSEMENTS) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => Reimbursements()));
        } else if (sourceTypeItem == SourceTypeItem.YOURTICKETS) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => Tickets()));
        } else if (sourceTypeItem == SourceTypeItem.HOLIDAYLIST) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => Holiday()));
        } else if (sourceTypeItem == SourceTypeItem.DEVICERECORDS) {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => DeviceRecordFul()));
        } else {


          Navigator.pop(context);
        }
      },
      child: Column(
        children: [
          Row(
            children: [
              Stack(
                children: [
                  Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                      color: CLR.darkredcart2,
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 10, left: 11),
                    child: Image.asset(
                      title,
                      height: height,
                      width: width,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                width: 15,
              ),
              Text(
                Title,
                style: TextStyle(
                  color: CLR.whiteClr,
                  fontSize: 14,
                  fontFamily: Fonts.AvenirNextLTProDemi,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 10,
          ),
          const Divider(
            height: 1,
            thickness: 1,
            color: CLR.lightDarkRedShadeClr,
            indent: 55,
          ),
          const SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }
}

Widget widgetLogoutBt(context) {
  return ElevatedButton.icon(
    icon: Image.asset(
      IMAGE.logoutBt,
      height: 17.57,
      width: 20.25,
    ),
    onPressed: () {
      PreferenceUtil.setLogin(false);
      Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => LoginPage()),
      );
    },
    label: Text(
      I18N.of(context)!.logoutBtString ?? '',
      style: TextStyle(
        fontSize: 16,
        fontFamily: Fonts.AvenirNextLTProMedium,
      ),
    ),
    style: ElevatedButton.styleFrom(
        fixedSize: const Size(261, 44),
        primary: CLR.whiteClr,
        onPrimary: CLR.blackClr,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25))),
  );
}
