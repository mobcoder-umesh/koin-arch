import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mob_kitchen_freshers/presentation/leave/applyLeave/apply_leave_screen.dart';
import '../../utils/app_constant.dart';
import '../../widget/app_background.dart';
import '../../widget/common_widgets.dart';
import 'cubit/dashboard_cubit.dart';
import 'cubit/dashboard_state.dart';
import 'custom_drawer.dart';
import 'profile_screen.dart';

void main() => runApp(dashboard());

class dashboard extends StatelessWidget {
  const dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DashBoardScreen(),
    );
  }
}

class DashBoardScreen extends StatefulWidget {
  const DashBoardScreen({Key? key}) : super(key: key);

  @override
  State<DashBoardScreen> createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DashboardCubit(),
      child: Scaffold(
        drawer: BlocBuilder<DashboardCubit, DashboardState>(
          builder: (context, state) {
            return CustomDrawer.widgetShowDrawer(
              context,
              '${state.userProfileResponse.responseData?.employeeProfile?.email ?? ''}',
              '${state.userProfileResponse.responseData?.employeeProfile?.phone ?? ''}',
            );
          },
        ),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Background.widgetAppBackground(context, CLR.whiteClr),
              BlocBuilder<DashboardCubit, DashboardState>(
                builder: (context, state) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CommonWidgets.widgetPadding(
                        context,
                        Builder(builder: (BuildContext context) {
                          return IconButton(
                            onPressed: () {
                              Scaffold.of(context).openDrawer();
                            },
                            icon: Image.asset(
                              IMAGE.menuBt,
                              width: 22.8,
                              height: 14.48,
                            ),
                          );
                        }),
                        left: 19,
                        top: 55.52,
                      ),
                      CommonWidgets.widgetPadding(
                        context,
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 15,
                            ),
                            BlocBuilder<DashboardCubit, DashboardState>(
                              builder: (context, state) {
                                return Text(
                                  'Hello ${state.userProfileResponse.responseData?.employeeProfile?.firstName ?? ''}',
                                  style: TextStyle(
                                      fontSize: 23,
                                      fontFamily: Fonts.AvenirNextLTProMedium),
                                );
                              },
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              'WELCOME HRM DASHBOARD!',
                              style: TextStyle(
                                  fontSize: 24,
                                  fontFamily: Fonts.AvenirNextLTProBold),
                            ),
                          ],
                        ),
                        left: 30,
                      ),
                      Container(
                          margin: EdgeInsets.only(left: 20, right: 20, top: 30),
                          width: MediaQuery.of(context).size.width,
                          child: Image.asset(IMAGE.dashBoardViewImg)),
                      Row(
                        children: [
                          StatusHeading(EdgeInsets.only(left: 30, top: 20),
                              IMAGE.leavelogo, 19.47, 19.41, 'Your Leaves'),
                          Spacer(flex: 1),
                          ApplyLeaveButton(context),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: widgetLeaveCard(
                                '18',
                                'TOTAL LEAVES',
                                CLR.greencart,
                                165,
                                97,
                                55,
                                55,
                                IMAGE.leavecartinnericon,
                                EdgeInsets.only(left: 20, top: 10)),
                          ),
                          SizedBox(width: 6),
                          Expanded(
                            child: widgetLeaveCard(
                                (state.empDashboardDetails.remainingLeaves ?? 0)
                                    .toString(),
                                'REMAINING LEAVE',
                                CLR.greencart1,
                                165,
                                97,
                                55,
                                55,
                                IMAGE.leavecartinnericon,
                                EdgeInsets.only(top: 10, right: 20)),
                          ),
                        ],
                      ),
                      StatusHeading(EdgeInsets.only(left: 30, top: 20),
                          IMAGE.attendancelogo, 19.47, 19.41, 'Attendance'),
                      Row(
                        children: [
                          Expanded(
                            child: widgetLeaveCard(
                                (state.empDashboardDetails.lateComings ?? 0)
                                    .toString(),
                                'LATE COMINGS',
                                CLR.bluecart,
                                165,
                                97,
                                55,
                                55,
                                IMAGE.attendanceinnericon,
                                EdgeInsets.only(left: 20, top: 10)),
                          ),
                          SizedBox(width: 6),
                          Expanded(
                            child: widgetLeaveCard(
                                (state.empDashboardDetails.avgHours ?? 0)
                                    .toString(),
                                'AVG.HRS/DAY',
                                CLR.bluecart1,
                                165,
                                97,
                                55,
                                55,
                                IMAGE.attendanceinnericon,
                                EdgeInsets.only(top: 10, right: 20)),
                          ),
                        ],
                      ),
                      StatusHeading(EdgeInsets.only(left: 30, top: 20),
                          IMAGE.projectlogo, 19.47, 19.41, 'Projects'),
                      Row(
                        children: [
                          Expanded(
                            child: widgetLeaveCard(
                                (state.empDashboardDetails.completedProjects ??
                                        0)
                                    .toString(),
                                'COMPLETED',
                                CLR.darkredcart,
                                165,
                                97,
                                55,
                                55,
                                IMAGE.projectinnericon,
                                EdgeInsets.only(left: 20, top: 10)),
                          ),
                          SizedBox(width: 6),
                          Expanded(
                            child: widgetLeaveCard(
                                (state.empDashboardDetails.onGoingProjects ?? 0)
                                    .toString(),
                                'ON-GOING',
                                CLR.darkredcart1,
                                165,
                                97,
                                55,
                                55,
                                IMAGE.projectinnericon,
                                EdgeInsets.only(top: 10, right: 20)),
                          ),
                        ],
                      ),
                    ],
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget StatusHeading(EdgeInsetsGeometry? margin, String string, double? height,
    double? weight, String text) {
  return Container(
    margin: margin,
    child: Row(
      children: [
        Image.asset(
          string,
          width: weight,
          height: height,
        ),
        const SizedBox(
          width: 14,
        ),
        Text(
          text,
          style: TextStyle(
              fontFamily: Fonts.AvenirNextLTProDemi,
              fontSize: 16,
              fontWeight: FontWeight.bold),
        )
      ],
    ),
  );
}

Widget widgetLeaveCard(
    String text1,
    String text2,
    Color? color,
    double? width,
    double? height,
    double? height1,
    double? width1,
    String string,
    EdgeInsets? margin) {
  return Container(
    width: width,
    height: height,
    margin: margin,
    decoration:
        BoxDecoration(color: color, borderRadius: BorderRadius.circular(10)),
    child: Stack(
      children: [
        Container(
          height: height1,
          width: width1,
          margin: EdgeInsets.only(left: 110, top: 54.82),
          child: Image.asset(
            string,
            color: CLR.whiteClr,
            fit: BoxFit.fill,
          ),
        ),
        Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Text(
              text1,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
            SizedBox(
              height: 5,
            ),
            Center(
              child: Text(
                text2,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14, color: Colors.white),
              ),
            ),
          ],
        )
      ],
    ),
  );
}

Widget ApplyLeaveButton(context) {
  return Container(
    width: 115,
    height: 32,
    margin: const EdgeInsets.only(left: 0, top: 30, right: 20),
    child: TextButton(
      style: TextButton.styleFrom(
        primary: CLR.whiteClr,
        backgroundColor: CLR.maharoonClr,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25)),
        ),
      ),
      onPressed: () {
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => applyleavepage()));
      },
      child: Text(
        "Apply Leave",
        style: TextStyle(
          fontFamily: Fonts.AvenirNextLTProMedium,
          fontSize: 16,
        ),
      ),
    ),
  );
}
