import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mob_kitchen_freshers/presentation/dashboard/cubit/dashboard_cubit.dart';
import 'package:mob_kitchen_freshers/presentation/dashboard/cubit/dashboard_state.dart';
import 'package:mob_kitchen_freshers/utils/app_constant.dart';
import 'package:mob_kitchen_freshers/widget/app_background.dart';
import 'package:mob_kitchen_freshers/widget/back_navigation.dart';

class employeeProfileScreen extends StatefulWidget {
  const employeeProfileScreen({Key? key}) : super(key: key);

  @override
  State<employeeProfileScreen> createState() => _employeeProfileScreenState();
}

class _employeeProfileScreenState extends State<employeeProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) => DashboardCubit(),
        child: Stack(
          children: [
            Background.widgetAppBackground(context, CLR.whiteClr),
            BlocBuilder<DashboardCubit, DashboardState>(
              builder: (context, state) {
                return Column(
                  children: [
                    BackNavigation(pageName: "My Profile"),
                    Container(
                      height: 94,
                      width: 94,
                      margin: EdgeInsets.only(top: 47),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: CLR.darkblueClr2,
                        border: Border.all(color: CLR.darkblueClr1, width: 4),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(15),
                        child: Image.asset(
                          IMAGE.profileicon,
                          height: 14,
                          width: 14,
                        ),
                      ),
                    ),
                    UserDetails(
                        '${state.userProfileResponse.responseData?.employeeProfile?.firstName ?? ''} ${state.userProfileResponse.responseData?.employeeProfile?.lastName ?? ''} ${gender(state.userProfileResponse.responseData?.employeeProfile?.gender ?? 0)} ',
                        EdgeInsets.only(top: 29),
                        Fonts.AvenirNextLTProBold,
                        18),
                    UserDetails('${state.userProfileResponse.responseData?.employeeProfile?.empId ?? ''}', EdgeInsets.only(top: 2),
                        Fonts.AvenirNextLTProMedium, 17),
                    UserDetails('Design Head (design)', EdgeInsets.only(top: 2),
                        Fonts.AvenirNextLTProMedium, 17),
                    UserDetails('Date of Joining: ${state.userProfileResponse.responseData?.employeeProfile?.dateOfJoining ?? ''}',
                        EdgeInsets.only(top: 9), Fonts.AvenirNextLTProBold, 17),
                    UserDetails(
                        'Previous Experience: ${state.userProfileResponse.responseData?.employeeProfile?.experienceYear ?? ''} Years',
                        EdgeInsets.only(top: 2),
                        Fonts.AvenirNextLTProMedium,
                        17),
                    Row(
                      children: [
                        UserDetails(
                            '${state.userProfileResponse.responseData?.employeeProfile?.phone?? ''}',
                            EdgeInsets.only(top: 35, left: 64),
                            Fonts.AvenirNextLTProBold,
                            17),
                        UserDetails(
                            '${state.userProfileResponse.responseData?.employeeProfile?.alternateMob ?? ''}',
                            EdgeInsets.only(top: 35, left: 46),
                            Fonts.AvenirNextLTProBold,
                            17),
                      ],
                    ),
                    Row(
                      children: [
                        UserDetails(
                            'Mobile Number',
                            EdgeInsets.only(top: 2, left: 64),
                            Fonts.AvenirNextLTProMedium,
                            14),
                        UserDetails(
                            'Alternate Number',
                            EdgeInsets.only(top: 2, left: 46),
                            Fonts.AvenirNextLTProMedium,
                            14),
                      ],
                    ),
                    UserDetails('${state.userProfileResponse.responseData?.employeeProfile?.email ?? ''}', EdgeInsets.only(top: 18),
                        Fonts.AvenirNextLTProBold, 17),
                    UserDetails('official Mail', EdgeInsets.only(top: 2),
                        Fonts.AvenirNextLTProMedium, 17),
                    UserDetails(
                        'Qualification: dhthehetheth',
                        EdgeInsets.only(top: 13),
                        Fonts.AvenirNextLTProBold,
                        17),
                    UserDetails(
                        'Assignmed Project: gfghherhrwgw',
                        EdgeInsets.only(top: 2),
                        Fonts.AvenirNextLTProMedium,
                        17),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 22),
                              height: 110,
                              width: 315,
                              child: Card(
                                color: CLR.whiteClr,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    UserDetails(
                                        'Skill: ',
                                        EdgeInsets.only(top: 18, left: 15),
                                        Fonts.AvenirNextLTProBold,
                                        17),
                                    UserDetails(
                                        'PhotoSchop gdwcuidguic,wdgcwc,jgciwd,kbciwd,ccuw,rfghhjj',
                                        EdgeInsets.only(top: 18, left: 15),
                                        Fonts.AvenirNextLTProMedium,
                                        17),
                                  ],
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10),
                              height: 315,
                              width: 315,
                              child: Card(
                                color: CLR.whiteClr,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    UserDetails(
                                        'Permanent Address:',
                                        EdgeInsets.only(top: 18, left: 15),
                                        Fonts.AvenirNextLTProBold,
                                        17),
                                    UserDetails(
                                        '${state.userProfileResponse.responseData?.employeeProfile?.permanentAddress?.permanentAddressOne ?? ''}',
                                        EdgeInsets.only(top: 18, left: 15),
                                        Fonts.AvenirNextLTProMedium,
                                        17),
                                    UserDetails(
                                        'Previous Address:',
                                        EdgeInsets.only(top: 18, left: 15),
                                        Fonts.AvenirNextLTProBold,
                                        17),
                                    UserDetails(
                                        '${state.userProfileResponse.responseData?.employeeProfile?.previousAddress?.previousAddressOne ?? ''}',
                                        EdgeInsets.only(top: 18, left: 15),
                                        Fonts.AvenirNextLTProMedium,
                                        17),
                                    UserDetails(
                                        'Current Address:',
                                        EdgeInsets.only(top: 18, left: 15),
                                        Fonts.AvenirNextLTProBold,
                                        17),
                                    UserDetails(
                                        '${state.userProfileResponse.responseData?.employeeProfile?.currentAddress?.currentAddressOne ?? ''}',
                                        EdgeInsets.only(top: 18, left: 15),
                                        Fonts.AvenirNextLTProMedium,
                                        17),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

Widget UserDetails(String text, EdgeInsetsGeometry? margin, String FontFamily,
    double FontSize) {
  return Container(
    margin: margin,
    child: Text(
      text,
      style: TextStyle(
        fontFamily: FontFamily,
        fontSize: FontSize,
      ),
    ),
  );
}
String gender(int choice) {
  switch (choice) {
    case 1:
      return '(MALE)';
    case 2:
      return '(FEMALE)';
    default:
      return '(NOT KNOWN)';
  }
}
