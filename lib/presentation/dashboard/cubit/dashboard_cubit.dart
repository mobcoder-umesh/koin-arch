import 'package:bloc/bloc.dart';
import 'package:mob_kitchen_freshers/network/model/userProfile/user_profile_response.dart';
import '../../../domain/repositories/dashboard_repository.dart';
import '../../../network/model/dashboard/dashboard_response.dart';
import '../../../utils/app_constant.dart';
import '../../../utils/preference_util.dart';
import 'dashboard_state.dart';
class DashboardCubit extends Cubit<DashboardState> {
  DashboardCubit() : super(DashboardInitial(EmpDashboardDetails(),UserProfileResponse())) {
    getDashboardData();
  }
  void getDashboardData()async{
    final DashboardRepository repository = DashboardRepository() ;
    try {
      emit(DashboardLoading(state.empDashboardDetails,state.userProfileResponse));
      ACCESS_TOKEN  = await PreferenceUtil.getAccessToken();
      DashboardResponse response = (await repository.getDashboardList());
      UserProfileResponse profileResponse =(await repository.getUserProfileData());

      emit(DashBoardLoaded(response.responseData!.empDashboardDetails!,profileResponse));
    } catch (e) {
      emit(DashBoardError(state.empDashboardDetails,state.userProfileResponse));
    }
  }

}
