import 'package:equatable/equatable.dart';
import '../../../network/model/dashboard/dashboard_response.dart';
import '../../../network/model/userProfile/user_profile_response.dart';
abstract class DashboardState extends Equatable {
  EmpDashboardDetails empDashboardDetails;
  UserProfileResponse userProfileResponse;
  DashboardState(this.empDashboardDetails,this.userProfileResponse);
}
class DashboardInitial extends DashboardState {
  DashboardInitial(EmpDashboardDetails empDashboardDetails, UserProfileResponse userProfileResponse) : super(empDashboardDetails, userProfileResponse);

  @override
  List<Object> get props => [];
}
class DashboardLoading extends DashboardState{
  DashboardLoading(EmpDashboardDetails empDashboardDetails, UserProfileResponse userProfileResponse) : super(empDashboardDetails, userProfileResponse);

  @override
  List<Object?> get props => [];
}
class DashBoardLoaded extends DashboardState{
  DashBoardLoaded(EmpDashboardDetails empDashboardDetails, UserProfileResponse userProfileResponse) : super(empDashboardDetails, userProfileResponse);

  @override
  List<Object?> get props => [empDashboardDetails];
}
class DashBoardError extends DashboardState{
  DashBoardError(EmpDashboardDetails empDashboardDetails, UserProfileResponse userProfileResponse) : super(empDashboardDetails, userProfileResponse);

  @override
  List<Object?> get props => [];
}

