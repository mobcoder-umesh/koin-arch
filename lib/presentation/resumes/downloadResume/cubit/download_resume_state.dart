part of 'download_resume_cubit.dart';

abstract class DownloadResumeState extends Equatable {
  final double progress;
  const DownloadResumeState(this.progress);
}

class DownloadResumeInitial extends DownloadResumeState {
  DownloadResumeInitial(double progress) : super(progress);

  @override
  List<Object> get props => [];
}

class ProgressDialogState extends DownloadResumeState{
  ProgressDialogState(double progress) : super(progress);



  @override

  List<Object?> get props => [];

}
