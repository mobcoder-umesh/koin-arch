import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';

import '../../resume_page.dart';

part 'download_resume_state.dart';

class DownloadResumeCubit extends Cubit<DownloadResumeState> {
  DownloadResumeCubit() : super(DownloadResumeInitial(0.0));


  void onStartDownloading(String resumeUrl,String resumeName,context) async{
    Dio dio = Dio();
    double progress = 0.0;
    String url = resumeUrl;
    String fileName = resumeName;

    String path = await _getFilePath(fileName,url);

    await dio.download(
      url,
      path,
      onReceiveProgress: (receivedBytes, totalBytes) {

        progress = receivedBytes / totalBytes;
        //print('progress $progress');
        //for(double i=0.0;progress<=1.0;)
      //  emit(ProgressDialogState(progress));
        emit(ProgressDialogState(progress));

        if (progress==1.0) {
          OpenFile.open(path, type: 'application/pdf');
        }else{
          print('again an again');
          // emit(ProgressDialogState(progress));

        }

      },
      deleteOnError: true,
    ).then((_) {
      Navigator.pop(context);
    });

  }

  void onProgressOfDownloadChange(){

  }

  Future<String> _getFilePath(String filename,String url) async {
    final dir = await getApplicationDocumentsDirectory();

    return "${dir.path}/${filename}${getFileExtension(url)}";
  }
}
