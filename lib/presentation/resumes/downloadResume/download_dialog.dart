import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mob_kitchen_freshers/presentation/resumes/downloadResume/cubit/download_resume_cubit.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';

import '../../../utils/app_constant.dart';
import '../resume_page.dart';

class DownloadingDialog extends StatefulWidget {
  final String resumeUrl;
  final String resumeName;

  const DownloadingDialog(
      {Key? key, required this.resumeUrl, required this.resumeName})
      : super(key: key);

  @override
  State<DownloadingDialog> createState() => _DownloadingDialogState();
}

class _DownloadingDialogState extends State<DownloadingDialog> {
  // Dio dio = Dio();
  // double progress = 0.0;
  //
  // void startDownloading() async {
  //   String url = widget.resumeUrl;
  //   String fileName = widget.resumeName;
  //
  //   String path = await _getFilePath(fileName, url);
  //
  //   await dio.download(
  //     url,
  //     path,
  //     onReceiveProgress: (receivedBytes, totalBytes) {
  //       progress = receivedBytes / totalBytes;
  //
  //       if (progress == 1.0) {
  //         OpenFile.open(path, type: 'application/pdf');
  //       } else {
  //
  //       }
  //
  //       print(progress);
  //     },
  //     deleteOnError: true,
  //   ).then((_) {
  //     Navigator.pop(context);
  //   });
  // }
  //
  // Future<String> _getFilePath(String filename, String url) async {
  //   final dir = await getApplicationDocumentsDirectory();
  //
  //   return "${dir.path}/${filename}${getFileExtension(url)}";
  // }
  //
  // @override
  // void initState() {
  //   super.initState();
  //
  //   // BlocProvider.of<DownloadResumeCubit>(context).onStartDownloading(
  //   //     widget.resumeUrl, widget.resumeName);
  // }

  @override
  Widget build(BuildContext context) {
    //String downloadingProgress = (progress * 100).toInt().toString();

    return BlocProvider(
      create: (context) => DownloadResumeCubit(),
      child: AlertDialog(
        backgroundColor: CLR.whiteClr,
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            const CircularProgressIndicator.adaptive(),
            const SizedBox(
              height: 20,
            ),
            /* BlocConsumer<DownloadResumeCubit, DownloadResumeState>(
                listener: (context, state) {
                  if(state is ProgressDialogState)
                  print("Downloading: ${(state.progress*100).toInt().toString()}%",);
                  // do stuff here based on BlocA's state
                },

              builder: (context, state) {
                BlocProvider.of<DownloadResumeCubit>(context).onStartDownloading(
                    widget.resumeUrl, widget.resumeName,context);
                print('progress=====>${state.progress}');
                return Text(
                  "Downloading: ${(state.progress*100).toInt().toString()}%",
                  style: const TextStyle(
                    color: CLR.greyShade,
                    fontSize: 17,
                  ),
                );
              },
            ),*/
            BlocBuilder<DownloadResumeCubit, DownloadResumeState>(
                builder: (context, state) {
              BlocProvider.of<DownloadResumeCubit>(context).onStartDownloading(
                  widget.resumeUrl, widget.resumeName, context);
              return Text(
                "Downloading: ${(state.progress * 100).toInt().toString()}%",
                style: const TextStyle(
                  color: CLR.greyShade,
                  fontSize: 17,
                ),
              );
              //print('progress=====>${state.progress}');
              // if(state is ProgressDialogState){
              //   return Text(
              //     "Downloading: ${(state.progress*100).toInt().toString()}%",
              //     style: const TextStyle(
              //       color: CLR.greyShade,
              //       fontSize: 17,
              //     ),
              //   );
              // }else{
              //   return Text(
              //     "Downloading: 0%",
              //     style: const TextStyle(
              //       color: CLR.greyShade,
              //       fontSize: 17,
              //     ),
              //   );
            }),
          ],
        ),
      ),
    );
  }
}

@override
Future<void> download({required String url}) async {
  bool hasPermission = await _requestWritePermission();
  if (!hasPermission) return;
}

Future<bool> _requestWritePermission() async {
  await Permission.storage.request();
  return await Permission.storage.request().isGranted;
}
