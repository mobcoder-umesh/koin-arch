import 'package:bloc/bloc.dart';
import 'package:mob_kitchen_freshers/network/model/deleteResume/delete_resume_response.dart';






import '../../../domain/repositories/resume_repository.dart';

import '../../../network/model/resumeList/models/AllResume.dart';
import '../../../network/model/resumeList/resume_list_response.dart';
import 'resume_state.dart';




class ResumeCubit extends Cubit<ResumeState> {
  ResumeCubit() : super(ResumeInitial([])){
    getResumeData();
  }



  void getResumeData()async{
    final ResumeRepository repository = ResumeRepository() ;
    try {

      emit(ResumeLoading(state.resumeList));
      ResumeListResponse response = (await repository.getResumeList());

      List<AllResume> listOfResumes = response.responseData?.resumeInfo?.allResume??[];
      emit(ResumeLoaded(listOfResumes));
    } catch (e) {
      emit(ResumeError(state.resumeList));
    }
  }
  void onDeleteResume(String resumeId)async{
    final ResumeRepository repository = ResumeRepository() ;



      DeleteResumeResponse response = (await repository.deleteResume(resumeId));
      if(response.statusCode==1){
        getResumeData();
      }else{
        emit(ResumeError(state.resumeList));
      }






  }


}
