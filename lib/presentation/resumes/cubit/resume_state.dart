

import 'package:equatable/equatable.dart';
import 'package:mob_kitchen_freshers/network/model/resumeList/models/AllResume.dart';

import '../../../network/model/userProfile/user_profile_response.dart';
import '../../../network/model/vendor/vendor_response.dart';

abstract class ResumeState extends Equatable {

  List<AllResume> resumeList;


  ResumeState(this.resumeList);
}

class ResumeInitial extends ResumeState {
  ResumeInitial(List<AllResume> resumeList) : super(resumeList);




  @override
  List<Object> get props => [];
}

class ResumeLoading extends ResumeState{
  ResumeLoading(List<AllResume> resumeList) : super(resumeList);




  @override
  List<Object?> get props => [];

}

class ResumeLoaded extends ResumeState{
  ResumeLoaded(List<AllResume> resumeList) : super(resumeList);
  @override
  List<Object?> get props => [];







}


class ResumeError extends ResumeState{
  ResumeError(List<AllResume> resumeList) : super(resumeList);




  @override
  List<Object?> get props => [];

}
