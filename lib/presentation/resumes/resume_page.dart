
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mob_kitchen_freshers/presentation/resumes/cubit/resume_cubit.dart';
import 'package:mob_kitchen_freshers/presentation/resumes/cubit/resume_state.dart';

import '../../utils/app_constant.dart';
import '../../utils/date_utils.dart';
import '../../widget/app_background.dart';
import '../../widget/back_navigation.dart';
import 'downloadResume/download_dialog.dart';





class ResumesPage extends StatefulWidget {
  const ResumesPage({Key? key}) : super(key: key);

  @override
  State<ResumesPage> createState() => _ResumesPageState();
}

class _ResumesPageState extends State<ResumesPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ResumeCubit(),
      child: Scaffold(
        body: Stack(
          children: [
            const AppBackground(height: 218),
            Column(
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                BackNavigation(pageName: 'My Resume'),
                SizedBox(
                  height: 50,
                ),
                AddResumeButton(),

                BlocBuilder<ResumeCubit, ResumeState>(
                  builder: (context, state) {
                    return Expanded(
                      child: ListView.builder(
                          itemCount: state.resumeList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ResumeInfoCard(
                              resumeName: state.resumeList[index].title ?? '',
                              date: state.resumeList[index].updated ?? '',
                              fileUrl: state.resumeList[index].resume ?? '',
                              resumeId: state.resumeList[index].id??'',
                            );
                          }),
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ResumeInfoCard extends StatelessWidget {
  final String resumeName;
  final String date;
  final String fileUrl;
  final String resumeId;

  const ResumeInfoCard(
      {Key? key,
      required this.resumeName,
      required this.date,
      required this.fileUrl,
      required this.resumeId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
      width: MediaQuery.of(context).size.width,
      height: 113,
      decoration: BoxDecoration(
          color: CLR.whiteClr,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: CLR.unselectedChoiceColor)),
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              getExtensionImage(fileUrl),
              width: 43,
              height: 56,
            ),
            const SizedBox(
              width: 14,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    resumeName,
                    style: TextStyle(
                        color: CLR.maharoonClr,
                        fontSize: 17,
                        fontFamily: Fonts.AvenirNextLTProDemi),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            DateUtil.dateFormatter(date),
                            style: TextStyle(
                                color: CLR.greyShade,
                                fontSize: 17,
                                fontFamily: Fonts.AvenirNextLTProMedium),
                          ),
                          const SizedBox(
                            height: 3,
                          ),
                          Text(
                            'Last Updated on',
                            style: TextStyle(
                                color: CLR.greyShade,
                                fontSize: 13,
                                fontFamily: Fonts.AvenirNextLTProMedium),
                          ),
                        ],
                      ),
                      const Spacer(
                        flex: 1,
                      ),
                      DownloadResume(resumeName: resumeName,resumeUrl: fileUrl,),
                      const SizedBox(
                        width: 15,
                      ),
                      DeleteResume(resumeID: resumeId),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AddResumeButton extends StatelessWidget {
  const AddResumeButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: () {
          showAlertDialog(context);
        },
        style: ElevatedButton.styleFrom(
          primary: CLR.maharoonClr,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
          fixedSize: const Size(216, 50),
        ),
        child: Text(
          'Add New Resume',
          style: TextStyle(
            fontSize: 16,
            fontFamily: Fonts.AvenirNextLTProMedium,
          ),
        ));
  }
}

class DeleteResume extends StatelessWidget {
  final String resumeID;
  const DeleteResume({Key? key,required this.resumeID}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  InkWell(
      onTap: (){
        BlocProvider.of<ResumeCubit>(context).onDeleteResume(resumeID);

      },
      child: Image.asset(
        IMAGE.deleteIcon,
        width: 19.34,
        height: 22,
      ),
    );
  }
}

class DownloadResume extends StatelessWidget {
  final String resumeUrl;
  final String resumeName;
  const DownloadResume({Key? key, required this.resumeUrl,required this.resumeName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        print('djkmnvm');
        showDialog(
          context: context,
          builder: (context) => DownloadingDialog(resumeUrl: resumeUrl,resumeName: resumeName,),
        );

      },
      child: Image.asset(
        IMAGE.downloadIcon,
        width: 17.43,
        height: 22,
      ),
    );
  }
}



String? getFileExtension(String fileName) {
  try {
    return "." + fileName.split('.').last;
  } catch (e) {
    return null;
  }
}

String getExtensionImage(String fileName) {
  switch (getFileExtension(fileName)) {
    case '.pdf':
      return IMAGE.pdfImage;
    case '.docx':
      return IMAGE.docxImage;
    case '.docs':
      return IMAGE.docsImage;
    default:
      return IMAGE.logo;
  }
}

showAlertDialog(context) {

  AlertDialog alert = AlertDialog(
    insetPadding: EdgeInsets.all(20),
    shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10.0))),

    title: Text('Add New Resume',
        style: TextStyle(
          fontSize: 17,
          fontFamily: Fonts.AvenirNextLTProBold,
        ),
        textAlign: TextAlign.center),
    content: Container(
      width: MediaQuery.of(context).size.width,

      height: 224,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Enter Your Title For Resume',
              style: TextStyle(
                fontSize: 14,
                fontFamily: Fonts.AvenirNextLTProMedium,
              ),
              textAlign: TextAlign.left),
          const SizedBox(height: 7,),
          const SizedBox(
            height: 42,
            child: TextField(
              //controller: subjectController,
              cursorColor: CLR.blackClr,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  borderSide: BorderSide(
                    width: 2,
                    color: Color(0xff000000),
                  ),
                ),

              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                borderSide: BorderSide(
                  width: 1,
                  color: Color(0xff000000),
                ),
              ),
                enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(5)),
            borderSide: BorderSide(
              width: 1,
              color: Color(0xff000000),
            ),),



              ),
            ),
          ),
          const SizedBox(height: 21,),
          Text('Upload File',
              style: TextStyle(
                fontSize: 14,
                fontFamily: Fonts.AvenirNextLTProMedium,
              ),
              textAlign: TextAlign.left),
          const SizedBox(height: 7,),
           SizedBox(
            height: 42,
            child: TextField(
              cursorColor: CLR.blackClr,
              decoration: InputDecoration(
                suffixIconColor: CLR.blueShade2,
                suffixIcon:  Material(
                  color: CLR.blueShade2,

                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(4.0),
                    bottomRight: Radius.circular(4.0),
                  ),


                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        right:  BorderSide(color: CLR.blackClr),
                        bottom:  BorderSide(color: CLR.blackClr),
                        top:  BorderSide(color: CLR.blackClr),

                      ),




                    ),
                    width: 100,

                    child: Center(
                      child: Text('Upload', style: TextStyle(fontFamily: Fonts.AvenirNextLTProMedium,
                      fontSize: 16,color: CLR.whiteClr),
                      textAlign: TextAlign.center,),
                    ),
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  borderSide: BorderSide(
                    width: 2,
                    color: Color(0xff000000),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  borderSide: BorderSide(
                    width: 1,
                    color: Color(0xff000000),
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                  borderSide: BorderSide(
                    width: 1,
                    color: Color(0xff000000),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 20,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                  onPressed: () {

                  },
                  style: ElevatedButton.styleFrom(
                    primary: CLR.maharoonClr,
                    shape:
                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                    fixedSize: const Size(150, 50),
                  ),
                  child: Text(
                    'Done',
                    style: TextStyle(
                      fontSize: 16,
                      fontFamily: Fonts.AvenirNextLTProMedium,
                    ),
                  )),

            ],
          )
        ],
      ),
    ),

  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}




