import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mob_kitchen_freshers/utils/app_constant.dart';
import 'package:mob_kitchen_freshers/utils/preference_util.dart';

import '../../../domain/repositories/login_repository.dart';
import '../../../network/http_client.dart';
import '../../../network/model/login/login_response.dart';
import '../../../network/model/resumeList/resume_list_end_point.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  //final HttpClient client;
  LoginCubit() : super(LoginInitial(false, false, true));

  void onEmailChange(String email) {
    RegExp regExp = RegExp(
      r'^[a-zA-Z0-9-._]+@[a-z]+\.[a-z]{2,3}$',
    );
    if (email != null && email.isNotEmpty && regExp.hasMatch(email)) {
      print('Email success $email');
      emit(LoginCredentialsState(
          true, state.isPasswordValid, state.isPasswordVisible));
    } else {
      print('Email fail $email');
      emit(LoginCredentialsState(
          false, state.isPasswordValid, state.isPasswordVisible));
    }
  }

  void onPasswordChange(String password) {
    RegExp regExp1 = RegExp(
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,32}$');
    if (password != null && password != '' && regExp1.hasMatch(password)) {
      emit(LoginCredentialsState(
          state.isEmailValid, true, state.isPasswordVisible));
    } else {
      emit(LoginCredentialsState(
          state.isEmailValid, false, state.isPasswordVisible));
    }
  }

  void onPasswordVisibilityChange() {
    if (state.isPasswordVisible) {
      emit(LoginPasswordVisibilityState(
          state.isEmailValid, state.isPasswordValid, false));
    } else {
      emit(LoginPasswordVisibilityState(
          state.isEmailValid, state.isPasswordValid, true));
    }
  }

  void onLogin(String email, String password) async {
    final LoginRepository repository = LoginRepository();

    LoginResponse responseData =
        await repository.getLoginResponse(email, password);
   // print('error data ====>${responsedata.errorData?.responseMessage ?? ''}');


    if (responseData.statusCode == 1) {
      //print('check=====>${responsedata.responseData!.employeeProfile!.accessToken}');

      PreferenceUtil.setAccessToken(responseData.responseData?.employeeProfile?.accessToken ?? '');

      PreferenceUtil.setLogin(true);
      PreferenceUtil.setEmployeeID(responseData.responseData?.employeeProfile?.employeeId??'');

      emit(LoginSuccessState(
          state.isEmailValid,
          state.isPasswordValid,
          state.isPasswordVisible,
          responseData.responseData?.employeeProfile?.email??''));

    } else {
      print('error data ====>${responseData.errorData?.errorMessage ?? ''}');
      emit(LoginErrorState(
          state.isEmailValid,
          state.isPasswordValid,
          state.isPasswordVisible,
          responseData.errorData?.responseMessage ?? ''));
    }
  }
}
