part of 'login_cubit.dart';

abstract class LoginState extends Equatable {
  final bool isEmailValid;
  final bool isPasswordValid;
  final bool isPasswordVisible ;
  const LoginState(this.isEmailValid,this.isPasswordValid,this.isPasswordVisible);
  @override
  List<Object?> get props => [isEmailValid,isPasswordValid,isPasswordVisible];


}

class LoginInitial extends LoginState {
  LoginInitial(bool isEmailValid, bool isPasswordValid, bool isPasswordVisible) : super(isEmailValid, isPasswordValid, isPasswordVisible);


  // @override
  // List<Object?> get props => [];
}

class LoginCredentialsState extends LoginState{
  LoginCredentialsState(bool isEmailValid, bool isPasswordValid, bool isPasswordVisible) : super(isEmailValid, isPasswordValid, isPasswordVisible);


}

class LoginSuccessState extends LoginState{
  final String email;

  LoginSuccessState(bool isEmailValid, bool isPasswordValid, bool isPasswordVisible,this.email) : super(isEmailValid, isPasswordValid, isPasswordVisible);


  @override
  List<Object?> get props => [email];

}

class LoginPasswordVisibilityState extends LoginState{
  LoginPasswordVisibilityState(bool isEmailValid, bool isPasswordValid, bool isPasswordVisible) : super(isEmailValid, isPasswordValid, isPasswordVisible);



}

class LoginErrorState extends LoginState{
  final String errorMessage;

  LoginErrorState(bool isEmailValid, bool isPasswordValid, bool isPasswordVisible,this.errorMessage) : super(isEmailValid, isPasswordValid, isPasswordVisible);



  @override
  List<Object?> get props => [errorMessage];

}
