import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:mob_kitchen_freshers/presentation/projectRecord/project_record_page.dart';

import 'package:mob_kitchen_freshers/presentation/dashboard/dashboard_screen.dart';
import 'package:mob_kitchen_freshers/presentation/resumes/resume_page.dart';


import 'package:mob_kitchen_freshers/widget/back_navigation.dart';
//

import '../../localization/i18n.dart';
import '../../utils/app_constant.dart';
import '../../utils/base_widget.dart';
import '../../utils/preference_util.dart';
import '../../widget/common_widgets.dart';
import '../dailyAttendence/daily_attendence_screen.dart';
import '../leave/leaveRecord/leave_record_screen.dart';
import '../sampleDashboard/sample_dashboard_page.dart';
import 'cubit/login_cubit.dart';

enum SourceType { email, password }

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isObscure = true;
  bool isLoader1 = false;
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  isSignIn() async {
    bool? login = await PreferenceUtil.isLogin();
    print(login);
    if (login ?? false) {
      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>DashBoardScreen()));
    }
  }

  @override
  void initState() {

    isSignIn();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<LoginCubit, LoginState>(
        listener: (context, state) {
          if(state is LoginSuccessState){
            Navigator.of(context).push(


              //MaterialPageRoute(builder: (context)=>ProjectRecordPage())


                MaterialPageRoute(builder: (context)=>DashBoardScreen())
              //MaterialPageRoute(builder: (context)=> ResumesPage())


            );
          }


        },
        child: Stack(
          children: [

            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Image.asset(IMAGE.appBackground, fit: BoxFit.fill),
            ),
            Container(
              alignment: Alignment.topCenter,
              child: _widgetIconImage(
                context,
                IMAGE.logo,
                EdgeInsets.only(
                  top: 167,
                  //left: 75,
                ),
              ),
            ),
            Row(
              children: [
                _widgetIconImage(
                  context,
                  IMAGE.mailMessage,
                  EdgeInsets.fromLTRB(38, 300, 0, 0),
                ),
                CommonWidgets.widgetPadding(
                    context,
                    _labelTexts(
                      'Email',
                      TextStyle(
                        fontSize: 12,
                        color: CLR.blackClr,
                      ),
                    ),
                    left: 10,
                    top: 300),
              ],
            ),
            CommonWidgets.widgetPadding(
                context,
                _signInTextField(
                  'Email',
                  TextStyle(
                    fontSize: 15,
                  ),
                  emailController,
                  SourceType.email,
                  SizedBox(),
                  false,
                ),
                left: 38,
                top: 323,
                right: 38),
            Row(
              children: [
                _widgetIconImage(
                  context,
                  IMAGE.passwordLock,
                  EdgeInsets.fromLTRB(38, 410, 0, 0),
                ),
                CommonWidgets.widgetPadding(
                    context, _labelTexts('Password', TextStyle(fontSize: 12)),
                    left: 10, top: 410),
              ],
            ),
            BlocBuilder<LoginCubit, LoginState>(
              builder: (context, state) {
                return CommonWidgets.widgetPadding(
                    context,
                    _signInTextField(
                        'Password',
                        TextStyle(
                          fontSize: 15,
                        ),
                        passwordController,
                        SourceType.password,
                        IconButton(
                          onPressed: () {
                            BlocProvider.of<LoginCubit>(context)
                                .onPasswordVisibilityChange();
                          },
                          icon: state.isPasswordVisible
                              ? Icon(Icons.visibility_off)
                              : Icon(Icons.visibility),
                          color: CLR.blackClr,
                        ),
                        state.isPasswordVisible),
                    left: 38,
                    top: 435,
                    right: 38);
              },
            ),
            CommonWidgets.widgetPadding(context,
                BlocBuilder<LoginCubit, LoginState>(builder: (context, state) {
                  if (state is LoginSuccessState) {
                    return Text(state.email);
                  } else {
                    return Container();
                  }
                }), top: 490, left: 80),
            CommonWidgets.widgetPadding(context,
                BlocBuilder<LoginCubit, LoginState>(builder: (context, state) {
                  if (state is LoginErrorState) {
                    return Text(state.errorMessage,
                      style: TextStyle(color: Colors.red),);
                  } else {
                    return Container();
                  }
                }), top: 690, left: 80),
            _widgetLoginButton(context),
            Visibility(visible: false, child: BaseWidget()),

          ],
        ),
      ),
    );
  }

  Widget _widgetIconImage(context, String img, EdgeInsetsGeometry? margin) {
    return Container(
      margin: margin,
      child: Image.asset(
        img,
      ),
    );
  }

  Widget _widgetLoginButton(context) {
    return GestureDetector(
      onTap: () {
        BlocProvider.of<LoginCubit>(context)
            .onLogin(emailController.text, passwordController.text);
      },
      child: Container(
        margin: const EdgeInsets.only(top: 550, left: 38, right: 38),
        width: MediaQuery.of(context).size.width,
        height: 50,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(25), color: CLR.blackClr),
        child: CommonWidgets.widgetPadding(
          context,
          Center(
            child: Text(
              'Login',
              style: TextStyle(
                  color: CLR.whiteClr,
                  fontSize: 16,
                  fontFamily: Fonts.AvenirNextLTProRegular),
            ),
          ),
        ),
      ),
    );
  }

  Widget _labelTexts(String txt, TextStyle? styling) {
    return Container(
      child: Text(
        txt,
        style: styling,
      ),
    );
  }

  Widget typeCheck(SourceType sourceType) {
    if (sourceType == SourceType.email) {
      return SizedBox(
        width: 1,
        height: 1,
      );
    } else {
      return IconButton(
          onPressed: () {},
          icon:
          isObscure ? Icon(Icons.visibility_off) : Icon(Icons.visibility));
    }
  }

  Widget _signInTextField(
      String hint,
      TextStyle? styling,
      TextEditingController controller,
      SourceType sourceType,
      Widget icon,
      bool isObscure) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 50,
      decoration: BoxDecoration(
        color: CLR.whiteClr,
        borderRadius: BorderRadius.circular(30),
      ),
      child: TextFormField(
        obscureText: isObscure,
        controller: controller,
        cursorColor: CLR.blackClr,
        decoration: InputDecoration(
          hintText: hint,
          suffixIcon: icon,
          contentPadding: EdgeInsets.only(left: 26),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(30)),
            borderSide: BorderSide(
              color: Colors.black,
              width: 50,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(30)),
            borderSide: BorderSide(
              color: Colors.black,
              width: 2,
            ),
          ),
        ),
      ),
    );
  }
}
