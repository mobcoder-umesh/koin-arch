


import '../../network/http_client.dart';
import '../../network/model/userProfile/user_profile_response.dart';
import '../../network/model/vendor/vendor_response.dart';
import '../../network/network_result.dart';

class DashboardRepository{

 Future<VendorResponse> getVendorList()async{

   HttpClient client = HttpClient.env();

   NetworkResult response = await client.vendorRequest();
   VendorResponse data = response.data;
   print('data2=====>${data.responseData!.vendorList!}');

   return data;
 }

 Future<UserProfileResponse> getUserProfileData()async{

   HttpClient client = HttpClient.env();

   NetworkResult response = await client.userProfileRequest();
   UserProfileResponse data = response.data;
   print('data2=====>${data.responseData!.employeeProfile!.firstName!}');

   return data;
 }
}