




import '../../network/http_client.dart';
import '../../network/model/login/login_response.dart';
import '../../network/network_result.dart';

class LoginRepository{



 Future<LoginResponse> getLoginResponse(String email,String password) async{
    HttpClient client = HttpClient.env();

      NetworkResult response = await client.loginRequest(email, password);
      LoginResponse data = response.data;
      return data;
  }

}