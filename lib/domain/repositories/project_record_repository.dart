

import '../../network/http_client.dart';
import '../../network/model/projectRecord/project_record_response.dart';
import '../../network/network_result.dart';

class ProjectRecordRepository{


   Future<ProjectRecordResponse> getProjectRecords()async{

     HttpClient client = HttpClient.env();

     NetworkResult response = await client.projectRecordRequest();
     ProjectRecordResponse data = response.data;
     //print('data2=====>${data.responseData!.vendorList!}');

     return data;
   }





 }