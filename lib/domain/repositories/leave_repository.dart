
import '../../network/http_client.dart';

import '../../network/model/leave/leaveRecord/leave_record_response.dart';
import '../../network/network_result.dart';
//
// class ApplyLeaveRepository {
//   Future<ApplyLeaveRepository> postApplyLeave() async {
//     HttpClient client = HttpClient.env();
//
//     NetworkResult response = await client.ApplyLeaveRequest();
//     ApplyLeaveResponse data = response.data;
//     print('data2=====>${data.responseData!}');
//
//     return data;
//   }
// }


class LeaveRecordRepository {
  Future<LeaveRecordResponse> getLeaveRecord() async {
    print('hello');
    HttpClient client = HttpClient.env();

    NetworkResult response = await client.leaveRecordRequest();
    LeaveRecordResponse data = response.data;
    print('data2=====>${data.responseData!}');

    return data;
  }
}
