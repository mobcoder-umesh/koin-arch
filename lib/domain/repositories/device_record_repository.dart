// import 'package:mob_kitchen_freshers/network/model/dailyAttendence/daily_attendence_response.dart';

import '../../network/http_client.dart';

import '../../network/model/deviceRecord/device_record_response.dart';
import '../../network/network_result.dart';

class DeviceRecordRepository {
  Future<DeviceRecordResponse> getDeviceRecord() async {
    HttpClient client = HttpClient.env();

    NetworkResult response = await client.deviceRecordRequest();
    DeviceRecordResponse data = response.data;
    print('data2=====>${data.responseData!}');

    return data;
  }
}
