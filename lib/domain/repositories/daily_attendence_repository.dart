import 'package:mob_kitchen_freshers/network/model/dailyAttendence/daily_attendence_response.dart';

import '../../network/http_client.dart';

import '../../network/network_result.dart';

class DailyAttendenceRepository {
  Future<DailyAttendenceResponse> getDailyAttendence() async {
    HttpClient client = HttpClient.env();

    NetworkResult response = await client.dailyAttendenceRequest();
    DailyAttendenceResponse data = response.data;
    print('data2=====>${data.responseData.toString()}');

    return data;
  }
}
