import 'package:mob_kitchen_freshers/network/model/reimbursement/model/ReimbursementResponse.dart';

import '../../network/http_client.dart';
import '../../network/model/tickets/ticket_response.dart';
import '../../network/network_result.dart';

class TicketsRepository {

  Future<TicketResponse> getTicketDetails() async {
    HttpClient client = HttpClient.env();

    NetworkResult response = await client.getTicketRequest();
    TicketResponse data = response.data;
    print('data2=====>${data.responseData!.ticketDetails}');

    return data;
  }
}