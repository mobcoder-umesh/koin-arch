import 'package:mob_kitchen_freshers/network/model/deleteResume/delete_resume_response.dart';
import 'package:mob_kitchen_freshers/network/model/resumeList/resume_list_response.dart';
import 'package:mob_kitchen_freshers/utils/app_constant.dart';
import 'package:mob_kitchen_freshers/utils/preference_util.dart';

import '../../network/http_client.dart';
import '../../network/network_result.dart';

class ResumeRepository {

  Future<ResumeListResponse> getResumeList() async {
    HttpClient client = HttpClient.env();
    EMPLOYEE_ID = await PreferenceUtil.getEmployeeID();

    NetworkResult response = await client.resumeListRequest(EMPLOYEE_ID??'');
    ResumeListResponse data = response.data;
    //print('data2=====>${data.responseData!.vendorList!}');

    return data;
  }

  Future<DeleteResumeResponse> deleteResume(String resumeId) async {
    HttpClient client = HttpClient.env();

    NetworkResult response = await client.deleteResumeRequest(resumeId);
    DeleteResumeResponse data = response.data;
    //print('data2=====>${data.responseData!.vendorList!}');

    return data;
  }
}
