import '../../network/http_client.dart';
import '../../network/model/dashboard/dashboard_response.dart';
import '../../network/model/userProfile/user_profile_response.dart';
import '../../network/network_result.dart';

class DashboardRepository {
  Future<DashboardResponse> getDashboardList() async {
    HttpClient client = HttpClient.env();
    NetworkResult response = await client.dashboardRequest();
    DashboardResponse data = response.data;
    return data;
  }
  Future<UserProfileResponse> getUserProfileData() async {
    HttpClient client = HttpClient.env();
    NetworkResult response = await client.userProfileRequest();
    UserProfileResponse data = response.data;
    print('data2=====>${data.responseData!.employeeProfile!.firstName!}');
    return data;
  }
}
