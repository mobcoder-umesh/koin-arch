import 'dart:async';
import 'dart:core';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';


import 'package:mob_kitchen_freshers/domain/repositories/raise_new_ticket_repository.dart';
import 'package:mob_kitchen_freshers/network/model/RaiseNewTicket/raise_new_ticket_request.dart';
import 'package:mob_kitchen_freshers/network/model/claimRaise/claim_raise_end_point.dart';
import 'package:mob_kitchen_freshers/network/model/claimRaise/claim_raise_request.dart';
import 'package:mob_kitchen_freshers/network/model/deleteResume/delete_resume_end_point.dart';
import 'package:mob_kitchen_freshers/network/model/deleteResume/delete_resume_request.dart';
import 'package:mob_kitchen_freshers/network/model/deleteResume/delete_resume_response.dart';
import 'package:mob_kitchen_freshers/network/model/reimbursement/model/ReimbursementResponse.dart';
import 'package:mob_kitchen_freshers/network/model/reimbursement/reimbursement_end_point.dart';
import 'package:mob_kitchen_freshers/network/model/reimbursement/reimbursement_request.dart';

import 'package:mob_kitchen_freshers/network/model/projectRecord/project_record_end_point.dart';
import 'package:mob_kitchen_freshers/network/model/projectRecord/project_record_request.dart';
import 'package:mob_kitchen_freshers/network/model/projectRecord/project_record_response.dart';
import 'package:mob_kitchen_freshers/network/model/resumeList/resume_list_end_point.dart';
import 'package:mob_kitchen_freshers/network/model/resumeList/resume_list_request.dart';
import 'package:mob_kitchen_freshers/network/model/resumeList/resume_list_response.dart';

import 'package:mob_kitchen_freshers/network/model/userProfile/user_profile_end_point.dart';
import 'package:mob_kitchen_freshers/network/model/userProfile/user_profile_request.dart';
import 'package:mob_kitchen_freshers/network/model/userProfile/user_profile_response.dart';
import 'package:mob_kitchen_freshers/network/model/vendor/vendor_end_point.dart';
import 'package:mob_kitchen_freshers/network/model/vendor/vendor_request.dart';
import 'package:mob_kitchen_freshers/network/model/vendor/vendor_response.dart';
import 'package:mob_kitchen_freshers/presentation/dashboard/dashboard_screen.dart';


import '../utils/app_constant.dart';
import 'model/dailyAttendence/daily_attendence_end_point.dart';
import 'model/dailyAttendence/daily_attendence_request.dart';
import 'model/dailyAttendence/daily_attendence_response.dart';
import 'model/deviceRecord/device_record_end_point.dart';
import 'model/deviceRecord/device_record_request.dart';
import 'model/deviceRecord/device_record_response.dart';
import 'model/leave/leaveRecord/leave_record_end_point.dart';
import 'model/leave/leaveRecord/leave_record_request.dart';
import 'model/leave/leaveRecord/leave_record_response.dart';
import 'model/dashboard/dashboard_end_point.dart';
import 'model/dashboard/dashboard_request.dart';
import 'model/dashboard/dashboard_response.dart';
import 'model/login/login_end_point.dart';
import 'model/login/login_request.dart';
import 'model/login/login_response.dart';
import 'model/projectRecord/project_record_end_point.dart';
import 'model/projectRecord/project_record_request.dart';
import 'model/projectRecord/project_record_response.dart';
import 'model/reimbursement/model/ReimbursementResponse.dart';
import 'model/reimbursement/reimbursement_end_point.dart';
import 'model/reimbursement/reimbursement_request.dart';
import 'model/resumeList/resume_list_end_point.dart';
import 'model/resumeList/resume_list_request.dart';
import 'model/resumeList/resume_list_response.dart';
import 'model/tickets/ticket_end_point.dart';
import 'model/tickets/ticket_request.dart';
import 'model/tickets/ticket_response.dart';
import 'model/userProfile/user_profile_end_point.dart';
import 'model/userProfile/user_profile_request.dart';
import 'model/userProfile/user_profile_response.dart';
import 'model/vendor/vendor_end_point.dart';
import 'model/vendor/vendor_request.dart';
import 'model/vendor/vendor_response.dart';
import 'network_result.dart';

Future<void> _injectHeaders(RequestOptions request) async {
  final headers = request.headers;
  headers['Authorization'] = 'Basic aHJtX2FkbWluOmFkbWluQGhybQ==';
  if (ACCESS_TOKEN!.isNotEmpty == true) {
    headers['accessToken'] = ACCESS_TOKEN;
  }
  request.headers = headers;
}

Dio _setupDio() {
  final _instance = Dio(BaseOptions(
    connectTimeout: 15000,
    baseUrl: 'https://devhrmapi.mobcoder.com/hrm/api/',
  ));
  _instance.interceptors
      .add(InterceptorsWrapper(onRequest: (request, handler) async {
    if (request.path.contains('health-card')) {
      request.connectTimeout = 120000;
    }
    await _injectHeaders(request);
    debugPrint(request.uri.toString());
    debugPrint('headers = ${request.headers}');
    debugPrint('data = ${request.data.toString()}');
    return handler.next(request); //continue
    // If you want to resolve the request with some custom data，
    // you can resolve a `Response` object eg: return `dio.resolve(response)`.
    // If you want to reject the request with a error message,
    // you can reject a `DioError` object eg: return `dio.reject(dioError)`
  }, onResponse: (response, handler) {
    // Do something with response data
    debugPrint(response.requestOptions.uri.toString());
    debugPrint(response.data.toString());
    assert(response.data is Map<String, dynamic>);
    // return response.requestOptions.extra['decoder'](response.data);
    return handler.next(response); // continue
    // If you want to reject the request with a error message,
    // you can reject a `DioError` object eg: return `dio.reject(dioError)`
  }, onError: (DioError e, handler) {
    debugPrint(e.toString());
    return handler.next(e); //continue
    // If you want to resolve the request with some custom data，
    // you can resolve a `Response` object eg: return `dio.resolve(response)`.
  }));
  // final performanceInterceptor = DioFirebasePerformanceInterceptor();
  // _instance.interceptors.add(performanceInterceptor);
  return _instance;
}

class HttpClient {
  final Dio _dio;

  HttpClient._(this._dio);

  HttpClient.env() : this._(_setupDio());

  Future<NetworkResult<LoginResponse>> loginRequest(
    String email,
    String password,
  ) async {
    final requestData = LoginRequest(email, password);
    //print("check====>${requestData.}");
    final endpointUseCase = LoginEndpointUsecase(_dio);
    //print("check====>${endpointUseCase.}");
    return endpointUseCase.request(requestData);
  }

  Future<NetworkResult<VendorResponse>> vendorRequest() async {
    final requestData = VendorRequest();
    //print("check====>${requestData.}");
    final endpointUseCase = VendorEndpointUseCase(_dio);
    //print("check====>${endpointUseCase.}");
    return endpointUseCase.request(requestData);
  }

  Future<NetworkResult<ReimbursementsResponse>>
      getReimbursementRequest() async {
    final requestData = ReimbursementRequest();
    //print("check====>${requestData.}");
    final endpointUseCase = ReimbursementEndpointUseCase(_dio);
    //print("check====>${endpointUseCase.}");
    return endpointUseCase.request(requestData);
  }

  Future<NetworkResult<UserProfileResponse>> userProfileRequest() async {
    final requestData = UserProfileRequest();
    final endpointUseCase = UserProfileEndpointUseCase(_dio);
    return endpointUseCase.request(requestData);
  }

  // Future<NetworkResult<ClaimRaiseResponse>> ClaimRaiseRequest(
  //     String tittle,
  //     String amount,
  //     String expenseDate,
  //     String expenseDescription,
  //     String billUpload,
  //     ) async {
  //   final requestData = ClaimRaiseRequest(tittle, amount,expenseDate,expenseDescription,billUpload);
  //   //print("check====>${requestData.}");
  //   final endpointUseCase = ClaimRaiseEndpointUseCase(_dio);
  //   //print("check====>${endpointUseCase.}");
  //   return endpointUseCase.request(requestData);
  // }
  Future<NetworkResult<TicketResponse>> getTicketRequest() async {
    final requestData = TicketRequest();
    //print("check====>${requestData.}");
    final endpointUseCase = TicketEndpointUseCase(_dio);
    //print("check====>${endpointUseCase.}");
    return endpointUseCase.request(requestData);
  }

// Future<NetworkResult<RaiseNewTicketResponse>> RaiseNewTicketRequest(
//     String category,
//     String subject,
//
//     String ticketDescription,
//
//     ) async {
//   final requestData = RaiseNewTicketRequest(category, subject, ticketDescription); //print("check====>${requestData.}");
//   final endpointUseCase =RaiseNewTicketEndpointUseCase(_dio);
//   //print("check====>${endpointUseCase.}");
//   return endpointUseCase.request(requestData);
// }

  Future<NetworkResult<DailyAttendenceResponse>>
      dailyAttendenceRequest() async {
    final requestData = DailyAttendenceRequest();
    final endpointUseCase = DailyAttendenceEndpointUseCase(_dio);
    return endpointUseCase.request(requestData);
  }

  Future<NetworkResult<DeviceRecordResponse>> deviceRecordRequest() async {
    final requestData = DeviceRecordRequest();
    final endpointUseCase = DeviceRecordEndpointUseCase(_dio);
    return endpointUseCase.request(requestData);
  }

  Future<NetworkResult<LeaveRecordResponse>> leaveRecordRequest() async {
    final requestData = LeaveRecordRequest();
    final endpointUseCase = LeaveRecordEndpointUseCase(_dio);
    return endpointUseCase.request(requestData);
  }

  // Future<NetworkResult<ApplyLeaveResponse>> applyLeaveRequest() async{
  //   final requestData = ApplyLeaveRequest();
  //   final endpointUseCase = ApplyLeaveEndpointUseCase(_dio);
  //   return endpointUseCase.request(requestData);
  // }

  Future<NetworkResult<ProjectRecordResponse>> projectRecordRequest() async {
    final requestData = ProjectRecordRequest();
    final endpointUseCase = ProjectRecordEndpointUsecase(_dio);
    return endpointUseCase.request(requestData);
  }

  Future<NetworkResult<ResumeListResponse>> resumeListRequest(
      String employeeId
      ) async {
    final requestData = ResumeListRequest(employeeId);
    final endpointUseCase = ResumeListEndpointUseCase(_dio,employeeId);
    return endpointUseCase.request(requestData);
  }
  Future<NetworkResult<DeleteResumeResponse>> deleteResumeRequest(
      String resumeId
      ) async {
    final requestData = DeleteResumeRequest(resumeId);
    final endpointUseCase = DeleteResumeEndpointUseCase(_dio);

    return endpointUseCase.request(requestData);
  }

  // Future<NetworkResult<LoginResponse>> loginRequest(
  //     String phoneNumber,
  //     String secret,
  //     ) async {
  //   final requestData = LoginRequest(phoneNumber, secret);
  //   final endpointUsecase = LoginEndpointUsecase(_dio);
  //   return endpointUsecase.request(requestData);
  // }

  // Future<NetworkResult<MeResponse>> meRequest() async {
  //   final requestData = MeRequest();
  //   final endpointUsecase = MeEndpointUsecase(_dio);
  //   return endpointUsecase.request(requestData);
  // }

  Future<NetworkResult<DashboardResponse>> dashboardRequest() async {
    final requestData = DashboardRequest();
    final endpointUseCase = DashboardEndpointUseCase(_dio);
    return endpointUseCase.request(requestData);
  }
}
