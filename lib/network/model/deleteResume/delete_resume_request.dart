import '../../base/base_request_model.dart';


abstract class _Keys {
  static const String resumeId = 'resumeId';

}
class DeleteResumeRequest extends BaseRequestModel {
  final String resumeId;

  DeleteResumeRequest(this.resumeId);

  @override
  Map<String, dynamic> get parameters => {
    _Keys.resumeId: this.resumeId,
  };
}
