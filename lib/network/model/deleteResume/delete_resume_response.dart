import 'package:mob_kitchen_freshers/network/base/base_response_model.dart';

class DeleteResumeResponse extends BaseResponseModel {
  DeleteResumeResponse({
    this.statusCode,
    this.responseData,
    this.time,});

  DeleteResumeResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    responseData = json['responseData'] != null ? DeleteResumeResponseData.fromJson(json['responseData']) : null;
    time = json['time'];
  }
  int? statusCode;
  DeleteResumeResponseData? responseData;
  String? time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (responseData != null) {
      map['responseData'] = responseData?.toJson();
    }
    map['time'] = time;
    return map;
  }

}

class DeleteResumeResponseData {
  DeleteResumeResponseData({
    this.message,
    this.isAdmin,});

  DeleteResumeResponseData.fromJson(dynamic json) {
    message = json['message'];
    isAdmin = json['isAdmin'];
  }
  String? message;
  int? isAdmin;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    map['isAdmin'] = isAdmin;
    return map;
  }

}