

import 'package:dio/dio.dart';

import '../../base/base_end_point.dart';
import 'delete_resume_request.dart';
import 'delete_resume_response.dart';


const String _path = 'v1/resume/delete';
class DeleteResumeEndpointUseCase
    extends DeleteEndpoint<DeleteResumeRequest, DeleteResumeResponse> {
  DeleteResumeEndpointUseCase(Dio _dio) : super(_dio, _path);
}
