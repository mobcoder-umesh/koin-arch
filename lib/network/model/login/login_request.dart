

import '../../base/base_request_model.dart';

abstract class _Keys {
  static const String email = 'email';
  static const String password = 'password';
}

class LoginRequest extends BaseRequestModel {
  final String email;
  final String password;

  LoginRequest(
      this.email,
      this.password,
      );

  @override
  Map<String, dynamic> get parameters => {
    _Keys.email: this.email,
    _Keys.password: this.password,
  };
}

