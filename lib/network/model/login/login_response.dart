

import '../../base/base_response_model.dart';
import '../error/error_response.dart';



class LoginResponse extends BaseResponseModel {
  LoginResponse({
    this.statusCode,
    this.responseData,
    this.errorData,
    this.time,});

  LoginResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    responseData = json['responseData'] != null ? LoginResponseData.fromJson(json['responseData']) : null;
    errorData = json['error'] != null ? ErrorData.fromJson(json['error']) : null;
    time = json['time'];
  }
  int? statusCode;
  LoginResponseData? responseData;
  ErrorData? errorData;
  String? time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (responseData != null) {
      map['responseData'] = responseData?.toJson();
    }
    if (errorData != null) {
      map['error'] = errorData?.toJson();
    }
    map['time'] = time;
    return map;
  }


}

class EmployeeProfile {
  EmployeeProfile({
    this.acessToken,
    this.accessToken,
    this.employeeId,
    this.firstName,
    this.lastName,
    this.empId,
    this.email,
    this.personalEmail,
    this.gender,
    this.phone,
    this.dob,
    this.experienceYear,
    this.doj,
    this.status,
    this.profileImage,
    this.isDelete,});

  EmployeeProfile.fromJson(dynamic json) {
    acessToken = json['acessToken'];
    accessToken = json['accessToken'];
    employeeId = json['employeeId'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    empId = json['empId'];
    email = json['email'];
    personalEmail = json['personalEmail'];
    gender = json['gender'];
    phone = json['phone'];
    dob = json['dob'];
    experienceYear = json['experienceYear'];
    doj = json['doj'];
    status = json['status'];
    profileImage = json['profileImage'];
    isDelete = json['isDelete'];
  }
  String? acessToken;
  String? accessToken;
  String? employeeId;
  String? firstName;
  String? lastName;
  String? empId;
  String? email;
  String? personalEmail;
  int? gender;
  String? phone;
  int? dob;
  int? experienceYear;
  int? doj;
  int? status;
  String? profileImage;
  int? isDelete;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['acessToken'] = acessToken;
    map['accessToken'] = accessToken;
    map['employeeId'] = employeeId;
    map['firstName'] = firstName;
    map['lastName'] = lastName;
    map['empId'] = empId;
    map['email'] = email;
    map['personalEmail'] = personalEmail;
    map['gender'] = gender;
    map['phone'] = phone;
    map['dob'] = dob;
    map['experienceYear'] = experienceYear;
    map['doj'] = doj;
    map['status'] = status;
    map['profileImage'] = profileImage;
    map['isDelete'] = isDelete;
    return map;
  }

}

class LoginResponseData {
  LoginResponseData({
    this.message,
    this.employeeProfile,});

  LoginResponseData.fromJson(dynamic json) {
    message = json['message'];
    employeeProfile = json['employeeProfile'] != null ? EmployeeProfile.fromJson(json['employeeProfile']) : null;
  }
  String? message;
  EmployeeProfile? employeeProfile;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    if (employeeProfile != null) {
      map['employeeProfile'] = employeeProfile?.toJson();
    }
    return map;
  }

}




