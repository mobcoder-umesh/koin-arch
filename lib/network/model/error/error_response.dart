

import '../../base/base_response_model.dart';

class ErrorData extends BaseResponseModel{
  int? errorCode;
  String? responseMessage;
  String? errorMessage;

  ErrorData({this.errorCode, this.responseMessage, this.errorMessage});

  ErrorData.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    responseMessage = json['responseMessage'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['errorCode'] = this.errorCode;
    data['responseMessage'] = this.responseMessage;
    data['errorMessage'] = this.errorMessage;
    return data;
  }
}