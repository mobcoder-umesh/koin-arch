class DeviceRecordResponseData {
  DeviceRecordResponseData({
      this.message, 
      this.deviceList, 
      this.totalDevices, 
      this.laptops, 
      this.desktops, 
      this.phones,});

  DeviceRecordResponseData.fromJson(dynamic json) {
    message = json['message'];
    // if (json['deviceList'] != null) {
    //   deviceList = [];
    //   json['deviceList'].forEach((v) {
    //     deviceList?.add(Dynamic.fromJson(v));
    //   });
    // }
    totalDevices = json['totalDevices'];
    laptops = json['laptops'];
    desktops = json['desktops'];
    phones = json['phones'];
  }
  String? message;
  List<dynamic>? deviceList;
  int? totalDevices;
  int? laptops;
  int? desktops;
  int? phones;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    if (deviceList != null) {
      map['deviceList'] = deviceList?.map((v) => v.toJson()).toList();
    }
    map['totalDevices'] = totalDevices;
    map['laptops'] = laptops;
    map['desktops'] = desktops;
    map['phones'] = phones;
    return map;
  }

}