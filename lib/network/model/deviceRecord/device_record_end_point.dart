import 'package:dio/dio.dart';

import '../../base/base_end_point.dart';

import 'device_record_request.dart';
import 'device_record_response.dart';


const String _path = 'v1/device/listByEmp';

class DeviceRecordEndpointUseCase
    extends GetEndpoint<DeviceRecordRequest, DeviceRecordResponse> {
  DeviceRecordEndpointUseCase(Dio _dio) : super(_dio, _path);
}
