import 'package:mob_kitchen_freshers/network/base/base_response_model.dart';

import 'ResponseData.dart';

class DeviceRecordResponse extends BaseResponseModel{
  DeviceRecordResponse({
      this.statusCode, 
      this.responseData, 
      this.time,});

  DeviceRecordResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    responseData = json['responseData'] != null ? DeviceRecordResponseData.fromJson(json['responseData']) : null;
    time = json['time'];
  }
  int? statusCode;
  DeviceRecordResponseData? responseData;
  String? time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (responseData != null) {
      map['responseData'] = responseData?.toJson();
    }
    map['time'] = time;
    return map;
  }

}