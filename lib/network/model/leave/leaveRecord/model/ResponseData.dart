import 'LeaveDetails.dart';

class LeaveRecordResponseData {
  LeaveRecordResponseData({
      this.message, 
      this.leaveDetails, 
      this.totalCasualLeaveTaken, 
      this.totalEarnedLeaveTaken, 
      this.totalSickLeaveTaken, 
      this.totalLwpTaken, 
      this.totalComOffTaken, 
      this.totalHalfCasual, 
      this.totalHalfEarned, 
      this.totalHalfSick, 
      this.totalHalfLwp, 
      this.totalHalfComOff, 
      this.totalLeaves, 
      this.pendingCount, 
      this.approvedCount, 
      this.rejectedCount, 
      this.dataCount,});

  LeaveRecordResponseData.fromJson(dynamic json) {
    message = json['message'];
    if (json['leaveDetails'] != null) {
      leaveDetails = [];
      json['leaveDetails'].forEach((v) {
        leaveDetails?.add(LeaveDetails.fromJson(v));
      });
    }
    totalCasualLeaveTaken = json['totalCasualLeaveTaken'];
    totalEarnedLeaveTaken = json['totalEarnedLeaveTaken'];
    totalSickLeaveTaken = json['totalSickLeaveTaken'];
    totalLwpTaken = json['totalLwpTaken'];
    totalComOffTaken = json['totalComOffTaken'];
    totalHalfCasual = json['totalHalfCasual'];
    totalHalfEarned = json['totalHalfEarned'];
    totalHalfSick = json['totalHalfSick'];
    totalHalfLwp = json['totalHalfLwp'];
    totalHalfComOff = json['totalHalfComOff'];
    totalLeaves = json['totalLeaves'];
    pendingCount = json['pendingCount'];
    approvedCount = json['approvedCount'];
    rejectedCount = json['rejectedCount'];
    dataCount = json['dataCount'];
  }
  String? message;
  List<LeaveDetails>? leaveDetails;
  int? totalCasualLeaveTaken;
  int? totalEarnedLeaveTaken;
  int? totalSickLeaveTaken;
  int? totalLwpTaken;
  int? totalComOffTaken;
  int? totalHalfCasual;
  int? totalHalfEarned;
  int? totalHalfSick;
  int? totalHalfLwp;
  int? totalHalfComOff;
  int? totalLeaves;
  int? pendingCount;
  int? approvedCount;
  int? rejectedCount;
  int? dataCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    if (leaveDetails != null) {
      map['leaveDetails'] = leaveDetails?.map((v) => v.toJson()).toList();
    }
    map['totalCasualLeaveTaken'] = totalCasualLeaveTaken;
    map['totalEarnedLeaveTaken'] = totalEarnedLeaveTaken;
    map['totalSickLeaveTaken'] = totalSickLeaveTaken;
    map['totalLwpTaken'] = totalLwpTaken;
    map['totalComOffTaken'] = totalComOffTaken;
    map['totalHalfCasual'] = totalHalfCasual;
    map['totalHalfEarned'] = totalHalfEarned;
    map['totalHalfSick'] = totalHalfSick;
    map['totalHalfLwp'] = totalHalfLwp;
    map['totalHalfComOff'] = totalHalfComOff;
    map['totalLeaves'] = totalLeaves;
    map['pendingCount'] = pendingCount;
    map['approvedCount'] = approvedCount;
    map['rejectedCount'] = rejectedCount;
    map['dataCount'] = dataCount;
    return map;
  }

}