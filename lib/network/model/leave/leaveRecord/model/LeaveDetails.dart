class LeaveDetails {
  LeaveDetails({
      this.leaveId, 
      this.employeeId, 
      this.leaveType, 
      this.startDate, 
      this.endDate, 
      this.leaveMsg, 
      this.reply, 
      this.status, 
      this.created,});

  LeaveDetails.fromJson(dynamic json) {
    leaveId = json['leaveId'];
    employeeId = json['employeeId'];
    leaveType = json['leaveType'];
    startDate = json['startDate'];
    endDate = json['endDate'];
    leaveMsg = json['leaveMsg'];
    reply = json['reply'];
    status = json['status'];
    created = json['created'];
  }
  String? leaveId;
  String? employeeId;
  int? leaveType;
  int? startDate;
  int? endDate;
  String? leaveMsg;
  String? reply;
  int? status;
  String? created;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['leaveId'] = leaveId;
    map['employeeId'] = employeeId;
    map['leaveType'] = leaveType;
    map['startDate'] = startDate;
    map['endDate'] = endDate;
    map['leaveMsg'] = leaveMsg;
    map['reply'] = reply;
    map['status'] = status;
    map['created'] = created;
    return map;
  }

}