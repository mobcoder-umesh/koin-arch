import 'package:dio/dio.dart';

import '../../../base/base_end_point.dart';

import 'leave_record_response.dart';
import 'leave_record_request.dart';




const String _path = 'v1/leave/getRemainingLeaves';

class LeaveRecordEndpointUseCase
    extends GetEndpoint<LeaveRecordRequest, LeaveRecordResponse> {
  LeaveRecordEndpointUseCase(Dio _dio) : super(_dio, _path);
}
