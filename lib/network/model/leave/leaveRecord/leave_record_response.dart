import 'package:mob_kitchen_freshers/network/base/base_response_model.dart';

import 'model/ResponseData.dart';



class LeaveRecordResponse extends BaseResponseModel{
  LeaveRecordResponse({
      this.statusCode, 
      this.responseData, 
      this.time,});

  LeaveRecordResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    responseData = json['responseData'] != null ? LeaveRecordResponseData.fromJson(json['responseData']) : null;
    time = json['time'];
  }
  int? statusCode;
  LeaveRecordResponseData? responseData;
  String? time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (responseData != null) {
      map['responseData'] = responseData?.toJson();
    }
    map['time'] = time;
    return map;
  }

}