
import 'package:mob_kitchen_freshers/network/base/base_response_model.dart';

class ProjectRecordResponse extends BaseResponseModel {
  ProjectRecordResponse({
    this.statusCode,
    this.responseData,
    this.time,});

  ProjectRecordResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    responseData = json['responseData'] != null ? ProjectRecordResponseData.fromJson(json['responseData']) : null;
    time = json['time'];
  }
  int? statusCode;
  ProjectRecordResponseData? responseData;
  String? time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (responseData != null) {
      map['responseData'] = responseData?.toJson();
    }
    map['time'] = time;
    return map;
  }

  @override
  String toString() {
    return 'ProjectRecordResponse{statusCode: $statusCode, responseData: $responseData, time: $time}';
  }
}

class ProjectRecordResponseData {
  ProjectRecordResponseData({
    this.message,
    this.projectDetails,
    this.totalProjects,
    this.onGoingProjects,
    this.completedProjects,
    this.inCompleteProjects,});

  ProjectRecordResponseData.fromJson(dynamic json) {
    message = json['message'];
    if (json['projectDetails'] != null) {
      projectDetails = [];
      json['projectDetails'].forEach((v) {
        projectDetails?.add(ProjectDetails.fromJson(v));
      });
    }
    totalProjects = json['totalProjects'];
    onGoingProjects = json['onGoingProjects'];
    completedProjects = json['completedProjects'];
    inCompleteProjects = json['inCompleteProjects'];
  }
  String? message;
  List<ProjectDetails>? projectDetails;
  int? totalProjects;
  int? onGoingProjects;
  int? completedProjects;
  int? inCompleteProjects;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    if (projectDetails != null) {
      map['projectDetails'] = projectDetails?.map((v) => v.toJson()).toList();
    }
    map['totalProjects'] = totalProjects;
    map['onGoingProjects'] = onGoingProjects;
    map['completedProjects'] = completedProjects;
    map['inCompleteProjects'] = inCompleteProjects;
    return map;
  }

  @override
  String toString() {
    return 'ProjectRecordResponseData{message: $message, projectDetails: $projectDetails, totalProjects: $totalProjects, onGoingProjects: $onGoingProjects, completedProjects: $completedProjects, inCompleteProjects: $inCompleteProjects}';
  }
}

class ProjectDetails {
  ProjectDetails({
    this.projectId,
    this.projectName,
    this.projectManagerId,
    this.projectManager,
    this.clientName,
    this.startDate,
    this.endDate,
    this.status,
    this.assignedTo,});

  ProjectDetails.fromJson(dynamic json) {
    projectId = json['projectId'];
    projectName = json['projectName'];
    projectManagerId = json['projectManagerId'];
    projectManager = json['projectManager'];
    clientName = json['clientName'];
    startDate = json['startDate'];
    endDate = json['endDate'];
    status = json['status'];
    assignedTo = json['assignedTo'] != null ? json['assignedTo'].cast<String>() : [];
  }
  String? projectId;
  String? projectName;
  String? projectManagerId;
  String? projectManager;
  String? clientName;
  int? startDate;
  int? endDate;
  int? status;
  List<String>? assignedTo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['projectId'] = projectId;
    map['projectName'] = projectName;
    map['projectManagerId'] = projectManagerId;
    map['projectManager'] = projectManager;
    map['clientName'] = clientName;
    map['startDate'] = startDate;
    map['endDate'] = endDate;
    map['status'] = status;
    map['assignedTo'] = assignedTo;
    return map;
  }

  @override
  String toString() {
    return 'ProjectDetails{projectId: $projectId, projectName: $projectName, projectManagerId: $projectManagerId, projectManager: $projectManager, clientName: $clientName, startDate: $startDate, endDate: $endDate, status: $status, assignedTo: $assignedTo}';
  }
}