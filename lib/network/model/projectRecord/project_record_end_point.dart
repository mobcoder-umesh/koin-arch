import 'package:dio/dio.dart';

import '../../base/base_end_point.dart';
import 'project_record_request.dart';
import 'project_record_response.dart';


const String _path = 'v1/project/projectListByEmp';

class ProjectRecordEndpointUsecase
    extends GetEndpoint<ProjectRecordRequest, ProjectRecordResponse> {
  ProjectRecordEndpointUsecase(Dio _dio) : super(_dio, _path);
}

