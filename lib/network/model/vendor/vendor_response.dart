

import 'package:mob_kitchen_freshers/network/base/base_response_model.dart';



class VendorResponse extends BaseResponseModel {
  VendorResponse({
    this.statusCode,
    this.responseData,
    this.time,});

  VendorResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    responseData = json['responseData'] != null ? VendorResponseData.fromJson(json['responseData']) : null;
    time = json['time'];
  }
  int? statusCode;
  VendorResponseData? responseData;
  String? time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (responseData != null) {
      map['responseData'] = responseData?.toJson();
    }
    map['time'] = time;
    return map;
  }

}

class VendorResponseData {
  VendorResponseData({
    this.message,
    this.vendorList,
    this.isAdmin,});

  VendorResponseData.fromJson(dynamic json) {
    message = json['message'];
    if (json['vendorList'] != null) {
      vendorList = [];
      json['vendorList'].forEach((v) {
        vendorList?.add(VendorList.fromJson(v));
      });
    }
    isAdmin = json['isAdmin'];
  }
  String? message;
  List<VendorList>? vendorList;
  int? isAdmin;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    if (vendorList != null) {
      map['vendorList'] = vendorList?.map((v) => v.toJson()).toList();
    }
    map['isAdmin'] = isAdmin;
    return map;
  }

}

class VendorList {
  VendorList({
    this.vendorId,
    this.name,
    this.phoneNo,
    this.email,
    this.image,
    this.aboutUs,});

  VendorList.fromJson(dynamic json) {
    vendorId = json['vendorId'];
    name = json['name'];
    phoneNo = json['phoneNo'];
    email = json['email'];
    image = json['image'];
    aboutUs = json['aboutUs'];
  }
  String? vendorId;
  String? name;
  String? phoneNo;
  String? email;
  String? image;
  String? aboutUs;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['vendorId'] = vendorId;
    map['name'] = name;
    map['phoneNo'] = phoneNo;
    map['email'] = email;
    map['image'] = image;
    map['aboutUs'] = aboutUs;
    return map;
  }

}