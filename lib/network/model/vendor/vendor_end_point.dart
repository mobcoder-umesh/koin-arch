import 'package:dio/dio.dart';
import '../../base/base_end_point.dart';
import 'vendor_request.dart';
import 'vendor_response.dart';


const String _path = 'v1/food/vendors';

class VendorEndpointUseCase
    extends GetEndpoint<VendorRequest,VendorResponse>{
 VendorEndpointUseCase(Dio _dio): super(_dio,_path);
}

