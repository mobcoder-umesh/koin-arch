import '../../base/base_request_model.dart';

abstract class _Keys {
  static const String tittle = 'tittle';
  static const String amount = 'amount';
  static const String expenseDate = 'expenseDate';
  static const String expenseDescription = 'expenseDescription';
  static const String billUpload = 'billUpload';
}

class ClaimRaiseRequest extends BaseRequestModel {
  final String tittle;
  final String amount;
  final String expenseDate;
  final String expenseDescription;
  final String billUpload;

  ClaimRaiseRequest(this.tittle, this.amount, this.expenseDate, this.expenseDescription, this.billUpload);


  @override
  Map<String, dynamic> get parameters => {
    _Keys.tittle: this.tittle,
    _Keys.amount: this.amount,
    _Keys.expenseDate: this.expenseDate,
    _Keys.expenseDescription: this.expenseDescription,
    _Keys.billUpload: this.billUpload,
  };
}
