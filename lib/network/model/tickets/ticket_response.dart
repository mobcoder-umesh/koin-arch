import 'dart:convert';

import '../../base/base_response_model.dart';
/// statusCode : 1
/// responseData : {"message":"Tickets added by Employees","ticketDetails":[{"ticketId":"61f1d1192997ed0351a6d462","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":1,"created":"2022-01-26T22:54:17.700Z","reply":""},{"ticketId":"61f1d11c2997ed0351a6d465","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":2,"created":"2022-01-26T22:54:20.485Z","reply":"","resolvedDate":"2022-05-27T04:44:50.228Z"},{"ticketId":"61f1d1202997ed0351a6d468","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":2,"created":"2022-01-26T22:54:24.672Z","reply":"resolve","resolvedDate":"2022-05-31T10:10:15.925Z"},{"ticketId":"61f1d1202997ed0351a6d46b","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":1,"created":"2022-01-26T22:54:24.819Z","reply":""},{"ticketId":"61f1d1202997ed0351a6d46e","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":1,"created":"2022-01-26T22:54:24.940Z","reply":""},{"ticketId":"61f1d1212997ed0351a6d471","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":1,"created":"2022-01-26T22:54:25.008Z","reply":""},{"ticketId":"61f1d1212997ed0351a6d474","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":1,"created":"2022-01-26T22:54:25.093Z","reply":""},{"ticketId":"61f1d1542997ed0351a6d495","employeeId":"61dffbe393b6d45922e2ddbb","category":4,"subject":"Api not working","description":"Same","date":null,"status":1,"created":"2022-01-26T22:55:16.298Z","reply":""}],"totalTickets":8}
/// time : "2022-07-18T04:49:26.161Z"

TicketResponse ticketResponseFromJson(String str) => TicketResponse.fromJson(json.decode(str));
String ticketResponseToJson(TicketResponse data) => json.encode(data.toJson());
class TicketResponse extends BaseResponseModel {
  TicketResponse({
    int? statusCode,
    ResponseData? responseData,
    String? time,}){
    _statusCode = statusCode;
    _responseData = responseData;
    _time = time;
  }

  TicketResponse.fromJson(dynamic json) {
    _statusCode = json['statusCode'];
    _responseData = json['responseData'] != null ? ResponseData.fromJson(json['responseData']) : null;
    _time = json['time'];
  }
  int? _statusCode;
  ResponseData? _responseData;
  String? _time;
  TicketResponse copyWith({  int? statusCode,
    ResponseData? responseData,
    String? time,
  }) => TicketResponse(  statusCode: statusCode ?? _statusCode,
    responseData: responseData ?? _responseData,
    time: time ?? _time,
  );
  int? get statusCode => _statusCode;
  ResponseData? get responseData => _responseData;
  String? get time => _time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = _statusCode;
    if (_responseData != null) {
      map['responseData'] = _responseData?.toJson();
    }
    map['time'] = _time;
    return map;
  }

}

/// message : "Tickets added by Employees"
/// ticketDetails : [{"ticketId":"61f1d1192997ed0351a6d462","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":1,"created":"2022-01-26T22:54:17.700Z","reply":""},{"ticketId":"61f1d11c2997ed0351a6d465","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":2,"created":"2022-01-26T22:54:20.485Z","reply":"","resolvedDate":"2022-05-27T04:44:50.228Z"},{"ticketId":"61f1d1202997ed0351a6d468","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":2,"created":"2022-01-26T22:54:24.672Z","reply":"resolve","resolvedDate":"2022-05-31T10:10:15.925Z"},{"ticketId":"61f1d1202997ed0351a6d46b","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":1,"created":"2022-01-26T22:54:24.819Z","reply":""},{"ticketId":"61f1d1202997ed0351a6d46e","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":1,"created":"2022-01-26T22:54:24.940Z","reply":""},{"ticketId":"61f1d1212997ed0351a6d471","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":1,"created":"2022-01-26T22:54:25.008Z","reply":""},{"ticketId":"61f1d1212997ed0351a6d474","employeeId":"61dffbe393b6d45922e2ddbb","category":1,"subject":"Need to format the laptop","description":"Just  for fun","date":null,"status":1,"created":"2022-01-26T22:54:25.093Z","reply":""},{"ticketId":"61f1d1542997ed0351a6d495","employeeId":"61dffbe393b6d45922e2ddbb","category":4,"subject":"Api not working","description":"Same","date":null,"status":1,"created":"2022-01-26T22:55:16.298Z","reply":""}]
/// totalTickets : 8

ResponseData responseDataFromJson(String str) => ResponseData.fromJson(json.decode(str));
String responseDataToJson(ResponseData data) => json.encode(data.toJson());
class ResponseData {
  ResponseData({
    String? message,
    List<TicketDetails>? ticketDetails,
    int? totalTickets,}){
    _message = message;
    _ticketDetails = ticketDetails;
    _totalTickets = totalTickets;
  }

  ResponseData.fromJson(dynamic json) {
    _message = json['message'];
    if (json['ticketDetails'] != null) {
      _ticketDetails = [];
      json['ticketDetails'].forEach((v) {
        _ticketDetails?.add(TicketDetails.fromJson(v));
      });
    }
    _totalTickets = json['totalTickets'];
  }
  String? _message;
  List<TicketDetails>? _ticketDetails;
  int? _totalTickets;
  ResponseData copyWith({  String? message,
    List<TicketDetails>? ticketDetails,
    int? totalTickets,
  }) => ResponseData(  message: message ?? _message,
    ticketDetails: ticketDetails ?? _ticketDetails,
    totalTickets: totalTickets ?? _totalTickets,
  );
  String? get message => _message;
  List<TicketDetails>? get ticketDetails => _ticketDetails;
  int? get totalTickets => _totalTickets;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = _message;
    if (_ticketDetails != null) {
      map['ticketDetails'] = _ticketDetails?.map((v) => v.toJson()).toList();
    }
    map['totalTickets'] = _totalTickets;
    return map;
  }

}

/// ticketId : "61f1d1192997ed0351a6d462"
/// employeeId : "61dffbe393b6d45922e2ddbb"
/// category : 1
/// subject : "Need to format the laptop"
/// description : "Just  for fun"
/// date : null
/// status : 1
/// created : "2022-01-26T22:54:17.700Z"
/// reply : ""

TicketDetails ticketDetailsFromJson(String str) => TicketDetails.fromJson(json.decode(str));
String ticketDetailsToJson(TicketDetails data) => json.encode(data.toJson());
class TicketDetails {
  TicketDetails({
    String? ticketId,
    String? employeeId,
    int? category,
    String? subject,
    String? description,
    dynamic date,
    int? status,
    String? created,
    String? reply,}){
    _ticketId = ticketId;
    _employeeId = employeeId;
    _category = category;
    _subject = subject;
    _description = description;
    _date = date;
    _status = status;
    _created = created;
    _reply = reply;
  }

  TicketDetails.fromJson(dynamic json) {
    _ticketId = json['ticketId'];
    _employeeId = json['employeeId'];
    _category = json['category'];
    _subject = json['subject'];
    _description = json['description'];
    _date = json['date'];
    _status = json['status'];
    _created = json['created'];
    _reply = json['reply'];
  }
  String? _ticketId;
  String? _employeeId;
  int? _category;
  String? _subject;
  String? _description;
  dynamic _date;
  int? _status;
  String? _created;
  String? _reply;
  TicketDetails copyWith({  String? ticketId,
    String? employeeId,
    int? category,
    String? subject,
    String? description,
    dynamic date,
    int? status,
    String? created,
    String? reply,
  }) => TicketDetails(  ticketId: ticketId ?? _ticketId,
    employeeId: employeeId ?? _employeeId,
    category: category ?? _category,
    subject: subject ?? _subject,
    description: description ?? _description,
    date: date ?? _date,
    status: status ?? _status,
    created: created ?? _created,
    reply: reply ?? _reply,
  );
  String? get ticketId => _ticketId;
  String? get employeeId => _employeeId;
  int? get category => _category;
  String? get subject => _subject;
  String? get description => _description;
  dynamic get date => _date;
  int? get status => _status;
  String? get created => _created;
  String? get reply => _reply;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['ticketId'] = _ticketId;
    map['employeeId'] = _employeeId;
    map['category'] = _category;
    map['subject'] = _subject;
    map['description'] = _description;
    map['date'] = _date;
    map['status'] = _status;
    map['created'] = _created;
    map['reply'] = _reply;
    return map;
  }

}