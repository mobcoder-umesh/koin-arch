import 'package:dio/dio.dart';


import '../../base/base_end_point.dart';
import 'ticket_request.dart';
import 'ticket_response.dart';

const String _path = 'v1/ticket/viewTicketByEmp';

class TicketEndpointUseCase
    extends GetEndpoint<TicketRequest,TicketResponse> {
  TicketEndpointUseCase(Dio _dio) : super(_dio, _path);
}