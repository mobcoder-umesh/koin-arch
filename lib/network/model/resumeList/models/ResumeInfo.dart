import 'AllResume.dart';

class ResumeInfo {
  ResumeInfo({
      this.totalResume, 
      this.allResume,});

  ResumeInfo.fromJson(dynamic json) {
    totalResume = json['totalResume'];
    if (json['allResume'] != null) {
      allResume = [];
      json['allResume'].forEach((v) {
        allResume?.add(AllResume.fromJson(v));
      });
    }
  }
  int? totalResume;
  List<AllResume>? allResume;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['totalResume'] = totalResume;
    if (allResume != null) {
      map['allResume'] = allResume?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}