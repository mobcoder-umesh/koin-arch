class AllResume {
  AllResume({
      this.id, 
      this.isDeleted, 
      this.title, 
      this.resume, 
      this.employeeId, 
      this.created, 
      this.updated,});

  AllResume.fromJson(dynamic json) {
    id = json['_id'];
    isDeleted = json['isDeleted'];
    title = json['title'];
    resume = json['resume'];
    employeeId = json['employeeId'];
    created = json['created'];
    updated = json['updated'];
  }
  String? id;
  int? isDeleted;
  String? title;
  String? resume;
  String? employeeId;
  String? created;
  String? updated;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['isDeleted'] = isDeleted;
    map['title'] = title;
    map['resume'] = resume;
    map['employeeId'] = employeeId;
    map['created'] = created;
    map['updated'] = updated;
    return map;
  }

}