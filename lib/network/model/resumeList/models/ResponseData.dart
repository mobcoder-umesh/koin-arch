import 'ResumeInfo.dart';

class ResumeListResponseData {
  ResumeListResponseData({
      this.message, 
      this.resumeInfo, 
      this.isAdmin,});

  ResumeListResponseData.fromJson(dynamic json) {
    message = json['message'];
    resumeInfo = json['resumeInfo'] != null ? ResumeInfo.fromJson(json['resumeInfo']) : null;
    isAdmin = json['isAdmin'];
  }
  String? message;
  ResumeInfo? resumeInfo;
  int? isAdmin;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    if (resumeInfo != null) {
      map['resumeInfo'] = resumeInfo?.toJson();
    }
    map['isAdmin'] = isAdmin;
    return map;
  }

}