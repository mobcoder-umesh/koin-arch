import '../../base/base_request_model.dart';

abstract class _Keys {
  static const String employeeId = 'employeeId';
}

class ResumeListRequest extends BaseRequestModel {
final String employeeId;

ResumeListRequest(this.employeeId);



  @override
  Map<String, dynamic> get parameters => {
    _Keys.employeeId : this.employeeId,
  };
}