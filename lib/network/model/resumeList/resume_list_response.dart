import 'package:mob_kitchen_freshers/network/base/base_response_model.dart';

import 'models/ResponseData.dart';

class ResumeListResponse extends BaseResponseModel {
  ResumeListResponse({
    this.statusCode,
    this.responseData,
    this.time,});

  ResumeListResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    responseData = json['responseData'] != null ? ResumeListResponseData.fromJson(json['responseData']) : null;
    time = json['time'];
  }
  int? statusCode;
  ResumeListResponseData? responseData;
  String? time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (responseData != null) {
      map['responseData'] = responseData?.toJson();
    }
    map['time'] = time;
    return map;
  }

}