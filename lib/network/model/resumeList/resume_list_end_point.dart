import 'package:dio/dio.dart';
import 'package:mob_kitchen_freshers/utils/preference_util.dart';

import '../../../utils/app_constant.dart';
import '../../base/base_end_point.dart';
import 'resume_list_request.dart';
import 'resume_list_response.dart';

 String _path = 'v1/resume/getByEmpId?employeeId=';

class ResumeListEndpointUseCase
    extends GetEndpoint<ResumeListRequest, ResumeListResponse> {


  ResumeListEndpointUseCase(Dio _dio,String employeeId) : super(_dio, _path+employeeId );

}




