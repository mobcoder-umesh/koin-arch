import 'ReimbursementList.dart';

class ReimbData {
  ReimbData({
      this.message, 
      this.reimbursementList,});

  ReimbData.fromJson(dynamic json) {
    message = json['message'];
    if (json['reimbursementList'] != null) {
      reimbursementList = [];
      json['reimbursementList'].forEach((v) {
        reimbursementList?.add(ReimbursementList.fromJson(v));
      });
    }
  }
  String? message;
  List<ReimbursementList>? reimbursementList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    if (reimbursementList != null) {
      map['reimbursementList'] = reimbursementList?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}