class MonthlyReimburseList {
  MonthlyReimburseList({
      this.id, 
      this.employeeId, 
      this.title, 
      this.expenseDate, 
      this.expenseDescription, 
      this.amount, 
      this.uploadBill, 
      this.status,});

  MonthlyReimburseList.fromJson(dynamic json) {
    id = json['_id'];
    employeeId = json['employeeId'];
    title = json['title'];
    expenseDate = json['expenseDate'];
    expenseDescription = json['expenseDescription'];
    amount = json['amount'];
    uploadBill = json['uploadBill'] != null ? json['uploadBill'].cast<String>() : [];
    status = json['status'];
  }
  String? id;
  String? employeeId;
  String? title;
  String? expenseDate;
  String? expenseDescription;
  int? amount;
  List<String>? uploadBill;
  dynamic status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['_id'] = id;
    map['employeeId'] = employeeId;
    map['title'] = title;
    map['expenseDate'] = expenseDate;
    map['expenseDescription'] = expenseDescription;
    map['amount'] = amount;
    map['uploadBill'] = uploadBill;
    map['status'] = status;
    return map;
  }

}