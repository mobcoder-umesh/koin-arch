

import 'package:mob_kitchen_freshers/network/base/base_response_model.dart';

import 'ReimbursementData.dart';

class ReimbursementsResponse extends BaseResponseModel{
  ReimbursementsResponse({
      this.statusCode, 
      this.responseData, 
      this.time,});

  ReimbursementsResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    responseData = json['responseData'] != null ? ReimbData.fromJson(json['responseData']) : null;
    time = json['time'];
  }
  int? statusCode;
  ReimbData? responseData;
  String? time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (responseData != null) {
      map['responseData'] = responseData?.toJson();
    }
    map['time'] = time;
    return map;
  }

}