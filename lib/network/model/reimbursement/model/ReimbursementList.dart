import 'Month1.dart';
import 'MonthlyReimburseList.dart';

class ReimbursementList {
  ReimbursementList({
      this.month1, 
      this.total, 
      this.pending, 
      this.approved, 
      this.rejected, 
      this.monthlyReimburseList,});

  ReimbursementList.fromJson(dynamic json) {
    month1 = json['month'] != null ? Month1.fromJson(json['month']) : null;
    total = json['total'];
    pending = json['pending'];
    approved = json['approved'];
    rejected = json['rejected'];
    if (json['monthlyReimburseList'] != null) {
      monthlyReimburseList = [];
      json['monthlyReimburseList'].forEach((v) {
        monthlyReimburseList?.add(MonthlyReimburseList.fromJson(v));
      });
    }
  }
  Month1? month1;
  int? total;
  int? pending;
  int? approved;
  int? rejected;
  List<MonthlyReimburseList>? monthlyReimburseList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (month1 != null) {
      map['month'] = month1?.toJson();
    }
    map['total'] = total;
    map['pending'] = pending;
    map['approved'] = approved;
    map['rejected'] = rejected;
    if (monthlyReimburseList != null) {
      map['monthlyReimburseList'] = monthlyReimburseList?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}