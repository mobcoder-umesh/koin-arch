import 'Month2.dart';
import 'Year2.dart';

class Month1 {
  Month1({
      this.month2, 
      this.year2,});

  Month1.fromJson(dynamic json) {
    month2 = json['month'] != null ? Month2.fromJson(json['month']) : null;
    year2 = json['year'] != null ? Year2.fromJson(json['year']) : null;
  }
  Month2? month2;
  Year2? year2;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (month2 != null) {
      map['month'] = month2?.toJson();
    }
    if (year2 != null) {
      map['year'] = year2?.toJson();
    }
    return map;
  }

}