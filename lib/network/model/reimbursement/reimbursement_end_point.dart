import 'package:dio/dio.dart';


import '../../base/base_end_point.dart';
import 'model/ReimbursementResponse.dart';
import 'reimbursement_request.dart';




const String _path = 'v1/reimbursement/reimburseHistory';
const String _path1 = 'v1/reimbursement/reimburseHistory?searchYear';

class ReimbursementEndpointUseCase
    extends GetEndpoint<ReimbursementRequest,ReimbursementsResponse> {
  ReimbursementEndpointUseCase(Dio _dio) : super(_dio, _path);
}
class ReimbursementEndpointUseCase1
    extends GetEndpoint<ReimbursementRequest,ReimbursementsResponse> {
  ReimbursementEndpointUseCase1(Dio _dio) : super(_dio, _path1);
}