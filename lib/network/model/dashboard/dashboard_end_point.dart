import 'package:dio/dio.dart';
import '../../base/base_end_point.dart';
import 'dashboard_request.dart';
import 'dashboard_response.dart';
const String _path = 'v1/employee/empDashboard';
class DashboardEndpointUseCase
    extends GetEndpoint<DashboardRequest, DashboardResponse> {
  DashboardEndpointUseCase(Dio _dio) : super(_dio, _path);
}
