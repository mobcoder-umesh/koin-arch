

import '../../base/base_response_model.dart';

/// statusCode : 1
/// responseData : {"message":"Employee Dashboard Details","empDashboardDetails":{"onGoingProjects":1,"completedProjects":2,"remainingLeaves":18,"lateComings":0,"avgHours":0,"holidayList":[{"_id":{"month":2,"year":2022},"holidays":[{"holidayId":"61efc6d38a9b5e0279f80415","holidayName":"Engineer's Day","holidayType":1,"date":1643677200000,"day":1,"message":"","status":1}]},{"_id":{"month":8,"year":2022},"holidays":[{"holidayId":"62b95a5ff785655e96065235","holidayName":"Rakshabandhan","holidayType":2,"date":1660179600000,"day":11,"message":"","status":1},{"holidayId":"62a0428cf785655e96038e5a","holidayName":"Independence Day","holidayType":2,"date":1660525200000,"day":15,"message":"","status":1}]},{"_id":{"month":10,"year":2022},"holidays":[{"holidayId":"62b96f67f785655e960656a7","holidayName":"Gandhi Jayanti","holidayType":2,"date":1664672400000,"day":2,"message":"","status":1}]},{"_id":{"month":7,"year":2022},"holidays":[{"holidayId":"62ba7cbdf785655e9606791a","holidayName":"Guru Purnima","holidayType":1,"date":1657674000000,"day":13,"message":"","status":1}]}]},"isAdmin":2}
/// time : "2022-07-15T04:54:49.252Z"

class DashboardResponse  extends BaseResponseModel{
  DashboardResponse({
      int? statusCode, 
      ResponseData? responseData, 
      String? time,}){
    _statusCode = statusCode;
    _responseData = responseData;
    _time = time;
}

  DashboardResponse.fromJson(dynamic json) {
    _statusCode = json['statusCode'];
    _responseData = json['responseData'] != null ? ResponseData.fromJson(json['responseData']) : null;
    _time = json['time'];
  }
  int? _statusCode;
  ResponseData? _responseData;
  String? _time;
DashboardResponse copyWith({  int? statusCode,
  ResponseData? responseData,
  String? time,
}) => DashboardResponse(  statusCode: statusCode ?? _statusCode,
  responseData: responseData ?? _responseData,
  time: time ?? _time,
);
  int? get statusCode => _statusCode;
  ResponseData? get responseData => _responseData;
  String? get time => _time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = _statusCode;
    if (_responseData != null) {
      map['responseData'] = _responseData?.toJson();
    }
    map['time'] = _time;
    return map;
  }

}

/// message : "Employee Dashboard Details"
/// empDashboardDetails : {"onGoingProjects":1,"completedProjects":2,"remainingLeaves":18,"lateComings":0,"avgHours":0,"holidayList":[{"_id":{"month":2,"year":2022},"holidays":[{"holidayId":"61efc6d38a9b5e0279f80415","holidayName":"Engineer's Day","holidayType":1,"date":1643677200000,"day":1,"message":"","status":1}]},{"_id":{"month":8,"year":2022},"holidays":[{"holidayId":"62b95a5ff785655e96065235","holidayName":"Rakshabandhan","holidayType":2,"date":1660179600000,"day":11,"message":"","status":1},{"holidayId":"62a0428cf785655e96038e5a","holidayName":"Independence Day","holidayType":2,"date":1660525200000,"day":15,"message":"","status":1}]},{"_id":{"month":10,"year":2022},"holidays":[{"holidayId":"62b96f67f785655e960656a7","holidayName":"Gandhi Jayanti","holidayType":2,"date":1664672400000,"day":2,"message":"","status":1}]},{"_id":{"month":7,"year":2022},"holidays":[{"holidayId":"62ba7cbdf785655e9606791a","holidayName":"Guru Purnima","holidayType":1,"date":1657674000000,"day":13,"message":"","status":1}]}]}
/// isAdmin : 2

class ResponseData {
  ResponseData({
      String? message, 
      EmpDashboardDetails? empDashboardDetails, 
      int? isAdmin,}){
    _message = message;
    _empDashboardDetails = empDashboardDetails;
    _isAdmin = isAdmin;
}

  ResponseData.fromJson(dynamic json) {
    _message = json['message'];
    _empDashboardDetails = json['empDashboardDetails'] != null ? EmpDashboardDetails.fromJson(json['empDashboardDetails']) : null;
    _isAdmin = json['isAdmin'];
  }
  String? _message;
  EmpDashboardDetails? _empDashboardDetails;
  int? _isAdmin;
ResponseData copyWith({  String? message,
  EmpDashboardDetails? empDashboardDetails,
  int? isAdmin,
}) => ResponseData(  message: message ?? _message,
  empDashboardDetails: empDashboardDetails ?? _empDashboardDetails,
  isAdmin: isAdmin ?? _isAdmin,
);
  String? get message => _message;
  EmpDashboardDetails? get empDashboardDetails => _empDashboardDetails;
  int? get isAdmin => _isAdmin;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = _message;
    if (_empDashboardDetails != null) {
      map['empDashboardDetails'] = _empDashboardDetails?.toJson();
    }
    map['isAdmin'] = _isAdmin;
    return map;
  }

}

/// onGoingProjects : 1
/// completedProjects : 2
/// remainingLeaves : 18
/// lateComings : 0
/// avgHours : 0
/// holidayList : [{"_id":{"month":2,"year":2022},"holidays":[{"holidayId":"61efc6d38a9b5e0279f80415","holidayName":"Engineer's Day","holidayType":1,"date":1643677200000,"day":1,"message":"","status":1}]},{"_id":{"month":8,"year":2022},"holidays":[{"holidayId":"62b95a5ff785655e96065235","holidayName":"Rakshabandhan","holidayType":2,"date":1660179600000,"day":11,"message":"","status":1},{"holidayId":"62a0428cf785655e96038e5a","holidayName":"Independence Day","holidayType":2,"date":1660525200000,"day":15,"message":"","status":1}]},{"_id":{"month":10,"year":2022},"holidays":[{"holidayId":"62b96f67f785655e960656a7","holidayName":"Gandhi Jayanti","holidayType":2,"date":1664672400000,"day":2,"message":"","status":1}]},{"_id":{"month":7,"year":2022},"holidays":[{"holidayId":"62ba7cbdf785655e9606791a","holidayName":"Guru Purnima","holidayType":1,"date":1657674000000,"day":13,"message":"","status":1}]}]

class EmpDashboardDetails {
  EmpDashboardDetails({
      int? onGoingProjects, 
      int? completedProjects, 
      int? remainingLeaves, 
      int? lateComings, 
      int? avgHours, 
      List<HolidayList>? holidayList,}){
    _onGoingProjects = onGoingProjects;
    _completedProjects = completedProjects;
    _remainingLeaves = remainingLeaves;
    _lateComings = lateComings;
    _avgHours = avgHours;
    _holidayList = holidayList;
}

  EmpDashboardDetails.fromJson(dynamic json) {
    _onGoingProjects = json['onGoingProjects'];
    _completedProjects = json['completedProjects'];
    _remainingLeaves = json['remainingLeaves'];
    _lateComings = json['lateComings'];
    _avgHours = json['avgHours'];
    if (json['holidayList'] != null) {
      _holidayList = [];
      json['holidayList'].forEach((v) {
        _holidayList?.add(HolidayList.fromJson(v));
      });
    }
  }
  int? _onGoingProjects;
  int? _completedProjects;
  int? _remainingLeaves;
  int? _lateComings;
  int? _avgHours;
  List<HolidayList>? _holidayList;
EmpDashboardDetails copyWith({  int? onGoingProjects,
  int? completedProjects,
  int? remainingLeaves,
  int? lateComings,
  int? avgHours,
  List<HolidayList>? holidayList,
}) => EmpDashboardDetails(  onGoingProjects: onGoingProjects ?? _onGoingProjects,
  completedProjects: completedProjects ?? _completedProjects,
  remainingLeaves: remainingLeaves ?? _remainingLeaves,
  lateComings: lateComings ?? _lateComings,
  avgHours: avgHours ?? _avgHours,
  holidayList: holidayList ?? _holidayList,
);
  int? get onGoingProjects => _onGoingProjects;
  int? get completedProjects => _completedProjects;
  int? get remainingLeaves => _remainingLeaves;
  int? get lateComings => _lateComings;
  int? get avgHours => _avgHours;
  List<HolidayList>? get holidayList => _holidayList;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['onGoingProjects'] = _onGoingProjects;
    map['completedProjects'] = _completedProjects;
    map['remainingLeaves'] = _remainingLeaves;
    map['lateComings'] = _lateComings;
    map['avgHours'] = _avgHours;
    if (_holidayList != null) {
      map['holidayList'] = _holidayList?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// _id : {"month":2,"year":2022}
/// holidays : [{"holidayId":"61efc6d38a9b5e0279f80415","holidayName":"Engineer's Day","holidayType":1,"date":1643677200000,"day":1,"message":"","status":1}]

class HolidayList {
  HolidayList({
      Id? id, 
      List<Holidays>? holidays,}){
    _id = id;
    _holidays = holidays;
}

  HolidayList.fromJson(dynamic json) {
    _id = json['_id'] != null ? Id.fromJson(json['_id']) : null;
    if (json['holidays'] != null) {
      _holidays = [];
      json['holidays'].forEach((v) {
        _holidays?.add(Holidays.fromJson(v));
      });
    }
  }
  Id? _id;
  List<Holidays>? _holidays;
HolidayList copyWith({  Id? id,
  List<Holidays>? holidays,
}) => HolidayList(  id: id ?? _id,
  holidays: holidays ?? _holidays,
);
  Id? get id => _id;
  List<Holidays>? get holidays => _holidays;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_id != null) {
      map['_id'] = _id?.toJson();
    }
    if (_holidays != null) {
      map['holidays'] = _holidays?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// holidayId : "61efc6d38a9b5e0279f80415"
/// holidayName : "Engineer's Day"
/// holidayType : 1
/// date : 1643677200000
/// day : 1
/// message : ""
/// status : 1

class Holidays {
  Holidays({
      String? holidayId, 
      String? holidayName, 
      int? holidayType, 
      int? date, 
      int? day, 
      String? message, 
      int? status,}){
    _holidayId = holidayId;
    _holidayName = holidayName;
    _holidayType = holidayType;
    _date = date;
    _day = day;
    _message = message;
    _status = status;
}

  Holidays.fromJson(dynamic json) {
    _holidayId = json['holidayId'];
    _holidayName = json['holidayName'];
    _holidayType = json['holidayType'];
    _date = json['date'];
    _day = json['day'];
    _message = json['message'];
    _status = json['status'];
  }
  String? _holidayId;
  String? _holidayName;
  int? _holidayType;
  int? _date;
  int? _day;
  String? _message;
  int? _status;
Holidays copyWith({  String? holidayId,
  String? holidayName,
  int? holidayType,
  int? date,
  int? day,
  String? message,
  int? status,
}) => Holidays(  holidayId: holidayId ?? _holidayId,
  holidayName: holidayName ?? _holidayName,
  holidayType: holidayType ?? _holidayType,
  date: date ?? _date,
  day: day ?? _day,
  message: message ?? _message,
  status: status ?? _status,
);
  String? get holidayId => _holidayId;
  String? get holidayName => _holidayName;
  int? get holidayType => _holidayType;
  int? get date => _date;
  int? get day => _day;
  String? get message => _message;
  int? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['holidayId'] = _holidayId;
    map['holidayName'] = _holidayName;
    map['holidayType'] = _holidayType;
    map['date'] = _date;
    map['day'] = _day;
    map['message'] = _message;
    map['status'] = _status;
    return map;
  }

}

/// month : 2
/// year : 2022

class Id {
  Id({
      int? month, 
      int? year,}){
    _month = month;
    _year = year;
}

  Id.fromJson(dynamic json) {
    _month = json['month'];
    _year = json['year'];
  }
  int? _month;
  int? _year;
Id copyWith({  int? month,
  int? year,
}) => Id(  month: month ?? _month,
  year: year ?? _year,
);
  int? get month => _month;
  int? get year => _year;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['month'] = _month;
    map['year'] = _year;
    return map;
  }

}