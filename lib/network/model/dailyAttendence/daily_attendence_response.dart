import 'package:mob_kitchen_freshers/network/base/base_response_model.dart';

import 'model/ResponseData.dart';

class DailyAttendenceResponse extends BaseResponseModel{
  DailyAttendenceResponse({
      this.statusCode, 
      this.responseData, 
      this.time,});

  DailyAttendenceResponse.fromJson(dynamic json) {
    statusCode = json['statusCode'];
    responseData = json['responseData'] != null ? DailyAttendenceResponseData.fromJson(json['responseData']) : null;
    time = json['time'];
  }
  int? statusCode;
  DailyAttendenceResponseData? responseData;
  String? time;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['statusCode'] = statusCode;
    if (responseData != null) {
      map['responseData'] = responseData?.toJson();
    }
    map['time'] = time;
    return map;
  }

}