class AttendanceRecord {
  AttendanceRecord({
      this.attendanceId, 
      this.checkIn, 
      this.checkOut, 
      this.date, 
      this.year, 
      this.month, 
      this.day, 
      this.quarter, 
      this.totalHours, 
      this.status, 
      this.created,});

  AttendanceRecord.fromJson(dynamic json) {
    attendanceId = json['attendanceId'];
    checkIn = json['checkIn'];
    checkOut = json['checkOut'];
    date = json['date'];
    year = json['year'];
    month = json['month'];
    day = json['day'];
    quarter = json['quarter'];
    totalHours = json['totalHours'];
    status = json['status'];
    created = json['created'];
  }
  String? attendanceId;
  int? checkIn;
  int? checkOut;
  int? date;
  int? year;
  int? month;
  int? day;
  int? quarter;
  int? totalHours;
  int? status;
  String? created;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['attendanceId'] = attendanceId;
    map['checkIn'] = checkIn;
    map['checkOut'] = checkOut;
    map['date'] = date;
    map['year'] = year;
    map['month'] = month;
    map['day'] = day;
    map['quarter'] = quarter;
    map['totalHours'] = totalHours;
    map['status'] = status;
    map['created'] = created;
    return map;
  }

}