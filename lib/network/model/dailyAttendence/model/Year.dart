class Year {
  Year({
      this.year, 
      this.month, 
      this.quarter,});

  Year.fromJson(dynamic json) {
    year = json['year'];
    month = json['month'];
    quarter = json['quarter'];
  }
  int? year;
  int? month;
  int? quarter;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['year'] = year;
    map['month'] = month;
    map['quarter'] = quarter;
    return map;
  }

}