import 'Year.dart';
import 'AttendanceRecord.dart';

class AttendenceDetails {
  AttendenceDetails({
      this.year, 
      this.attendanceRecord, 
      this.dataCount,});

  AttendenceDetails.fromJson(dynamic json) {
    year = json['year'] != null ? Year.fromJson(json['year']) : null;
    if (json['AttendanceRecord'] != null) {
      attendanceRecord = [];
      json['AttendanceRecord'].forEach((v) {
        attendanceRecord?.add(AttendanceRecord.fromJson(v));
      });
    }
    dataCount = json['dataCount'];
  }
  Year? year;
  List<AttendanceRecord>? attendanceRecord;
  int? dataCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (year != null) {
      map['year'] = year?.toJson();
    }
    if (attendanceRecord != null) {
      map['AttendanceRecord'] = attendanceRecord?.map((v) => v.toJson()).toList();
    }
    map['dataCount'] = dataCount;
    return map;
  }

}