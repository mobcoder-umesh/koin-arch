import 'AttendenceDetails.dart';

class DailyAttendenceResponseData {
  DailyAttendenceResponseData({
      this.message, 
      this.attendenceDetails, 
      this.totalAttendence, 
      this.late, 
      this.avgHours, 
      this.earlyCheckoutCount,});

  DailyAttendenceResponseData.fromJson(dynamic json) {
    message = json['message'];
    if (json['AttendenceDetails'] != null) {
      attendenceDetails = [];
      json['AttendenceDetails'].forEach((v) {
        attendenceDetails?.add(AttendenceDetails.fromJson(v));
      });
    }
    totalAttendence = json['totalAttendence'];
    late = json['late'];
    avgHours = json['avgHours'];
    earlyCheckoutCount = json['earlyCheckoutCount'];
  }
  String? message;
  List<AttendenceDetails>? attendenceDetails;
  int? totalAttendence;
  int? late;
  int? avgHours;
  int? earlyCheckoutCount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['message'] = message;
    if (attendenceDetails != null) {
      map['AttendenceDetails'] = attendenceDetails?.map((v) => v.toJson()).toList();
    }
    map['totalAttendence'] = totalAttendence;
    map['late'] = late;
    map['avgHours'] = avgHours;
    map['earlyCheckoutCount'] = earlyCheckoutCount;
    return map;
  }

}