import 'package:dio/dio.dart';

import '../../base/base_end_point.dart';
import 'daily_attendence_response.dart';
import 'daily_attendence_request.dart';

const String _path = 'v1/attendence/viewMonthlyAttendenceByEmp';

class DailyAttendenceEndpointUseCase
    extends GetEndpoint<DailyAttendenceRequest, DailyAttendenceResponse> {
  DailyAttendenceEndpointUseCase(Dio _dio) : super(_dio, _path);
}
