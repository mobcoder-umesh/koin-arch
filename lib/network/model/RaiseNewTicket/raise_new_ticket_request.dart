import '../../base/base_request_model.dart';

abstract class _Keys {
  static const String category = 'category';
  static const String subject = 'subject';

  static const String ticketDescription = 'ticketDescription';
}

class RaiseNewTicketRequest extends BaseRequestModel {
  final String category;
  final String subject;

  final String ticketDescription;

  RaiseNewTicketRequest(
    this.category,
    this.subject,
    this.ticketDescription,
  );

  @override
  Map<String, dynamic> get parameters => {
        _Keys.category: this.category,
        _Keys.subject: this.subject,
        _Keys.ticketDescription: this.ticketDescription,
      };
}
