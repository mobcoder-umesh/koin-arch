import 'package:dio/dio.dart';


import '../../base/base_end_point.dart';
import 'user_profile_request.dart';
import 'user_profile_response.dart';

const String _path = 'v1/employee/getProfile';

class UserProfileEndpointUseCase
    extends GetEndpoint<UserProfileRequest,UserProfileResponse>{
  UserProfileEndpointUseCase(Dio _dio): super(_dio,_path);
}
