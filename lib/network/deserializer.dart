

import 'dart:convert';

import 'package:mob_kitchen_freshers/network/model/deleteResume/delete_resume_response.dart';
import 'package:mob_kitchen_freshers/network/model/error/error_response.dart';

import 'package:mob_kitchen_freshers/network/model/reimbursement/model/ReimbursementResponse.dart';
import 'package:mob_kitchen_freshers/network/model/tickets/ticket_response.dart';

import 'package:mob_kitchen_freshers/network/model/projectRecord/project_record_response.dart';
import 'package:mob_kitchen_freshers/network/model/resumeList/resume_list_response.dart';

import 'package:mob_kitchen_freshers/network/model/userProfile/user_profile_response.dart';
import 'package:mob_kitchen_freshers/network/model/vendor/vendor_response.dart';


import 'base/base_response_model.dart';
import 'model/dailyAttendence/daily_attendence_response.dart';
import 'model/deviceRecord/device_record_response.dart';
import 'model/dashboard/dashboard_response.dart';
import 'model/leave/leaveRecord/leave_record_response.dart';
import 'model/login/login_response.dart';
import 'model/projectRecord/project_record_response.dart';
import 'model/reimbursement/model/ReimbursementResponse.dart';
import 'model/resumeList/resume_list_response.dart';
import 'model/tickets/ticket_response.dart';
import 'model/userProfile/user_profile_response.dart';
import 'model/vendor/vendor_response.dart';

abstract class Deserializer {
  static final Map<Type, Function(Map<String, dynamic>)> _deserializers = {
    LoginResponse: (Map<String, dynamic> json) => LoginResponse.fromJson(json),

    VendorResponse:(Map<String, dynamic> json) =>VendorResponse.fromJson(json),
    DashboardResponse:(Map<String, dynamic> json) =>DashboardResponse.fromJson(json),
    UserProfileResponse:(Map<String, dynamic> json) =>UserProfileResponse.fromJson(json),
    ProjectRecordResponse:(Map<String, dynamic> json) =>ProjectRecordResponse.fromJson(json),
    ResumeListResponse: (Map<String, dynamic> json) =>ResumeListResponse.fromJson(json),
    DeleteResumeResponse:(Map<String, dynamic> json) =>DeleteResumeResponse.fromJson(json),

    ReimbursementsResponse:(Map<String, dynamic> json) =>ReimbursementsResponse.fromJson(json),
   // ClaimRaiseResponse:(Map<String, dynamic> json) =>ClaimRaiseResponse.fromJson(json),
    TicketResponse:(Map<String, dynamic> json) =>TicketResponse.fromJson(json),


    UserProfileResponse: (Map<String, dynamic> json) =>
        UserProfileResponse.fromJson(json),

    DailyAttendenceResponse: (Map<String, dynamic> json) =>
        DailyAttendenceResponse.fromJson(json),
    LeaveRecordResponse: (Map<String, dynamic> json) =>
        LeaveRecordResponse.fromJson(json),
    //ApplyLeaveResponse:(Map<String, dynamic> json) =>ApplyLeaveResponse.fromJson(json),
    DeviceRecordResponse: (Map<String, dynamic> json) =>
        DeviceRecordResponse.fromJson(json),




    //RaiseNewTicketResponse:(Map<String, dynamic> json) =>RaiseNewTicketResponse.fromJson(json),
  };

  static Response deserialize<Response extends BaseResponseModel>(
      Map<String, dynamic> json) {
    // int statusCode = map['statusCode'];
    // if (statusCode == 1) {
    //   Map<String, dynamic> data = Map();
    //   data = map['responseData'];
    //   return Deserializer._deserializers[Response]!(data);
    // }else{
    //   print('111111111');
    //
    //   Map<String, dynamic> error = Map();
    //   error = map['error'];
    //   return Deserializer._deserializers[Response]!(error);
    //
    // }

    return Deserializer._deserializers[Response]!(json);
  }
}
